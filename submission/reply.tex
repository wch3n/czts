\documentclass[11pt,english,numbers=endperiod,parskip=full]{scrartcl}
\usepackage[fromlogo,foldmarks=false]{scrletter}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[version=4]{mhchem}
\usepackage[breaklinks,colorlinks,linkcolor={blue},
            citecolor={blue},urlcolor={blue}]{hyperref}
\usepackage{setspace}
\onehalfspacing

\setkomafont{section}{\large}
\setkomavar{backaddress}{}
\setkomavar{fromlogo}{\includegraphics[width=0.25\textwidth]{uclouvain}}
\setkomavar{signature}{Wei Chen, Diana Dahliah, Gian-Marco Rignanese, and Geoffroy Hautier}
\setkomavar{location}{MODL \\ Chemin des \'{E}toiles 8/L7.03.01 \\ B-1348 Louvain-la-Neuve}
\renewcommand*{\raggedsignature}{\raggedright}

\begin{document}
\begin{letter}{Dr Michael Spencelayh\\
Publishing Editor, Journals\\
Royal Society of Chemistry}
\opening{Dear Editor,}

Thank you for sending our manuscript 
``\textit{Origin of the low conversion efficiency in \ce{Cu2ZnSnS4} kesterite solar cells: 
the actual role of cation disorder}''
to three of your referees.
We are grateful for their very positive comments on our work and 
also for their valuable suggestions for further improvement.
In the following, we reply to all the comments and indicate the changes that are
incorporated into the revised manuscript.

We are confident that our revised manuscript has addressed all the points raised by the referees.
We thank you for your consideration of publishing our work in Energy \& Environmental Science.

\closing{Yours sincerely,}

\end{letter}

\section{Response to Referee 1}
\textit{This study thus presents a very rigorous study from first principles 
of the optical properties and defects of disordered CZTSSe, 
which will provide guidance to reducing the energy loss in kesterite and photovoltaic materials. 
I am very pleased to recommend this manuscript for publication in Energy \& Environmental Science 
given the significance and novelty.}

We thank the referee for the positive remarks and the recommendation for publication.

\textit{Minor points to note \\
In Figure 1, (b) was not properly displayed.}

While we do not find any irregularities of the labels in Fig.~1, 
we have regenerated the figure and provided a rasterized EPS version with the resubmission.

\textit{In Figure 5, the dashed line should be defined. The origin of deviation needs further explanations.}

The dashed green line highlighting the valence-band fluctuations is obtained as the isoline of the 
local density of states. The isovalue is chosen so that the isoline at the highest energy reconciles the VBM.
We have clarified the definition of the dashed line in the caption of Fig.~5 (p.~8).

The valence-band fluctuations are related to the electrostatic potential variations
at the Cu-rich and the Cu-poor sites. This has been illustrated in Ref.~44.
We have clarified this in the revised manuscript (p.~7).

\section{Response to Referee 2}
\textit{General point -  This is an old issue in kesterite solar cells and 
one that reached peak interest about 5 years ago. 
There were a number of experiments reports that supported the idea that cation disorder 
alone did not cause the voltage deficit. 
In particular Jonathan Scragg and colleagues pioneered slow anneals for these systems 
to tune cation disorder leading to observations such as 
``The open circuit voltage deficit was not affected by the ordering degree''
in \url{https://www.sciencedirect.com/science/article/pii/S0927024816000763}. 
But I agree that disorder can trigger other phenomena, which I think is the novelty of this manuscript.}

We thank the referee for recognizing the novelty of our work and 
for recommending publication in EES. The work by Rey \textit{et al.}\
mentioned by the referee has been cited in the introduction section of the revised manuscript 
when we discuss the experimental evidences showing the absence of correlation between cation disorder
and open-circuit voltage ($V_\text{OC}$) deficit for CZTSSe devices.

\textit{Specific suggestions: \\
- I suggest replacing ``actual'' by ``secondary'' in the title. 
This makes it more complementary to the existing literature. 
It is not showing they are wrong, but presenting an additional viewpoint.}

The role of cation disorder has been under scrutiny over the last few years
as more experimental studies show that the low $V_\text{OC}$ is not improved
by a higher degree of ordering. From the theoretical point of view,
there were some contradictory statements and it was not clear how cation disorder
could be associated with the poor performance of CZTS, partly because of the lack of 
a rigorous statistical treatment of disorder.
While our present work unveils an important role of cation disorder through
the stabilization of deep defects, 
it conveys an equally important message that band-gap fluctuations and Urbach tails
due to cation disorder are not the culprit of the low conversion efficiency of CZTS.
These two aspects form the \emph{actual} role of cation disorder, which is 
elucidated for the first time.
In addition, we feel that using the ``secondary role'' in the title might lead 
to the misconception that band-gap fluctuations and band tailing 
constitute the major role of cation disorder.
Based on these considerations, we prefer to keep the title as is.

\textit{- Add an actual \% limit in the abstract. At the moment it is too vague.}

The maximum conversion efficiency 13\% has been added to the abstract.

\textit{- I recommend a brief method section following the introduction. 
The reader cannot judge or understand what is being reported otherwise 
as the results jump straight into cluster expansion data that is only accessible to a specialist. }

We have added a brief introduction to the cluster expansion at the beginning of 
the results section (p.~1--2).

\textit{- "Predicted DOS as a function of temperature." 
It is important to emphasise what temperature effects were included 
as this could also also imply electron-phonon interactions.}

We have clarified in the caption of Fig.~2 that it is the configurational dependence of the DOS as a function
of temperature (see p.~3).

\textit{- Table I - clarify the charge state (or if this is a sum over all charge state for each species).}

Indeed, the defect concentrations are the sum over all charge states for each defect species. 
A detailed breakdown of the concentrations for different charge states are given in Table~S1 
of the Supplementary Information.
We have made this clear in the caption of Table~1 (p.~7).

\textit{- The point defect section is very long winded and a little difficult to read. 
It could be better to add some sub-section heading to break up the concepts.}

We have added subsection headings for the point defect section and the discussion section.

\textit{- I recommend adding a paragraph to the discussion or conclusions 
on the limitations of the approaches used. 
There are obvious points such as whether the cell sizes are large enough to fully describe the disorder, 
but also a bigger issue on whether defects and disorder are separable since they form simultaneously. 
A grand-canonical approach may be the ultimate way to model such a system? 
In any case, the piece would be stronger if the limitations and pathways forward were 
outlined as there are many theorists working on similar topics.}

The present defect calculation employs a supercell of 288 atoms. 
Figure~S1 shows that the pair correlation functions of the supercell agree well with those
of MC simulations.
As a result, the degree of disorder should be equally well described by the current supercell.

We agree on the second point that future work should put both disorder and point defects
on the same footing in the cluster expansion. 
This is particularly relevant as the kesterite materials are typically Cu-rich and Zn-poor 
among the best-performing CZTSSe devices.
The non-stoichiometry of kesterite is necessarily accompanied by a large number of point defects,
which in turn have an effect on the order-disorder transition of CZTSSe.
Following the referee's suggestion,  we have outlined an extension to the present work to fully account
for the interplay between disorder and defects in a grand (or semi-grand) canonical ensemble.
This can be found before the conclusion section in the revised manuscript (p.~9).

\textit{- Methods: you should not point to an unpublished reference for methodological details. 
Either add a preprint for Ref 80 or remove the reference and include the relevant details.}

Since Ref. 80 is yet to be published, we decide to give the relevant details on the efficiency calculation.
In the revised manuscript, we have provided the expressions for the three current
density components at the end of the method section (p.~10).

\section{Response to Referee 3}
\textit{The manuscript reports theoretical work on kesterite, 
a material of great interest for photovoltaics. 
They provide novel fundamental understanding on the limits of kesterite, 
suggesting that the current, unsatisfactory, performance of this material 
may indeed be close to its intrinsic limits, at variance to what was believed so far. 
This result is novel and is of considerable general interest. 
The methods are sound and the conclusions are supported by the data provided. 
Therefore, the paper can be accepted for publication.}

We thank the referee for the positive comments on our work.
\end{document}
