#!/usr/bin/env python3

from pymatgen.core import Structure,Molecule
import numpy as np
import random

s = Structure.from_file('lt_288.vasp')

to_del = np.where((s.frac_coords[:,0] > 0.35) & (s.frac_coords[:,1] > 0.35) & (s.frac_coords[:,2] > 0.2))[0]
s.remove_sites(to_del)

to_replace =  np.where((s.frac_coords[:,0] > 0.3) & (s.frac_coords[:,1] > 0.30) & (s.frac_coords[:,2] > 0.4) & (s.frac_coords[:,2] < 0.9))[0]
zn = list(s.indices_from_symbol('Zn'))

sn = set(to_replace).intersection(zn)

for i in random.sample(list(sn),2):
    s.replace(i,'K')

mol = Molecule.from_sites(s)
mol.to('xyz','lt_288.xyz')
