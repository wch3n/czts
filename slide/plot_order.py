#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, AutoMinorLocator

df_ccts = pd.read_hdf('../figs/occ_ccts.h5')
df_czts = pd.read_hdf('../figs/occ_czts.h5')

df_ccts = df_ccts.sort_values(by='Temp')
df_czts = df_czts.sort_values(by='Temp')

fig_width = 8/2.54
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)

fig, ax1 = plt.subplots(1)

ax1.plot(df_ccts.Temp, df_ccts.Order, '-o', color='C0', label=r'Cu$_2$CdSnS$_4$', zorder=10)
ax1.plot(df_czts.Temp, df_czts.Order, '-o', color='C1', label=r'Cu$_2$ZnSnS$_4$', zorder=1)
ax1.set_xlabel('Temperature (K)')
ax1.set_ylabel('Order parameter')
ax1.set_xlim([200,900])
ax1.text(700,0.7,r'Cu$_2$CdSnS$_4$')
ax1.text(350,0.45,r'Cu$_2$ZnSnS$_4$')

minorLocator = AutoMinorLocator(n=2)
ax1.yaxis.set_minor_locator(minorLocator)

plt.tight_layout()
plt.savefig('order.pdf')
