#!/usr/bin/env python3
 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

fig_width = 8.5/2.54
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)


kwargs = {"delim_whitespace": True}
bbox = {"bbox":{"boxstyle":"square", "fc":(1,1,1), "ec":(1,1,1)}, "va": "bottom"}

df_ccts = pd.read_csv('../figs/mc_ccts.csv', **kwargs)
df_czts = pd.read_csv('../figs/mc_czts.csv', **kwargs)
df_ccts = df_ccts.iloc[::-1]
df_czts = df_czts.iloc[::-1]

clex = {"czts":{"kest":-28.58373368, "stan":-28.54818776},"ccts":{"stan":-27.87809300, "kest":-27.84030424}}
#fig, (ax1,ax2,ax3) = plt.subplots(3,1, sharex = True)
fig, ax1 = plt.subplots(1)
#
for ax in [ax1]:
    ax.set_xlim([200,1000])
czts_str = r"Cu$_2$ZnSnS$_4$"
ccts_str = r"Cu$_2$CdSnS$_4$"
#    
ax1.plot(df_ccts['T'], df_ccts['heat_capacity']*4*1000, '-o', color='C0')
ax1.plot(df_czts['T'], df_czts['heat_capacity']*4*1000, '-o', color='C1')
ax1.set_ylabel('Heat capacity (meV/K)')
ax1.text(300, 0.55, czts_str)
ax1.text(600, 0.3, ccts_str)
ax1.set_xlabel('Temperature (K)')
#
'''
ax2.plot(df_ccts['T'], df_ccts['<potential_energy>']*4 - clex['ccts']['kest'], '-o', color='C0', zorder=9)
ax2.set_ylabel('Potential energy (eV)')
ax2.axhline(clex['ccts']['kest']-clex['ccts']['kest'], ls='--', lw=0.5, color='gray')
ax2.axhline(clex['ccts']['stan']-clex['ccts']['kest'], ls='--', lw=0.5, color='gray')
ax2.text(750,clex['ccts']['kest']-clex['ccts']['kest'], 'kesterite', **bbox)
ax2.text(750,clex['ccts']['stan']-clex['ccts']['kest'], 'stannite',  **bbox)
ax2.text(0.1, 0.85, ccts_str, transform=ax2.transAxes)
#
ax3.plot(df_czts['T'], df_czts['<potential_energy>']*4 - clex['czts']['kest'], '-o', color='C1', zorder=9)
ax3.set_ylabel('Potential energy (eV)')
ax3.axhline(clex['czts']['kest']-clex['czts']['kest'], ls='--', lw=0.5, color='gray')
ax3.axhline(clex['czts']['stan']-clex['czts']['kest'], ls='--', lw=0.5, color='gray')
ax3.text(750,clex['czts']['kest']-clex['czts']['kest'], 'kesterite', **bbox, )
ax3.text(750,clex['czts']['stan']-clex['czts']['kest'], 'stannite',  **bbox, )
ax3.text(0.1, 0.85, czts_str, transform=ax3.transAxes)
ax3.set_xlabel('Temperature (K)')

for ax in (ax1, ax2, ax3):
    ax.yaxis.set_label_coords(-0.12, 0.5)
'''

plt.tight_layout()
plt.savefig('tc.pdf')
