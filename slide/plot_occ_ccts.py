#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, AutoMinorLocator

df_ccts = pd.read_hdf('../figs/occ_ccts.h5')
df_czts = pd.read_hdf('../figs/occ_czts.h5')

df_ccts = df_ccts.sort_values(by='Temp')
df_czts = df_czts.sort_values(by='Temp')

fig_width = 8/2.54
fig_height = fig_width*0.7
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)

fig, ax1 = plt.subplots(1)

ax1.plot(df_ccts.Temp, df_ccts.Cu_1, '-.', label=r'${2d}$')
ax1.plot(df_ccts.Temp, 1 - df_ccts.Cd, '-.', label=r'${2a}$')
ax1.plot(df_ccts.Temp, df_ccts.Cu_2, '-+', mfc='none', label=r'${2c}$', zorder=-99)
ax1.set_title(r'Cu$_2$CdSnS$_4$', fontsize=9)
ax1.set_xlabel('Temperature (K)')
ax1.set_ylabel('Cu occupancy')
ax1.set_xlim([200,900])
ax1.legend()
ax1.axhline(2/3, color='gray', lw=0.5, ls='dashed', zorder=0)

minorLocator = AutoMinorLocator(n=2)
ax1.yaxis.set_minor_locator(minorLocator)

plt.tight_layout()
plt.savefig('occ_ccts_0.pdf')
