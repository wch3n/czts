#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

fig_width = 8.5/2.54
fig_height = fig_width*0.7
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 8,
          'axes.labelweight': 'light',
          'font.size': 8,
          'font.weight': 'light',
          'legend.fontsize': 8,
          'xtick.labelsize': 8,
          'ytick.labelsize': 8,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 4.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'ytick.right': False,
          'xtick.major.width': 0.8,
          'xtick.minor.width': 0.6,
          'ytick.major.width': 0.8,
          'ytick.minor.width': 0.6}
plt.rcParams.update(params)

fig, ax1 = plt.subplots(1)

v = np.linspace(0,1,100)

i_0 = 1e-15
i_l = 0.5
j = i_l - i_0*(np.exp(v/0.026) - 1)
v_oc = np.log(i_l/i_0+1)*0.026
v_mp = 0.8
i_mp = i_l - i_0*(np.exp(v_mp/0.026) - 1)

p = j*v

ax1.set_ylim([0,0.6])
ax1.set_xlim([0,1])
ax1.set_xlabel('Voltage (V)')
ax1.set_ylabel('Current (A)')
ax1.plot(v,j,c='C0',label='Current',lw=2, zorder=99)
ax1.fill_between(v, 0*np.ones(v.size), j, facecolor='C0', alpha=0.4)
ax2 = ax1.twinx()
ax2.set_ylim([0,0.6])
ax2.set_ylabel('Power (W)')
ax2.plot(v,p,c='C3',label='Power',lw=2, zorder=8)

ax1.set_yticks([0,i_mp,i_l])
ax1.set_yticklabels(['0',r'$I_{MP}$',r'$I_{SC}$'])
ax1.set_xticks([0,v_mp,v_oc])
ax1.set_xticklabels(['0',r'$V_{MP}$', r'$V_{OC}$'])
ax2.set_yticks([0, i_mp*v_mp])
ax2.set_yticklabels(['0','MP'])

ax2.vlines(v_mp, ymin=0, ymax=i_mp, color='k', linestyle='--', linewidth=1, zorder=0)
ax2.hlines(i_mp, xmin=0, xmax=v_mp, color='k', linestyle='--', linewidth=1, zorder=0)
ax2.hlines(v_mp*i_mp, xmin=v_mp, xmax=1, color='k', linestyle='-', linewidth=0.5, zorder=0)

plt.tight_layout()
plt.savefig('jv.pdf')
