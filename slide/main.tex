\documentclass[xcolor=pdftex,dvipsnames,table]{beamer}
\usepackage{amsmath}
\usepackage[font=footnotesize]{caption}
\usepackage{graphicx}
\usepackage{adjustbox}
\usepackage{tabularx}
\usepackage{tikz-feynman}
\usepackage[style=authoryear, maxbibnames=5, maxcitenames=1, backend=biber]{biblatex}
\usepackage[scaled=0.86]{helvet}
\hypersetup{pdfstartview={Fit}}
\usetheme{Pittsburgh}
\usecolortheme{seagull}
\usefonttheme{professionalfonts}
%\beamertemplatenavigationsymbolsempty
%
\addbibresource{main.bib}
\renewcommand*{\bibfont}{\scriptsize}
\DeclareMathOperator\erf{erf}
%
\begin{document}
\title[kesterite vs stannite]
{\textbf{Kesterite solar cells}}
\subtitle{\textsc{Order-disorder transition}}
\author[\textsc{W. Chen}]{Wei Chen}
\date[Oct 2018] %
{\small Oct. 2018}
%
\frame{\titlepage}
%
\section{CZTS Status}
\begin{frame}{Cu$_2$ZnSnS$_4$ (CZTS)}
    \begin{columns}[c]
    \column{0.8\textwidth}
        \only<1>{\includegraphics[width=\textwidth]{czts_ks.pdf}}
        \onslide<2>{\includegraphics[width=\textwidth]{czts_plane.pdf}}
    \column{0.4\textwidth}
   \end{columns}
\end{frame}
%
\begin{frame}{CZTSSe thin-film photovoltaics}
    \begin{columns}[c]
    \column{0.5\textwidth}
        \begin{itemize}
        \item {Earth-abundant elements.}
        \item {Low-cost synthesis.}
        \item {Non-toxic materials.}
        \item {Favorable band gap: 1.3 -- 1.5 eV.} 
        \end{itemize}
    \column{0.5\textwidth}
        \includegraphics[width=\textwidth]{device.pdf}
    \end{columns}
\end{frame}
%
\begin{frame}{Power conversion efficiency}
\includegraphics[width=1.05\textwidth]{pv_eff.pdf}
\end{frame}
%
\begin{frame}{Open-circuit voltage (V$_\text{OC}$) deficit}
    \begin{columns}[c]
    \column{0.45\textwidth}
        \begin{itemize}
        \item {$V_\text{OC}$ {deficit} $= E_g/q - V_\text{OC}$.}
        \item {For CZTS, $V_\text{OC} \sim 600$ meV at best.}
        \item {V$_\text{OC}$ deficit of 900 meV.}
        \item {For reference, V$_\text{OC}$ deficit $\sim 500$ meV for $c$-Si solar cells.} 
        \end{itemize}
    \column{0.55\textwidth}
        \includegraphics[width=\textwidth]{jv.pdf} \\
        \center\framebox{$I = I_{SC} - I_0[\text{exp}(\frac{qV}{k_BT}) - 1]$}
    \end{columns}
\end{frame}
%
\begin{frame}{CTZSSe limited by V$_\text{OC}$ deficit}
\includegraphics[width=0.7\textwidth]{ael.jpg} \\
\medskip \ \\
\begin{flushright}\footnotesize{Wallace, Mitzi, Walsh, ``The steady rise of kesterite solar cells``, 2017}
\end{flushright}
\end{frame}
%
\begin{frame}{Origin of the large V$_\text{OC}$ deficit}
\framebox{\includegraphics[width=1.0\textwidth]{ref_1.pdf}} \\
\begin{flushright}\footnotesize{Rey et al., ``On the origin of band-tails in kesterite``, 2018}
\end{flushright}
\medskip \ \\
\framebox{\includegraphics[width=1.0\textwidth]{ref_2.pdf}} \\
\begin{flushright}\footnotesize{Gershon et al., ``Photovoltaic device with over 5\% efficiency based on an n‐-type Ag2ZnSnSe4 absorber``, 2016}
\end{flushright}
\end{frame}
%
\begin{frame}{Cation substitution with Cd}
\begin{columns}
    \column{0.6\textwidth}
    \includegraphics[width=\textwidth]{wong_iv.pdf} \\
    \center\footnotesize{Figure from L. H. Wong NTU of Singapore, unpublished}
    \column{0.4\textwidth}
    Replacing Zn with Cd mitigates the $V_\text{OC}$ deficit. \\
    \begin{itemize}
    \item CZTS: $E_g = 1.5$ eV, $V_\text{OC} = 0.6$ eV
    \item CCTS: $E_g = 1.2$ eV, $V_\text{OC} = 0.6$ eV
    \item Higher $J_\text{SC}$ achieved with CCTS, implying higher power.
    \end{itemize}
\end{columns}
\end{frame}
%
\begin{frame}{Cation substitution with Cd}
\begin{columns}
    \column{0.6\textwidth}
    \only<1>{\includegraphics[width=\textwidth]{wong_eqe.pdf} \\}
    \only<2>{\includegraphics[width=\textwidth]{wong_eqe_2.pdf} \\}
    \center\footnotesize{Figure from L. H. Wong NTU of Singapore, unpublished}
    \column{0.4\textwidth}
    \begin{itemize}
    \item<1->{External quantum efficiency (EQE) $= \frac{\text{electrons/sec}}{\text{photons/sec}}$ \\}
    \item<2->{Sharper band edges $\rightarrow$ less band tailing with Cu$_2$CdSnS$_4$.}
    \end{itemize}
\end{columns}
\end{frame}
%
\section{Order-disorder transition}
\begin{frame}{Take one: Order-disorder transition}
\begin{itemize}
\item Monte Carlo simulation in canonical emsemble 
\item Effective cluster interactions (ECIs) from Connolly-Williams cluster-expansion method
\begin{gather}
    \begin{pmatrix} E_1 \\ E_2 \\  E_3 \\ \vdots \\ E_M \end{pmatrix}
    =
    \begin{pmatrix} \Pi_{11} & \Pi_{12} & \Pi_{13} & \cdots & \Pi_{1N} \\
                    \Pi_{21} & \Pi_{22} & \Pi_{23} & \cdots & \Pi_{2N} \\
                    \Pi_{31} & \Pi_{32} & \Pi_{33} & \cdots & \Pi_{3N} \\
                    \vdots   & \vdots   & \vdots   & \ddots & \vdots   \\ 
                    \Pi_{M1} & \Pi_{M2} & \Pi_{M3} & \cdots & \Pi_{MN} \end{pmatrix}
    \begin{pmatrix} \xi_1   \\ \xi_2 \\ \xi_3 \\ \vdots \\ \xi_N \end{pmatrix} \nonumber
\end{gather} 
    $E_i$: energy of the $i$-th structure \\
    $\Pi_{i,j}$: correlation function of the $j$-th cluster on the $i$-th structure \\
    $\xi_j$: ECI of the $j$-th cluster
\end{itemize}
\end{frame}
%
\begin{frame}{Order-disorder transition: CZTS vs CCTS}
\center
\only<1>{\includegraphics[width=0.7\textwidth]{tc_czts.pdf}}
\only<2>{\includegraphics[width=0.7\textwidth]{tc_ccts.pdf}}
\end{frame}
%
\begin{frame}{Order-disorder transition: CZTS vs CCTS}
\center
\only<1>{\includegraphics[width=0.7\textwidth]{order.pdf}}
\end{frame}
%
\begin{frame}
\frametitle{Order-disorder transition: CZTS vs CCTS}
\begin{columns}
    \column{0.45\textwidth}
    \center\includegraphics[width=\textwidth]{occ_czts_0.pdf}
    \onslide<2->{\center\includegraphics[width=0.85\textwidth]{czts_od.pdf}}
    \column{0.10\textwidth}
    \includegraphics[width=\textwidth]{sites_czts.png}
    \vspace{10mm} \ \\
    \column{0.45\textwidth}
    \center\includegraphics[width=\textwidth]{occ_ccts_0.pdf}
    \onslide<3->{\center\includegraphics[width=0.85\textwidth]{ccts_od.pdf}}
\end{columns}
\end{frame}
%
\begin{frame}{Stannite solar cell(?)}
\begin{itemize}
\item Lack of second-order phase transition.
\item Easier to equilibrate into the ordered structure as the sample is cooled down from the synthesis temperature at 850 K. \\
\end{itemize}
\end{frame}
%
\begin{frame}{Stannite solar cell(?)}
\center\includegraphics[width=0.55\textwidth]{xrd.png} 
\begin{flushright}\footnotesize{L. H. Wong, unpublished}
\end{flushright}
\end{frame}
%
\begin{frame}{Prospects}
\begin{itemize}
\item Non-stoichiometric effect
\item Origin of band tailing (disorder, point defects)
\item Absorber/buffer-layer interface, band alignment
\end{itemize}
\end{frame}
%
\end{document}
