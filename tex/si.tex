\documentclass[aps,amsmath,prb,reprint,floatfix,superscriptaddress,footinbib]{revtex4-2} 

\usepackage{graphicx}% include figure files
\usepackage{float}
\usepackage{dcolumn}% align table columns on decimal point
\usepackage{bm}% bold math
\usepackage[breaklinks,colorlinks,linkcolor={blue},
            citecolor={blue},urlcolor={blue}]{hyperref}
\usepackage[version=4]{mhchem}
\usepackage{numprint}
\usepackage{newtxtext, newtxmath}
\usepackage{siunitx}
\sisetup{retain-zero-exponent=true}

\renewcommand{\thefigure}{S\arabic{figure}}
\renewcommand{\thetable}{S\arabic{table}}

\begin{document}
\title{\textsc{supplementary information}\\
Origin of the low conversion efficiency in \ce{Cu2ZnSnS4} kesterite solar cells:
the actual role of cation disorder}

\author{Wei Chen}
\affiliation{Institute of Condensed Matter and Nanoscicence (IMCN), 
Universit\'{e} catholique de Louvain, 
Louvain-la-Neuve 1348, Belgium}
\author{Diana Dahliah}
\affiliation{Institute of Condensed Matter and Nanoscicence (IMCN),
Universit\'{e} catholique de Louvain,
Louvain-la-Neuve 1348, Belgium}
\author{Gian-Marco Rignanese}
\affiliation{Institute of Condensed Matter and Nanoscicence (IMCN), 
Universit\'{e} catholique de Louvain, 
Louvain-la-Neuve 1348, Belgium}
\author{Geoffroy Hautier}
\affiliation{Institute of Condensed Matter and Nanoscicence (IMCN), 
Universit\'{e} catholique de Louvain, 
Louvain-la-Neuve 1348, Belgium}
\affiliation{Thayer School of Engineering, Dartmouth College, 
Hanover, New Hampshire 03755, USA}
\maketitle

Figure~\ref{fig:gofr_mc} shows the radial distribution functions $g(r)$ for the
ordered kesterite, the cation-disordered special supercell, and the Monte-Carlo
snapshot at 800 K. The overall agreement between the special supercell and the 
Monte-Carlo snapshot is demonstrated.

\bigskip

Figures~\ref{fig:pd_cu} and \ref{fig:pd_s} show the phase stability of \ce{Cu2ZnSnS4}
under various growth conditions.
The values reported in the main text refer to the growth condition P1 (Cu-poor).

\bigskip

Figure~\ref{fig:def_level_all} shows the formation energies for a number of defects in \ce{Cu2ZnSnS4}
under the various growth conditions given in Figs.~\ref{fig:pd_cu} and \ref{fig:pd_s}.

\bigskip

Table~\ref{tab:defect_all} gives the defect concentration and hole concentration at equilibrium 
Fermi energy under the various growth conditions.
\vfill\eject

\begin{figure}
\includegraphics{gofr_mc.pdf}
\caption{\label{fig:gofr_mc}Radial distribution functions $g(r)$ for the ordered kesterite, 
        the 288-atom special supercell (SC), and the Monte-Carlo (MC) snapshot at 800 K. 
        The bars are slightly displaced horizontally to allow for an easy comparison.}
\end{figure}

\clearpage
        
\begin{figure}[H]
\includegraphics{pd_cu.pdf}
\caption{\label{fig:pd_cu}Chemical potential diagrams for (cation disordered) \ce{Cu2ZnSnS4} calculated 
        with HSE hybrid functional at varying Cu chemical potentials.
        The growth conditions range from Cu-rich ($\Delta\mu_\text{Cu}=[0,-0.20]$ eV) to 
        Cu-poor ($\Delta\mu_\text{Cu}=[-0.50,-0.67]$ eV) conditions. 
        The region where \ce{Cu2ZnSnS4} is stable is shaded. The representative boundary
        vertices are labeled (R1 and R2 under Cu-rich conditions, P1 and P2 under Cu-poor conditions).}
\end{figure}

\begin{figure}[H]
\includegraphics{pd_s.pdf}
\caption{\label{fig:pd_s}Chemical potential diagrams at varying S chemical potentials.
        The growth conditions range from S-rich ($\Delta\mu_\text{Cu}=[0,-0.30]$ eV) to 
        S-poor ($\Delta\mu_\text{Cu}=[-0.60,-0.85]$ eV) conditions. Two representative vertices
        are labeled (RS for S-rich and PS for P-poor conditions). 
        The shade color refers to the chemical potential of Cu.
        Dark (bright) blue corresponds to a Cu-rich (poor) condition.}
\end{figure}


\begin{figure*}
\includegraphics{def_level_all.pdf}
\caption{\label{fig:def_level_all}Defect formation energies for \ce{Cu2ZnSnS4} obtained with HSE
        hybrid-functional calculations under various growth conditions (cf.\ Fig.~\ref{fig:pd_cu} and \ref{fig:pd_s}).
        The results for disordered and ordered systems are shown by solid and dashed lines, 
        respectively.}
\end{figure*}


\squeezetable
\begingroup
\def\arraystretch{1.4}
\begin{table*}
\caption{\label{tab:defect_all} Defect concentration $N$ and hole concentration $p_0$ 
         at equilibrium Fermi energy $E_\text{F}$ (referred to the VBM) under various growth conditions for 
         ordered and cation-disordered CZTS.}
\begin{ruledtabular}
\begin{tabular}{lllllllllllll}
   & \multicolumn{6}{c}{Ordered} 
   & \multicolumn{6}{c}{Disordered} \\
\cline{2-7}
\cline{8-13}
   &  P1 
   &  P2 
   &  R1 
   &  R2 
   &  RS
   &  PS
   &  P1 
   &  P2 
   &  R1 
   &  R2 
   &  RS 
   &  PS\\
\hline
$E_\text{F}$ (eV)                                & 0.52         & 0.05         & 0.54         & 0.05         & 0.26        & 0.41          & 0.25         & 0.00        & 0.27          & 0.00         & 0.00         & 0.14        \\
$p_0$ (cm$^{-3}$)                                 & \num{3e+10} & \num{3e+18} & \num{1e+10} & \num{2e+18} & \num{6e+14} & \num{2e+12} & \num{1e+15} & \num{2e+19} & \num{5e+14} & \num{2e+19} & \num{2e+19} & \num{7e+16} \\
\rule{0pt}{4ex}$N$ (cm$^{-3}$)                   & \\
$V_\text{Cu}^{-1}$                                & \num{2e+18} & \num{2e+15} & \num{2e+15} & \num{2e+12} & \num{2e+17} & \num{3e+15} & \num{4e+19} & \num{1e+18} & \num{4e+16} & \num{9e+14} & \num{5e+18} & \num{6e+16} \\
$V_\text{Cu}^{0}$                                 & \num{2e+15} & \num{2e+15} & \num{2e+12} & \num{2e+12} & \num{1e+16} & \num{1e+13} & \num{3e+18} & \num{3e+18} & \num{2e+15} & \num{2e+15} & \num{1e+19} & \num{2e+16} \\
$V_\text{Zn}^{-2}$                                & \num{4e+12} & \num{4e+09} & \num{4e+09} & \num{3e+06} & \num{4e+11} & \num{5e+09} & \num{1e+16} & \num{1e+16} & \num{1e+13} & \num{7e+12} & \num{2e+15} & \num{2e+13} \\
$V_\text{Zn}^{-1}$                                & \num{5e+10} & \num{6e+10} & \num{4e+07} & \num{4e+07} & \num{2e+11} & \num{3e+08} & \num{4e+15} & \num{1e+17} & \num{3e+12} & \num{8e+13} & \num{2e+16} & \num{3e+13} \\
$V_\text{Zn}^{0}$                                 & \num{9e+07} & \num{9e+10} & \num{5e+04} & \num{6e+07} & \num{2e+10} & \num{3e+06} & \num{9e+13} & \num{1e+17} & \num{5e+10} & \num{6e+13} & \num{2e+16} & \num{3e+12} \\
$V_\text{Sn}^{-3}$                                & \num{7e-10} & \num{4e-13} & \num{4e-13} & \num{3e-16} & \num{3e-10} & \num{6e-14} & \num{2e-03} & \num{3e-02} & \num{1e-06} & \num{1e-05} & \num{1e-03} & \num{2e-07} \\
$V_\text{Sn}^{-2}$                                & \num{1e-08} & \num{8e-09} & \num{6e-12} & \num{5e-12} & \num{2e-07} & \num{5e-12} & \num{7e-03} & \num{3e+00} & \num{3e-06} & \num{2e-03} & \num{2e-01} & \num{3e-06} \\
$V_\text{Sn}^{-1}$                                & \num{2e-04} & \num{1e-01} & \num{6e-08} & \num{6e-05} & \num{1e-01} & \num{3e-07} & \num{4e-01} & \num{8e+03} & \num{2e-04} & \num{3e+00} & \num{4e+02} & \num{9e-04} \\
$V_\text{Sn}^{0}$                                 & \num{1e-10} & \num{9e-05} & \num{4e-14} & \num{4e-08} & \num{5e-06} & \num{1e-12} & \num{4e-04} & \num{2e+02} & \num{1e-07} & \num{1e-01} & \num{1e+01} & \num{4e-06} \\
$\text{Cu}_\text{Zn}^{-1}$                        & \num{5e+19} & \num{6e+19} & \num{5e+19} & \num{6e+19} & \num{5e+19} & \num{6e+19} & \num{8e+21} & \num{2e+23} & \num{8e+21} & \num{2e+23} & \num{1e+22} & \num{8e+21} \\
$\text{Cu}_\text{Zn}^{0}$                         & \num{5e+17} & \num{5e+20} & \num{4e+17} & \num{5e+20} & \num{2e+19} & \num{2e+18} & \num{1e+21} & \num{1e+24} & \num{1e+21} & \num{1e+24} & \num{6e+22} & \num{6e+21} \\
$\text{Cu}_\text{Sn}^{-3}$                        & \num{6e+02} & \num{4e-01} & \num{6e+02} & \num{4e-01} & \num{7e+01} & \num{9e+00} & \num{2e+07} & \num{3e+08} & \num{2e+07} & \num{2e+08} & \num{3e+06} & \num{3e+05} \\
$\text{Cu}_\text{Sn}^{-2}$                        & \num{5e+07} & \num{3e+07} & \num{3e+07} & \num{3e+07} & \num{2e+08} & \num{3e+06} & \num{7e+11} & \num{3e+14} & \num{5e+11} & \num{2e+14} & \num{4e+12} & \num{5e+10} \\
$\text{Cu}_\text{Sn}^{-1}$                        & \num{2e+07} & \num{1e+10} & \num{8e+06} & \num{7e+09} & \num{3e+09} & \num{5e+06} & \num{2e+11} & \num{4e+15} & \num{1e+11} & \num{3e+15} & \num{5e+13} & \num{7e+10} \\
$\text{Cu}_\text{Sn}^{0}$                         & \num{3e+06} & \num{2e+12} & \num{1e+06} & \num{1e+12} & \num{2e+10} & \num{4e+06} & \num{2e+11} & \num{1e+17} & \num{6e+10} & \num{6e+16} & \num{1e+15} & \num{2e+11} \\
$\text{Zn}_\text{Sn}^{-2}$                        & \num{5e+13} & \num{3e+10} & \num{4e+13} & \num{3e+10} & \num{5e+12} & \num{7e+11} & \num{5e+16} & \num{2e+16} & \num{4e+16} & \num{2e+16} & \num{7e+15} & \num{6e+14} \\
$\text{Zn}_\text{Sn}^{-1}$                        & \num{3e+12} & \num{2e+12} & \num{2e+12} & \num{1e+12} & \num{1e+13} & \num{2e+11} & \num{5e+16} & \num{8e+17} & \num{3e+16} & \num{6e+17} & \num{3e+17} & \num{3e+15} \\
$\text{Zn}_\text{Sn}^{0}$                         & \num{7e+09} & \num{4e+12} & \num{3e+09} & \num{3e+12} & \num{1e+12} & \num{2e+09} & \num{2e+15} & \num{1e+18} & \num{1e+15} & \num{1e+18} & \num{4e+17} & \num{7e+14} \\
$\text{Zn}_\text{Cu}^{0}$                         & \num{3e+14} & \num{2e+11} & \num{3e+14} & \num{3e+11} & \num{6e+12} & \num{5e+13} & \num{4e+17} & \num{4e+14} & \num{6e+17} & \num{5e+14} & \num{1e+16} & \num{8e+16} \\
$\text{Zn}_\text{Cu}^{+1}$                        & \num{6e+19} & \num{5e+19} & \num{6e+19} & \num{5e+19} & \num{6e+19} & \num{5e+19} & \num{8e+21} & \num{3e+20} & \num{8e+21} & \num{3e+20} & \num{7e+21} & \num{8e+21} \\
$\text{Sn}_\text{Cu}^{0}$                         & \num{2e+03} & \num{3e-03} & \num{4e+03} & \num{4e-03} & \num{2e-01} & \num{1e+03} & \num{5e+07} & \num{8e+01} & \num{1e+08} & \num{1e+02} & \num{6e+03} & \num{3e+07} \\
$\text{Sn}_\text{Cu}^{+1}$                        & \num{5e+08} & \num{9e+05} & \num{1e+09} & \num{1e+06} & \num{3e+06} & \num{2e+09} & \num{3e+12} & \num{2e+08} & \num{6e+12} & \num{3e+08} & \num{1e+10} & \num{9e+12} \\
$\text{Sn}_\text{Cu}^{+2}$                        & \num{1e+06} & \num{2e+06} & \num{2e+06} & \num{2e+06} & \num{2e+05} & \num{1e+07} & \num{4e+12} & \num{8e+09} & \num{6e+12} & \num{1e+10} & \num{6e+11} & \num{6e+13} \\
$\text{Sn}_\text{Cu}^{+3}$                        & \num{4e+03} & \num{6e+06} & \num{4e+03} & \num{7e+06} & \num{4e+04} & \num{3e+05} & \num{2e+12} & \num{2e+11} & \num{3e+12} & \num{3e+11} & \num{1e+13} & \num{2e+14} \\
$\text{Sn}_\text{Zn}^{0}$                         & \num{3e+10} & \num{5e+07} & \num{6e+10} & \num{6e+07} & \num{2e+08} & \num{9e+10} & \num{1e+11} & \num{2e+08} & \num{2e+11} & \num{3e+08} & \num{6e+08} & \num{4e+11} \\
$\text{Sn}_\text{Zn}^{+1}$                        & \num{5e+11} & \num{8e+11} & \num{8e+11} & \num{1e+12} & \num{1e+11} & \num{8e+12} & \num{9e+13} & \num{6e+12} & \num{1e+14} & \num{7e+12} & \num{2e+13} & \num{1e+15} \\
$\text{Sn}_\text{Zn}^{+2}$                        & \num{2e+12} & \num{4e+15} & \num{3e+12} & \num{4e+15} & \num{2e+13} & \num{2e+14} & \num{1e+16} & \num{3e+16} & \num{2e+16} & \num{4e+16} & \num{1e+17} & \num{1e+18} \\
\rule{0pt}{4ex}$N$ (cm$^{-3}$)                   & \\
$[\text{Sn}_\text{Cu}+\text{Cu}_\text{Sn}]^{-2}$  & \num{2e-03} & \num{2e-09} & \num{4e-03} & \num{3e-09} & \num{1e-06} & \num{9e-05} & \num{4e+01} & \num{3e-02} & \num{8e+01} & \num{3e-02} & \num{3e-02} & \num{2e+00} \\
$[\text{Sn}_\text{Cu}+\text{Cu}_\text{Sn}]^{-1}$  & \num{4e+00} & \num{4e-03} & \num{6e+00} & \num{5e-03} & \num{1e-01} & \num{9e-01} & \num{1e+07} & \num{4e+05} & \num{2e+07} & \num{4e+05} & \num{4e+05} & \num{3e+06} \\
$[\text{Sn}_\text{Cu}+\text{Cu}_\text{Sn}]^{0}$   & \num{1e+03} & \num{1e+03} & \num{1e+03} & \num{1e+03} & \num{1e+03} & \num{1e+03} & \num{7e+11} & \num{7e+11} & \num{7e+11} & \num{7e+11} & \num{7e+11} & \num{7e+11} \\
$[\text{Sn}_\text{Cu}+\text{Cu}_\text{Sn}]^{+1}$  & \num{4e+03} & \num{4e+06} & \num{3e+03} & \num{3e+06} & \num{2e+05} & \num{2e+04} & \num{7e+10} & \num{2e+12} & \num{5e+10} & \num{2e+12} & \num{2e+12} & \num{3e+11} \\
$[\text{Sn}_\text{Zn}+\text{Zn}_\text{Sn}]^{-1}$  & \num{3e+08} & \num{3e+05} & \num{4e+08} & \num{4e+05} & \num{8e+06} & \num{7e+07} & \num{1e+11} & \num{3e+09} & \num{2e+11} & \num{3e+09} & \num{3e+09} & \num{2e+10} \\
$[\text{Sn}_\text{Zn}+\text{Zn}_\text{Sn}]^{0}$   & \num{1e+13} & \num{1e+13} & \num{1e+13} & \num{1e+13} & \num{1e+13} & \num{1e+13} & \num{2e+16} & \num{2e+16} & \num{2e+16} & \num{2e+16} & \num{2e+16} & \num{2e+16} \\
$[\text{Sn}_\text{Zn}+\text{Zn}_\text{Sn}]^{+1}$  & \num{4e+09} & \num{4e+12} & \num{3e+09} & \num{4e+12} & \num{2e+11} & \num{2e+10} & \num{1e+14} & \num{5e+15} & \num{1e+14} & \num{5e+15} & \num{5e+15} & \num{7e+14} \\
$[\text{Sn}_\text{Zn}+V_\text{Zn}]^{-2}$          & \num{7e+01} & \num{1e-04} & \num{1e-01} & \num{1e-07} & \num{4e-02} & \num{3e-01} & \num{9e+08} & \num{1e+06} & \num{2e+06} & \num{1e+03} & \num{7e+05} & \num{3e+06} \\
$[\text{Sn}_\text{Zn}+V_\text{Zn}]^{-1}$          & \num{2e+05} & \num{4e+02} & \num{3e+02} & \num{3e-01} & \num{5e+03} & \num{4e+03} & \num{3e+12} & \num{1e+11} & \num{4e+09} & \num{1e+08} & \num{8e+10} & \num{6e+10} \\
$[\text{Sn}_\text{Zn}+V_\text{Zn}]^{0}$           & \num{1e+08} & \num{2e+08} & \num{1e+05} & \num{2e+05} & \num{1e+08} & \num{1e+07} & \num{4e+15} & \num{8e+15} & \num{5e+12} & \num{6e+12} & \num{4e+15} & \num{4e+14} \\
$[\text{Sn}_\text{Zn}+2V_\text{Cu}]^{-2}$         & \num{3e+02} & \num{5e-07} & \num{6e-04} & \num{4e-13} & \num{2e-02} & \num{2e-03} & \num{1e+04} & \num{2e-02} & \num{2e-02} & \num{1e-08} & \num{9e-01} & \num{6e-02} \\
$[\text{Sn}_\text{Zn}+2V_\text{Cu}]^{-1}$         & \num{1e+07} & \num{2e+01} & \num{2e+01} & \num{2e-05} & \num{3e+04} & \num{4e+02} & \num{3e+09} & \num{2e+05} & \num{4e+03} & \num{1e-01} & \num{9e+06} & \num{8e+04} \\
$[\text{Sn}_\text{Zn}+2V_\text{Cu}]^{0}$          & \num{7e+10} & \num{1e+08} & \num{7e+04} & \num{8e+01} & \num{7e+09} & \num{8e+06} & \num{2e+14} & \num{3e+11} & \num{2e+08} & \num{2e+05} & \num{2e+13} & \num{2e+10} \\
$[\text{Sn}_\text{Zn}+2\text{Cu}_\text{Zn}]^{-2}$ & \num{1e+06} & \num{2e+03} & \num{2e+06} & \num{2e+03} & \num{6e+03} & \num{3e+06} & \num{3e+11} & \num{5e+11} & \num{7e+11} & \num{5e+11} & \num{3e+09} & \num{1e+12} \\
$[\text{Sn}_\text{Zn}+2\text{Cu}_\text{Zn}]^{-1}$ & \num{5e+09} & \num{1e+10} & \num{8e+09} & \num{1e+10} & \num{1e+09} & \num{8e+10} & \num{2e+15} & \num{1e+17} & \num{3e+15} & \num{1e+17} & \num{6e+14} & \num{3e+16} \\
$[\text{Sn}_\text{Zn}+2\text{Cu}_\text{Zn}]^{0}$  & \num{6e+12} & \num{1e+16} & \num{7e+12} & \num{1e+16} & \num{6e+13} & \num{5e+14} & \num{4e+18} & \num{7e+21} & \num{4e+18} & \num{7e+21} & \num{4e+19} & \num{3e+20} \\
$[\text{Sn}_\text{Zn}+2\text{Cu}_\text{Zn}]^{+1}$ & \num{8e+09} & \num{1e+16} & \num{6e+09} & \num{1e+16} & \num{3e+12} & \num{3e+12} & \num{5e+16} & \num{4e+21} & \num{5e+16} & \num{4e+21} & \num{2e+19} & \num{2e+19} \\
$[\text{Sn}_\text{Zn}+\text{Cu}_\text{Zn}]^{-1}$  & \num{4e+08} & \num{8e+05} & \num{9e+08} & \num{1e+06} & \num{2e+06} & \num{1e+09} & \num{2e+11} & \num{9e+09} & \num{4e+11} & \num{1e+10} & \num{1e+09} & \num{6e+11} \\
$[\text{Sn}_\text{Zn}+\text{Cu}_\text{Zn}]^{0}$   & \num{3e+11} & \num{5e+11} & \num{4e+11} & \num{6e+11} & \num{7e+10} & \num{5e+12} & \num{2e+14} & \num{4e+14} & \num{4e+14} & \num{5e+14} & \num{6e+13} & \num{4e+15} \\
$[\text{Sn}_\text{Zn}+\text{Cu}_\text{Zn}]^{+1}$  & \num{1e+13} & \num{2e+16} & \num{1e+13} & \num{2e+16} & \num{1e+14} & \num{9e+14} & \num{3e+16} & \num{2e+18} & \num{4e+16} & \num{3e+18} & \num{3e+17} & \num{3e+18} \\
\end{tabular}                                     
\end{ruledtabular}                
\end{table*}
\endgroup
\end{document} 
