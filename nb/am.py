#!/usr/bin/env python3

import numpy as np
from scipy.interpolate import interp1d
from math import pi,exp,log,sqrt
import matplotlib.pyplot as plt
import pandas as pd

# constants
d = 2E-4 # in cm
q = 1.60217662E-19 
kb = 1.38064852e-23
m0 = 9.1093837e-31
h = 6.62607004e-34
T = 300

sigma_n1 = 1.2E-14 # Sn_Zn
sigma_p1 = 3.2E-15  
sigma_n2 = 1.5E-13 # Sn_Zn + Cu_Zn
sigma_p2 = sigma_p1

def initialize(sys):
    global p0, nt1, nt2, evt1, etc1, evt2, etc2, eg
    if sys == 'order':
        eg = 1.5
        p0 = 3E10
        nt1 = 2E12 # Sn_Zn
        nt2 = 1E13 # Sn_Zn + Cu_Zn
        evt1 = -0.6
        etc1 = -(eg+evt1)
        evt2 = -0.8
        etc2 = -(eg+evt2)
    elif sys == 'disorder':
        eg = 1.5
        p0 = 1E15
        nt1 = 1E16
        nt2 = 3E16
        evt1 = -0.6
        etc1 = -(eg+evt1)
        evt2 = -0.6
        etc2 = -(eg+evt2)

def calc_jsc():
    jsc = np.trapz((1 - np.exp(-2*abs_coef*d))*influx, omega)
    return jsc

def calc_jrad(qv):
    hc = 1.239841E-6 # eV m
    c = 299792458 # m/s
    bb = 2*pi*omega**2*c/hc**3/(np.exp(omega/0.026)-1)*1E-4 # 1/eV/cm^2/s
    bb *= q * 1E3 # mA/cm^2/eV 
    #_g = np.where(omega > eg)[0]
    #jrad = np.trapz(bb[_g], omega[_g])
    jrad0 = np.trapz(bb*(1 - np.exp(-2*abs_coef*d)), omega)
    jrad = jrad0*np.exp(qv/0.026)-1
    return jrad, jrad0

def calc_density_effective(m_eff):
    return 2*(2*pi*m_eff*m0*kb*T/h**2)**(3./2)*1e-6

def calc_ni(eg, n_c, n_v):
    return sqrt(n_c*n_v*exp(-eg/8.617333e-5/T))

def calc_dn(qv, ni):
    return 0.5*(-p0+np.sqrt(p0**2-4*ni**2*(1-np.exp(qv/8.617333e-5/T))))

def calc_rate(e_tc, e_vt, n0, p0, dn, dp, n_c, n_v, tau_n, tau_p):
    num = (n0+dn)*(p0+dp) - n0*p0
    den = tau_n*((n0+dn)+n_c*exp(e_tc/(8.617333e-5*T)))
    den += tau_p*((p0+dp)+n_v*exp(e_vt/(8.617333e-5*T)))
    return num/den

def calc_tau(nt,sigma):
    return 1/(nt*sigma*1E7)

def calc_jsrh(eg, qv, tau, p0, n_c, n_v):
    if sys == 'disorder':
        j0 = q*d*n_c*n_v/p0/tau*exp(-eg/0.026)*1000
        jsrh = j0*(np.exp(qv/0.026)-1)
    elif sys == 'order':
        j0 = q*d*sqrt(n_c*n_v)/tau*exp(-eg/0.026/2)*1000/2
        jsrh = j0*(np.exp(qv/2/0.026)-1)
    return jsrh, j0

def plot_jv(jsc, jrad, jsrh):
    j_sq = jsc - jrad
    j = jsc - jrad - jsrh
    p = j*v
    print("Efficiency: {0:.2f}".format(p.max()/sun))
    plt.plot(v, j_sq, '--')
    plt.plot(v, j)
    plt.ylim(0,j_sq[0]+5)
    plt.xlim(0,voc+0.1)
    plt.xlabel('V (Volts)')
    plt.ylabel('J (mA/cm^2)')
    plt.show()

def jsrh_kim():
    n_c = calc_density_effective(0.17)
    n_v = calc_density_effective(0.71)
    ni = calc_ni(eg, n_c, n_v)
    n0 = ni**2/p0
    dn = calc_dn(v,ni)
    dp = dn
    print("--Kim's model--")
    print("Nc: {0:.2e}, Nv: {1:.2e}, ni: {2:.2e}, p0: {3:.2e}, n0: {4:.2e}".format(n_c, n_v, ni, p0, n0))

    tau_n = calc_tau(nt1,sigma_n1)
    tau_p = calc_tau(nt1,sigma_p1)
    rate1 = calc_rate(evt1, etc1, n0, p0, dn, dp, n_c, n_v, tau_n, tau_p)
    print("tau_1: {0:.2e}".format((dn/rate1).mean()))
    tau_n = calc_tau(nt2,sigma_n2)
    tau_p = calc_tau(nt2,sigma_p2)
    rate2 = calc_rate(evt2, etc2, n0, p0, dn, dp, n_c, n_v, tau_n, tau_p)
    print("tau_2: {0:.2e}".format((dn/rate2).mean()))
    rate = rate1 + rate2
    tau = dn/rate
    print("tau_s: {0:.2e} +- {1:.2e}".format(tau.mean(), tau.std()))
    jsrh = q*rate*1000

    plot_jv(jsc, jrad, jsrh)

def jsrh_rau():
    n_c = calc_density_effective(0.17)
    n_v = calc_density_effective(0.71)
    ni = calc_ni(eg, n_c, n_v)
    n0 = ni**2/p0
    dn = 1E14
    dp = dn
    print("--Rau's model--")
    print("Nc: {0:.2e}, Nv: {1:.2e}, ni: {2:.2e}, p0: {3:.2e}, n0: {4:.2e}".format(n_c, n_v, ni, p0, n0))

    tau_n = calc_tau(nt1,sigma_n1)
    tau_p = calc_tau(nt1,sigma_p1)
    rate1 = calc_rate(evt1, etc1, n0, p0, dn, dp, n_c, n_v, tau_n, tau_p)
    print("tau_1: {0:.2e}".format(dn/rate1))
    tau_n = calc_tau(nt2,sigma_n2)
    tau_p = calc_tau(nt2,sigma_p2)
    rate2 = calc_rate(evt2, etc2, n0, p0, dn, dp, n_c, n_v, tau_n, tau_p)
    print("tau_2: {0:.2e}".format(dn/rate2))
    rate = rate1 + rate2
    tau = dn/rate
    print("tau_s: {0:.2e}".format(tau))
    
    _, jsrh0 = calc_jsrh(eg,0,tau,p0, n_c, n_v)
    voc2 = 0.026*log(jsc/jsrh0+1)
    jsrh, _ = calc_jsrh(eg, v, tau, p0, n_c, n_v)

    plot_jv(jsc, jrad, jsrh)

def import_am(am='astmg173.csv', whitespace=False):
    am = pd.read_csv(am, delim_whitespace=whitespace)
    am['eV'] = 1240/am['nm']
    int_irrad0 = np.trapz(am['irrad'], am['nm'])
    am['irrad_eV'] = am['irrad']*am['nm']/am['eV']
    am['eV'] = am['eV'][::-1].reset_index(drop=True)
    am['irrad_eV'] = am['irrad_eV'][::-1].reset_index(drop=True)
    int_irrad = np.trapz(am['irrad_eV'], am['eV'])
    am['influx'] = am['irrad_eV']/am['eV']/10  #mA/cm^2
    return am

def import_abs(f='ipa_czts.h5'):
    absp = pd.read_hdf(f)
    eps_r = absp.loc[1].eps_r.mean(axis=1)
    eps_i = absp.loc[1].eps_i.mean(axis=1)
    _omega = absp.loc[1].energy+(eg-0.6)
    kappa = np.sqrt((np.sqrt(eps_r**2+eps_i**2) - eps_r)/2)
    _abs_coef = 4*np.pi*_omega*kappa*8065.5
    _i = np.where((am['eV'] < _omega.max()) & (am['eV'] > _omega.min()))[0]
    omega = am['eV'][_i].values
    influx = am['influx'][_i].values
    _f = interp1d(_omega, _abs_coef)
    abs_coef = _f(omega)
    return omega, abs_coef, influx

## main

sys = 'disorder'
#sys = 'order'
initialize(sys)
am = import_am()
omega, abs_coef, influx = import_abs()

jsc = calc_jsc()
_, jrad0 = calc_jrad(0)
voc = 0.026*log(jsc/jrad0+1)
v = np.linspace(0.0,voc+.1,1000)
jrad, _ = calc_jrad(v)
p = (jsc-jrad)*v
v_pmax, j_pmax = v[p.argmax()], (jsc-jrad)[p.argmax()]
jsc0 = (jsc-jrad)[0]
print("--SQ limit--")
print("Eg: {0:.2f}  Voc_SQ: {1:.2f}".format(eg,voc))
print("V_pmax: {0:.2f}, J_pmax: {1:.2f}, J_sc: {2:.2f}".format(v_pmax, j_pmax, jsc0))
print("FF_SQ: {0:.2f}".format(v_pmax*j_pmax/(jsc0*voc)))
sun = np.trapz(am['influx']*am['eV'],am['eV'])
eff = p.max()/sun
print("Efficiency_SQ: {0:.2f}".format(eff))

jsrh_kim()
jsrh_rau()
