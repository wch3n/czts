#!/usr/bin/env python3
# coding: utf-8

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, AutoMinorLocator
from scipy import interpolate
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score, GridSearchCV

# cluster functions
empty = [0]
point = [1,2]
pair_1 = [3,4]
pair_2 = [5,6,7]
pair_3 = [8,9,10,11]
pair_4 = [12,13,14,15]
triplet_1 = [16,17]

fig_width = 16/2.54
fig_height = fig_width*0.4
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.3,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)

def select_cluster(df_corr, clusters):
    clus = []
    for i in clusters:
        clus = clus + i
    clus = np.array(clus)
    return df_corr.iloc[:,clus]

def regularize(df, grid_fine, smooth_width=0):
    df_fine = pd.DataFrame(index=df.index, columns=['configname', 'energy', 'abs_coef', 'ahv', 'gap'])
    for i in df.index:
        abs_coef = df.loc[i, "abs_coef"]
        ahv = df.loc[i, "ahv"]
        grid_coarse = df.loc[i, "energy"]
        #if smooth_width > 0:
        #    box = np.ones(smooth_width)/smooth_width
        #    abs_coef = np.convolve(dos, box, mode='same')
        f = interpolate.interp1d(grid_coarse,abs_coef)
        df_fine.loc[i,"abs_coef"] = f(grid_fine)
        f = interpolate.interp1d(grid_coarse,ahv)
        df_fine.loc[i,"ahv"] = f(grid_fine)
        df_fine.loc[i,"energy"] = grid_fine
        df_fine.loc[i,"gap"] = df.loc[i, "gap"]
        df_fine.loc[i,"configname"] = df.loc[i, "configname"]
    return df_fine

def weight(df_abs, gap_max, w_exp):
    w = np.exp((gap_max-df_abs.gap.values.astype(float))/w_exp)
    return w

def calc_model(corr,x, w):
    xx = np.concatenate(x.values)
    nabs = len(xx)/len(corr)
    xx = xx.reshape(len(corr), int(nabs))
    ridge = linear_model.Ridge(fit_intercept=False)
    alphas = np.logspace(-5, 1, 10)
    params = [{'alpha': alphas}]
    model = GridSearchCV(ridge, params, scoring='neg_mean_squared_error', cv=5, refit=True, return_train_score= True)
    model.fit(corr.values, xx)
    return model

def match(df_abs, df_corr):
    overlap = list(set(df_abs['configname']).intersection(set(df_corr['configname'])))
    if len(overlap) < len(df_abs['configname']):
        raise ValueError('Data and correlation data inconsistent.')
    df = pd.DataFrame()
    for i in df_abs['configname']:
        df = df.append(df_corr[df_corr['configname'] == i])
    return df

def calc_abs(df):
    df['abs_coef'] = np.nan
    df['abs_coef'] = df['abs_coef'].astype(object)
    df['ahv'] = np.nan
    df['ahv'] = df['ahv'].astype(object)
    for i in df.index:
        eps_r = df.loc[i].eps_r.mean(axis=1)
        eps_i = df.loc[i].eps_i.mean(axis=1)
        #
        #eps_r = df.loc[i].eps_r[:,0]
        #eps_i = df.loc[i].eps_i[:,0]
        #
        omega = df.loc[i].energy
        kappa = np.sqrt((np.sqrt(eps_r**2+eps_i**2) - eps_r)/2)
        abs_coef = 4*np.pi*omega*kappa*8065.5
        df.at[i, 'abs_coef'] = np.log(abs_coef)
        #df.at[i, 'abs_coef'] = abs_coef
        df.at[i, 'ahv'] = (abs_coef * omega)**2
        

def main(mat, clusters, gap_th=0.35, w_exp=10, verbose=False):
    global grid, df_abs, ref_ind
    #grid_fine = np.arange(2,8,0.02) # grid for the selected energy range
    # read corr matrix and electronic densities
    df_corr = pd.read_csv('corr_{0}.csv'.format(mat), delim_whitespace=True)
    df_abs = pd.read_hdf('ipa_{0}.h5'.format(mat))
    # semiconductors only    
    df_abs = df_abs[df_abs['configname'].str.contains('SCEL1|SCEL2|SCEL3')]
    semi = df_abs[(df_abs.gap > gap_th)].index
    df_abs = df_abs.loc[semi]
    df_corr = match(df_abs, df_corr)
    #assert (df_abs.index == df_corr.index).all(), "Index not aligned"
    df_corr = df_corr.set_index(df_abs.index)
    calc_abs(df_abs)
    energy_regular = np.linspace(0.1,1.2,501)
    df_abs = regularize(df_abs, energy_regular)
    #ref_ind = df_dos.gap.idxmax()
    if mat == 'czts':
        ref_ind = 1
        _energy = df_abs.loc[ref_ind, 'energy']
        _abs_ref = df_abs.loc[ref_ind, 'abs_coef']
        np.savetxt('abs_ref.dat', np.array(list(zip(_energy,_abs_ref))))
    else:
        ref_ind = 0
    #
    # interpolate the dos on a specific regular mesh
    #df_dos_fine = regularize_dos(df_dos, grid_fine, smooth_width=smooth_width, log=is_log)
    #
    # keep original mesh and data
    grid = df_abs.iloc[0]['energy']
    # define training set
    #train = df_corr.sample(frac=0.9, random_state=None)
    train = df_corr
    if ref_ind not in train.index:
        train = train.append(df_corr.loc[ref_ind])
    corr_train = train.loc[:,"corr(0)":"corr(17)"]
    abs_train = df_abs.loc[train.index,'abs_coef']
    ahv_train = df_abs.loc[train.index,'ahv']
    #
    corr_train = select_cluster(corr_train, clusters)
    #
    w = weight(df_abs.loc[train.index],df_abs.loc[ref_ind,'gap'],w_exp)
    if verbose:
        display(corr_train)
    model = calc_model(corr_train, abs_train, w)
    abs_train_pred = model.predict(corr_train)
    #print(model.best_estimator_.coef_[ind_vbm:ind_vbm+10].sum(axis=0))

    # plot the real abs coef. (gray) and the predicted ones (red)
    '''plt.rcParams['figure.figsize'] = [10,8]
    fig, (ax0) = plt.subplots(1,1)
    for i in range(len(abs_train)):
        ax0.plot(grid, abs_train.iloc[i]+0.5*i, color='gray')
        ax0.plot(grid, abs_train_pred[i]+0.5*i, color='r')
    ax0.set_xlim([0.2,1.0])
    ax0.set_ylim([0,1e4])
    ax0.set_title("Cluster expanded abs coef: Training set")
    ax0.set_ylabel(r"$\alpha$ (cm$^{-1}$)")
    # plot the eci vs energy
    fig, ax2 = plt.subplots(1)
    for i in range(len(model.best_estimator_.coef_[0,:])):
        ax2.plot(grid, model.best_estimator_.coef_[:,i], label=i)
    ax2.set_ylabel("Effective cluster abs coef.")
    ax2.set_xlim([0.2,1.0])
    ax2.legend()'''
    #
    return df_abs, model

def ec_abs(mat,clusters,model):
    # plot abs coef. vs Temperature using the correlation matrix from MC simulations
    df_mc = pd.read_csv('mc_{0}.csv'.format(mat), delim_whitespace=True)
    df_mc = df_mc[df_mc['T'] <= 1000].reset_index()
    df_mc = df_mc[::2]
    corr_str = []
    for i in range(18):
        corr_str.append("<corr({0:})>".format(i))
    df_corr_mc = df_mc.loc[:,corr_str]
    corr_mc = select_cluster(df_corr_mc, clusters)
    corr_mc = corr_mc.set_index(df_mc['T']).sort_index(ascending=False) # use temperature as index
    abs_mc_pred = model.predict(corr_mc)
    # cluster decomposed contribution
    _eci = model.best_estimator_.coef_.T
    _corr = corr_mc.values
    _ecabs = np.zeros(len(_corr.T))
    corr_fraction = np.zeros((len(model.best_estimator_.coef_),)+corr_mc.shape)
    for i in range(len(corr_fraction)): # energy
        for j in range(len(_corr)):     # temperature
            for k in range(len(_corr.T)): # corr
                _ecabs[k] = _corr[j,k]*_eci[k,i]
            corr_fraction[i,j] = _ecabs
    #
    return corr_fraction, corr_mc, abs_mc_pred

clusters = [empty, point, pair_1, pair_2, triplet_1]
df_abs, model_czts = main("ccts", clusters, gap_th=0.2, w_exp=10, verbose=False)
corr_fraction, corr_mc, abs_mc_pred = ec_abs("ccts",clusters,model_czts)

fig = plt.figure()
gs0 = gridspec.GridSpec(1,1)
gs0.update(left=0.10, right=0.55, top=0.95, bottom=0.15)
ax0 = plt.subplot(gs0[0])
norm = mpl.colors.Normalize(vmin=corr_mc.index.min(), vmax = corr_mc.index.max())
cmap = mpl.cm.get_cmap('plasma')
c = [cmap(norm(temp)) for temp in corr_mc.index]
ax0.set_xlim([0.1, 1.0])
ax0.set_ylim([0.5e2, 5e4])
grid_ = grid

for i in range(len(abs_mc_pred)):
    ax0.plot(grid_, np.exp(abs_mc_pred[i]), color=c[i], lw=0.5)
    #np.savetxt('abs_{0:3g}.dat'.format(corr_mc.index[i]), np.array(list(zip(grid_,(abs_mc_pred[i])))))
# plot the kesterite reference abs coef.
ax0.plot(grid_, np.exp(df_abs.loc[ref_ind, "abs_coef"]), '--', c='k', lw=0.5, label='reference at LT')
#ax0.legend()
ax0.set_xlabel(r'Energy (eV)')
ax0.set_ylabel(r'$\alpha$ (cm$^{-1}$)')
ax0.set_yscale('log')
ax0.text(0.1,0.9, '(a)', fontweight='regular', transform=ax0.transAxes)
#

# Urbach energy (inset)
gap = np.loadtxt('gap_T.dat')
et = np.zeros(len(corr_mc))
i = 0
for t in corr_mc.index:
    eg = gap[gap[:,0] == t,1]
    ma = np.where((grid - eg < 0.12) & (grid >= eg))
    b, _ = np.polyfit(grid[ma], abs_mc_pred[i][ma], 1)
    et[i] = 1/b
    i += 1

c1 = "#69306d"
c2 = "#76427a"
axins = ax0.inset_axes([0.60, 0.20, 0.35, 0.35])
axins.scatter(corr_mc.index, et*1000, edgecolor='k', color=c, lw=0.5, zorder=11)
#axins.plot(corr_mc.index, et*1000, color='gray', lw=0.5, zorder=10)
axins.set_ylim([25,95])
axins.set_xlim([200,800])
axins.set_ylabel('$E_U$ (meV)', fontsize=6)
axins.set_xlabel('T (K)', fontsize=6)
minorLocator = AutoMinorLocator(n=2)
axins.yaxis.set_minor_locator(minorLocator)
axins.yaxis.set_major_locator(MultipleLocator(20))
minorLocator = AutoMinorLocator(n=2)
axins.xaxis.set_minor_locator(minorLocator)
axins.xaxis.set_major_locator(MultipleLocator(200))
axins.xaxis.set_label_coords(0.5,-0.20)
axins.yaxis.set_label_coords(-0.15,0.5)

gs1 = gridspec.GridSpec(1,1)
gs1.update(left=0.65, right=0.98, top=0.95, bottom=0.15)
ax1 = plt.subplot(gs1[0])
for i in range(len(abs_mc_pred)):
    ax1.plot(grid_, np.exp(abs_mc_pred[i])**0.5*grid_**0.5, lw=0.5, color=c[i])

ax1.text(0.1,0.9, '(b)', fontweight='regular', transform=ax1.transAxes)
ax1.set_xlim([0.1,0.8])
ax1.set_ylim([0,200])
ax1.set_xlabel(r'Energy (eV)')
ax1.set_ylabel(r'($\alpha \omega)^{1/2}$ (eV$^{1/2}$cm$^{-1/2}$)')
minorLocator = AutoMinorLocator(n=2)
ax1.yaxis.set_minor_locator(minorLocator)
ax1.yaxis.set_major_locator(MultipleLocator(100))
minorLocator = AutoMinorLocator(n=2)
ax1.xaxis.set_minor_locator(minorLocator)
ax1.xaxis.set_major_locator(MultipleLocator(0.2))
cax, _ = mpl.colorbar.make_axes(ax1)
cbar = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm)

plt.savefig('abs.eps')
