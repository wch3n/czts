#!/usr/bin/env python3
# coding: utf-8

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, AutoMinorLocator
from scipy import interpolate
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.gaussian_process import GaussianProcessRegressor

# cluster functions
empty = [0]
point = [1,2]
pair_1 = [3,4]
pair_2 = [5,6,7]
pair_3 = [8,9,10,11]
pair_4 = [12,13,14,15]
triplet_1 = [16,17]

# adjustables
gap_th = 0.33
w_exp = 0.1
clusters = [empty, point, pair_1, pair_2, triplet_1]

fig_width = 16/2.54
fig_height = fig_width*0.4
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)

def select_cluster(df_corr, clusters):
    clus = []
    for i in clusters:
        clus = clus + i
    clus = np.array(clus)
    return df_corr.iloc[:,clus]

def regularize(df, grid_fine, smooth_width=0):
    df_fine = pd.DataFrame(index=df.index, columns=['configname', 'energy', 'abs_coef', 'ahv', 'gap'])
    for i in df.index:
        abs_coef = df.loc[i, "abs_coef"]
        ahv = df.loc[i, "ahv"]
        grid_coarse = df.loc[i, "energy"]
        #if smooth_width > 0:
        #    box = np.ones(smooth_width)/smooth_width
        #    abs_coef = np.convolve(dos, box, mode='same')
        f = interpolate.interp1d(grid_coarse,abs_coef)
        df_fine.loc[i,"abs_coef"] = f(grid_fine)
        f = interpolate.interp1d(grid_coarse,ahv)
        df_fine.loc[i,"ahv"] = f(grid_fine)
        df_fine.loc[i,"energy"] = grid_fine
        df_fine.loc[i,"gap"] = df.loc[i, "gap"]
        df_fine.loc[i,"configname"] = df.loc[i, "configname"]
    return df_fine

def weight(df_abs, gap_max, w_exp):
    w = np.exp(-(gap_max-df_abs.gap.values.astype(float))/w_exp)
    print(w)
    return w

def calc_model(corr,y,w):
    ridge = linear_model.Ridge(fit_intercept=False)
    alphas = np.logspace(-5, 1, 10)
    params = [{'alpha': alphas}]
    model = GridSearchCV(ridge, params, scoring='neg_mean_squared_error', cv=5, refit=True, return_train_score= True)
    model.fit(corr.values,np.stack(y))
    return model

def calc_model2(corr,y,w):
    alphas = np.logspace(-5, 1, 10)
    model = linear_model.RidgeCV(alphas=alphas, scoring='neg_mean_squared_error',fit_intercept=False, cv=5)
    model.fit(corr.values,np.stack(y),w)
    return model

def gp_model(corr,y):
    model = GaussianProcessRegressor(kernel=None, optimizer='fmin_l_bfgs_b', copy_X_train=True)
    model.fit(corr.values,np.stack(y))
    return model

def match(df_abs, df_corr):
    overlap = list(set(df_abs['configname']).intersection(set(df_corr['configname'])))
    if len(overlap) < len(df_abs['configname']):
        raise ValueError('Data and correlation data inconsistent.')
    df = pd.DataFrame()
    for i in df_abs['configname']:
        df = df.append(df_corr[df_corr['configname'] == i])
    return df

def calc_abs(df):
    df['abs_coef'] = np.nan
    df['abs_coef'] = df['abs_coef'].astype(object)
    df['ahv'] = np.nan
    df['ahv'] = df['ahv'].astype(object)
    for i in df.index:
        eps_r = df.loc[i].eps_r.mean(axis=1)
        eps_i = df.loc[i].eps_i.mean(axis=1)
        #
        #eps_r = df.loc[i].eps_r[:,2]
        #eps_i = df.loc[i].eps_i[:,2]
        #
        omega = df.loc[i].energy
        kappa = np.sqrt((np.sqrt(eps_r**2+eps_i**2) - eps_r)/2)
        abs_coef = 4*np.pi*omega*kappa*8065.5
        #lnabs = np.log(abs_coef)
        #lnabs[np.isneginf(lnabs)] = 1
        #df.at[i, 'abs_coef'] = lnabs
        df.at[i, 'abs_coef'] = abs_coef
        df.at[i, 'ahv'] = (abs_coef * omega)**2

def ec_abs(mat,clusters,model):
    # plot abs coef. vs Temperature using the correlation matrix from MC simulations
    df_mc = pd.read_csv('mc_{0}.csv'.format(mat), delim_whitespace=True)
    df_mc = df_mc[df_mc['T'] <= 1000].reset_index()
    df_mc = df_mc[::2]
    corr_str = []
    for i in range(18):
        corr_str.append("<corr({0:})>".format(i))
    df_corr_mc = df_mc.loc[:,corr_str]
    corr_mc = select_cluster(df_corr_mc, clusters)
    corr_mc = corr_mc.set_index(df_mc['T']).sort_index(ascending=False) # use temperature as index
    abs_mc_pred = model.predict(corr_mc)
    # cluster decomposed contribution
    #_eci = model.best_estimator_.coef_.T
    '''_eci = model.coef_.T
    _corr = corr_mc.values
    _ecabs = np.zeros(len(_corr.T))
    #corr_fraction = np.zeros((len(model.best_estimator_.coef_),)+corr_mc.shape)
    corr_fraction = np.zeros((len(model.coef_),)+corr_mc.shape)
    for i in range(len(corr_fraction)): # energy
        for j in range(len(_corr)):     # temperature
            for k in range(len(_corr.T)): # corr
                _ecabs[k] = _corr[j,k]*_eci[k,i]
            corr_fraction[i,j] = _ecabs'''
    #
    return corr_mc, abs_mc_pred

def main(mat, clusters, gap_th, w_exp, verbose=False):
    df_abs = pd.read_hdf('ipa_{0}.h5'.format(mat))
    calc_abs(df_abs)
    energy_regular = np.linspace(0.0,1.1,1001)
    df_abs = regularize(df_abs, energy_regular)
    # keep original mesh and data
    grid = df_abs.loc[0]['energy']
    return df_abs, grid

df_abs_cztse, grid = main("cztse", clusters, gap_th=gap_th, w_exp=w_exp, verbose=False)
df_abs_czts, grid = main("czts", clusters, gap_th=gap_th, w_exp=w_exp, verbose=False)

fig = plt.figure()
gs0 = gridspec.GridSpec(1,1)
gs0.update(left=0.10, right=0.55, top=0.95, bottom=0.15)
ax0 = plt.subplot(gs0[0])
ax0.set_xlim([0., 0.8])
ax0.set_ylim([1e2, 5e4])

# plot the kesterite reference abs coef.
ax0.plot(grid, (df_abs_cztse.loc[0, "abs_coef"]), '--', c='k', lw=0.5, label='kesterite reference')
ax0.plot(grid, (df_abs_czts.loc[1, "abs_coef"]), '--', c='k', lw=0.5, label='kesterite reference')
#ax0.legend()
ax0.set_xlabel(r'Energy (eV)')
ax0.set_ylabel(r'$\alpha$ (cm$^{-1}$)')
ax0.set_yscale('log')
ax0.text(0.1,0.9, '(a)', fontweight='regular', transform=ax0.transAxes)
#

# Urbach energy (inset)
abs_ = df_abs_cztse.loc[0,'abs_coef']
ma = np.where((abs_ < 2000) & (abs_ > 200))
b = np.polyfit(grid[ma], np.log(abs_[ma]), 1)
et = 1/b[0]
ax0.plot(grid[ma], np.exp(np.poly1d(b)(grid[ma])))
print(et)
abs_ = df_abs_czts.loc[1,'abs_coef']
ma = np.where((abs_ < 2000) & (abs_ > 200))
b = np.polyfit(grid[ma], np.log(abs_[ma]), 1)
et = 1/b[0]
ax0.plot(grid[ma], np.exp(np.poly1d(b)(grid[ma])))
print(et)

'''gs1 = gridspec.GridSpec(1,1)
gs1.update(left=0.65, right=0.98, top=0.95, bottom=0.15)
ax1 = plt.subplot(gs1[0])
for i in range(len(abs_mc_pred)):
    #ax1.plot(grid_, np.exp(abs_mc_pred[i])**0.5*grid_**0.5, lw=0.5, color=c[i])
    #abs_mc_pred[i][abs_mc_pred[i] < 0] = 0
    ax1.plot(grid_, (abs_mc_pred[i])**2*grid_**2, lw=0.5, color=c[i])
#reference
#ax1.plot(grid_, (df_abs.loc[ref_ind, "abs_coef"])**2*grid_**2, '--', c='k', lw=0.5, label='kesterite reference')
# Tauc gap
for i in [i,0]:
    ahv2 = abs_mc_pred[i]**2*grid_**2
    ma = np.where((ahv2 < 0.4e9) & (ahv2 > 0.2e9))
    z = np.polyfit(grid_[ma], ahv2[ma],1)
    p = np.poly1d(z)
    x0 = grid_[ma][0]-0.2
    x1 = grid_[ma][0]+0.05
    _e = np.linspace(x0,x1,100)
    ax1.plot(_e, p(_e), '--', c='k', lw=0.5)

ax1.text(0.1,0.9, '(b)', fontweight='regular', transform=ax1.transAxes)
ax1.set_xlim([0.4,0.9])
ax1.set_ylim([0,1e9])
ax1.set_xlabel(r'Energy (eV)')
ax1.set_ylabel(r'($\alpha h\nu)^{2}$ (eV$^{2}$cm$^{-2}$)')
minorLocator = AutoMinorLocator(n=2)
#ax1.yaxis.set_minor_locator(minorLocator)
#ax1.yaxis.set_major_locator(MultipleLocator(100))
minorLocator = AutoMinorLocator(n=2)
ax1.xaxis.set_minor_locator(minorLocator)
ax1.xaxis.set_major_locator(MultipleLocator(0.2))
cax, _ = mpl.colorbar.make_axes(ax1)
cbar = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm)
'''

plt.savefig('abs.pdf')
