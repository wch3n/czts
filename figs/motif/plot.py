#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
import seaborn as sns

fig_width = 8.5/2.54
fig_height = fig_width*0.7
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

df = pd.read_json('motif_czts.json', orient='table')

fig = plt.figure()
ax = fig.subplots(1,1)

color = ['C0', 'C1', 'C2', 'C3']
count = []
label = []
for i, motif in enumerate(['Cu-Cu-Sn-Zn', 'Cu-Cu-Cu-Sn', 'Cu-Sn-Zn-Zn']):
    f = interpolate.interp1d(df.index, df[motif]/32000, 'linear')
    xx = np.linspace(200,800,16,endpoint=True)
    #ax.plot(df.index, df[motif]/32000, 'o', label=str(motif))
    count.append(f(xx))
    label.append(motif)

ax.stackplot(xx, count[0], count[1], count[2], labels=label, colors=sns.color_palette("GnBu", 4))

ax.set_xlim(200,800)
ax.set_ylim(0,1)

ax.set_xlabel('Temperature (K)')
ax.set_ylabel('Concentration')

ax.legend(loc='lower left')

plt.tight_layout()

plt.savefig('motif_czts.eps')
