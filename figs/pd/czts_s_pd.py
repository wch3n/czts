#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator, AutoMinorLocator

CZTS = -4.65
CZTS += 0.1
ZnS = -1.90
CuS = -0.51
CuS2 = -0.22
SnS = -0.85
SnS2 = -1.29
Cu2SnS3 = -2.52

fig_width = 8.5/2.54
fig_height = fig_width*1
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 5,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

fig = plt.figure(constrained_layout=True)
gs0 = fig.add_gridspec(2,2)
ax0 = fig.add_subplot(gs0[0])
ax1 = fig.add_subplot(gs0[1])
ax2 = fig.add_subplot(gs0[2])
ax3 = fig.add_subplot(gs0[3])
for ax in [ax0,ax1,ax2,ax3]:
    ax.set_xlim([-2., 0])
    ax.set_ylim([-2., 0])
    ax.yaxis.set_major_locator(MultipleLocator(1))

def mu_cu(zn,sn,s):
    czts = np.ones(zn.shape)
    return (CZTS*czts - zn - sn - 4*s)*0.5

def extrema(el1, el2):
    min_idx = np.argmin(el1[out])
    el1_min = el1[out].min()
    el2_at_min = el2[out][min_idx]
    max_idx = np.argmax(el1[out])
    el1_max = el1[out].max()
    el2_at_max = el2[out][max_idx]
    return el1_min, el2_at_min, el1_max, el2_at_max

def main(chem_s, ax, alpha):
    global z_out,out
    _zn = np.arange(-2.5,0,0.005)
    _sn = np.arange(-2.5,0,0.005)
    zn, sn = np.meshgrid(_zn,_sn)
    s  = np.ones(zn.shape)
    s *= chem_s
    cu = mu_cu(zn,sn,s)

    zns = (zn + s < ZnS)
    cus = (cu + s < CuS)
    cus2 = (cu + 2*s < CuS2)
    sns = (sn + s < SnS)
    sns2 = (sn + 2*s < SnS2)
    cu2sns3 = (2*cu + sn + 3*s < Cu2SnS3)
    cu1 = (cu < 0)

    # cus
    f_cus = lambda x: CZTS-2*CuS-2*chem_s-x
    # cu2s
    #f_cu2s = lambda x: CZTS-Cu2S-3*chem_s-x
    # sns
    f_sns = lambda x: SnS-chem_s-x*0
    # sns2
    f_sns2 = lambda x: SnS2-2*chem_s-x*0
    # cu
    f_cu = lambda x: (CZTS-4*chem_s)-x

    out = (np.ones(zn.shape) == 1)
    for i in [zns, cus, cus2, sns, sns2, cu2sns3, cu1]:
        out = np.bitwise_and(out,i)

    z_out = out.astype(float)
    z_out[z_out == 0] = np.nan
    x,y = np.where(out)
    for i, _x in enumerate(x):
        _y = y[i]
        z_out[_x, _y] = (CZTS - zn[0,_x] - sn[_y,0] - 4*s.flatten()[0])*0.5

    sn_min, zn_sn_min, sn_max, zn_sn_max = extrema(sn,zn)
    zn_min, sn_zn_min, zn_max, sn_zn_max = extrema(zn,sn)

    print("[S]: {0:6.2f}".format(chem_s))
    print("[Sn,Zn]: [{0:6.2f} {1:6.2f}] [{2:6.2f} {3:6.2f}]".format(sn_min, zn_sn_min, sn_max, zn_sn_max))
    print("[Sn,Zn]: [{1:6.2f} {0:6.2f}] [{3:6.2f} {2:6.2f}]".format(zn_min, sn_zn_min, zn_max, sn_zn_max))

    ax.pcolormesh(zn,sn,z_out,cmap=cm.Blues,vmin=-0.9, vmax=0.1, alpha=alpha,rasterized=True)
    for _f in [f_cus, f_sns, f_sns2, f_cu]:
        ax.plot(_zn, _f(_zn), '-', c='k', lw=0.4)
        ax.axvline(ZnS-chem_s, c='k', lw=0.4)
        ax.axvline(CZTS-Cu2SnS3-chem_s, c='k', lw=0.4)

main(-0.0, ax0, 1)
main(-0.3, ax1, 1)
main(-0.6, ax2, 1)
main(-0.8, ax3, 1)

props = dict(boxstyle='square,pad=0.5', lw=0.5, edgecolor='gray', facecolor='white', alpha=1)
ax0.text(-1.3+0.5,-2.6+1, '$\Delta \mu_{S}$ = 0', bbox=props)
ax1.text(-1.3+0.5,-2.6+1, '$\Delta \mu_{S}$ = -0.30', bbox=props)
ax2.text(-1.3+0.5,-2.6+1, '$\Delta \mu_{S}$ = -0.60', bbox=props)
ax3.text(-1.3+0.5,-2.6+1, '$\Delta \mu_{S}$ = -0.85', bbox=props)

ax0.text(-1.8,-1.2, 'RS')
ax2.text(-1.25,-0.4, 'PS')

ax2.set_xlabel('$\Delta \mu_{Zn}$ (eV)')
ax3.set_xlabel('$\Delta \mu_{Zn}$ (eV)')
ax0.set_ylabel('$\Delta \mu_{Sn}$ (eV)')
ax2.set_ylabel('$\Delta \mu_{Sn}$ (eV)')

plt.savefig('pd_s.pdf', dpi=600)
