#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import gridspec
from matplotlib.ticker import MultipleLocator, AutoMinorLocator

CCTS = -4.20
CCTS += 0.0
CdS = -1.52
CdS2 = -1.27
CuS = -0.49
CuS2 = -0.22
SnS = -0.83
SnS2 = -1.27
Cu2SnS3 = -2.55

fig_width = 8.5/2.54
fig_height = fig_width*1
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 5,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

fig = plt.figure(constrained_layout=True)
gs0 = fig.add_gridspec(2,2)
ax0 = fig.add_subplot(gs0[0])
ax1 = fig.add_subplot(gs0[1])
ax2 = fig.add_subplot(gs0[2])
ax3 = fig.add_subplot(gs0[3])
for ax in [ax0,ax1,ax2,ax3]:
    ax.set_xlim([-3, 0])
    ax.set_ylim([-3, 0])
    ax.yaxis.set_major_locator(MultipleLocator(1)) 

def mu_s(cd,sn,cu):
    ccts = np.ones(cd.shape)
    return (CCTS*ccts - cd - sn - 2*cu)*0.25

def extrema(el1, el2):
    min_idx = np.argmin(el1[out])
    el1_min = el1[out].min()
    el2_at_min = el2[out][min_idx]
    max_idx = np.argmax(el1[out])
    el1_max = el1[out].max()
    el2_at_max = el2[out][max_idx]
    return el1_min, el2_at_min, el1_max, el2_at_max 

def main(chem_cu, ax, alpha):
    global out
    _cd = np.linspace(-3,0,1001)
    _sn = np.linspace(-3,0,1001)
    cd, sn = np.meshgrid(_cd,_sn)
    cu  = np.ones(cd.shape)
    cu *= chem_cu
    s = mu_s(cd,sn,cu)

    cds = (cd + s < CdS)
    cds2 = (cd+ 2*s < CdS2)
    cus = (cu + s < CuS)
    cus2 = (cu + 2*s < CuS2)
    sns = (sn + s < SnS)
    sns2 = (sn + 2*s < SnS2)
    cu2sns3 = (2*cu + sn + 3*s < Cu2SnS3)
    s1 = (s < 0)
    cd1 = (cd < 0)
    sn1 = (sn < 0)


    # cds
    f_cds = lambda x: 3*x+(CCTS-2*chem_cu-4*CdS)
    # cus
    f_cus = lambda x: (2*chem_cu+CCTS-4*CuS)-x
    # cus2
    f_cus2 = lambda x: (2*CCTS-CuS2-3*chem_cu)*0.5-x
    # sns
    f_sns = lambda x: (4*SnS-CCTS+2*chem_cu)/3.0+x/3.0
    # sns2
    f_sns2 = lambda x: (2*SnS2-CCTS+2*chem_cu)+x
    # cu2sns3
    f_cu2sns3 = lambda x: (4*Cu2SnS3-3*CCTS-2*chem_cu)+3*x
    # s
    f_s = lambda x: (CCTS-2*chem_cu)-x

    out = (np.ones(cd.shape) == 1)
    for i in [cds, cus, cus2, sns, sns2, cu2sns3, s1, cd1, sn1]:
        out = np.bitwise_and(out,i)

    sn_min, cd_sn_min, sn_max, cd_sn_max = extrema(sn,cd)
    cd_min, sn_cd_min, cd_max, sn_cd_max = extrema(cd,sn)

    print("[Cu]: {0:6.2f}".format(chem_cu))
    print("[Sn,Cd]: [{0:6.2f} {1:6.2f}] [{2:6.2f} {3:6.2f}]".format(sn_min, cd_sn_min, sn_max, cd_sn_max))
    print("[Sn,Cd]: [{1:6.2f} {0:6.2f}] [{3:6.2f} {2:6.2f}]".format(cd_min, sn_cd_min, cd_max, sn_cd_max))

    ax.pcolormesh(cd,sn,out,cmap=cm.GnBu,alpha=alpha,shading='auto',rasterized=True)
    for _f in [f_cds, f_cus, f_sns, f_sns2, f_cu2sns3, f_s]:
        ax.plot(_cd, _f(_cd), '-', c='k', lw=0.5)
    ax.plot(_cd, f_cds(_cd), c='C3', lw=1)

main(-0.0, ax0, 0.8)
main(-0.2, ax1, 0.6)
main(-0.5, ax2, 0.4)
main(-0.63, ax3, 0.2)

props = dict(boxstyle='square,pad=0.5', lw=0.5, edgecolor='gray', facecolor='white', alpha=1)
ax0.text(-1.3,-2.6, '$\Delta \mu_{Cu}$ = 0', bbox=props)
ax1.text(-1.3,-2.6, '$\Delta \mu_{Cu}$ = -0.20', bbox=props)
ax2.text(-1.3,-2.6, '$\Delta \mu_{Cu}$ = -0.50', bbox=props)
ax3.text(-1.3,-2.6, '$\Delta \mu_{Cu}$ = -0.67', bbox=props)

'''ax0.text(-0.8,-0.2, 'R1')
ax0.text(-2.0,-1.1, 'R2')
ax2.text(-1.4,-0.8, 'P1')
ax2.text(-2.4,-1.65, 'P2')
'''

ax2.set_xlabel('$\Delta \mu_{Cd}$ (eV)')
ax3.set_xlabel('$\Delta \mu_{Cd}$ (eV)')
ax0.set_ylabel('$\Delta \mu_{Sn}$ (eV)')
ax2.set_ylabel('$\Delta \mu_{Sn}$ (eV)')

plt.savefig('ccts_cu_pd.pdf', dpi=600)
