#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

CZTS = -4.65
CZTS += 0.1
ZnS = -1.90
CuS = -0.51
Cu2S = -0.52
SnS = -0.85
SnS2 = -1.29
Cu2SnS3 = -2.52

def mu_s(zn,sn,cu):
    czts = np.ones(zn.shape)
    return (CZTS*czts - zn - sn - 2*cu)*0.25

_cu = np.arange(-3,0,0.01)
_sn = np.arange(-3,0,0.01)
cu, sn = np.meshgrid(_cu,_sn)
zn  = np.ones(cu.shape)
#zn *= -1.01
zn *= -1.5
#zn *= -2.12
s = mu_s(zn,sn,cu)

zns = (zn + s < ZnS)
cus = (cu + s < CuS)
cu2s = (2*cu + s < Cu2S)
sns = (sn + s < SnS)
sns2 = (sn + 2*s < SnS2)
cu2sns3 = (2*cu + sn + 3*s < Cu2SnS3)
s1 = (s < 0)

out = (np.ones(zn.shape) == 1)
for i in [zns, cus, cu2s, sns, sns2, cu2sns3, s1]:
    out = np.bitwise_and(out,i)

plt.pcolormesh(cu,sn,out,cmap=cm.GnBu)
plt.show()
