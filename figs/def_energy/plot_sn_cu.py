#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator
import seaborn as sns
import pandas as pd

def set_color(config):
    fac = [4,3,5,6]
    _num = np.sum([int(n)*fac[ind] for ind,n in enumerate(list(config))])
    _num_norm = (_num - 13.0)/6.0
    color_series = cmap(norm(_num_norm))
    return list(color_series)

fig_width = 16/2.54
fig_height = fig_width*0.3
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 5,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
cmap = plt.get_cmap('magma')
norm = colors.Normalize(-0.1,1.1)

fig = plt.figure()
ax0,ax1,ax2 = fig.subplots(1,3, sharey=False)

vs_0 = np.genfromtxt('v_s/0/energy')
vs_1 = np.genfromtxt('v_s/1/energy')
vs_2 = np.genfromtxt('v_s/2/energy')
eV = 27.2116/2
cut = 0.08
width = 0.05

df = pd.read_hdf('sn_cu/sn_cu.h5')
ref =  -49017.36141309-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy+width), width),
            hist=True, kde=False, color=set_color(config), label=config, ax=ax0)
ax0.legend(frameon=False)
ax0.set_xlabel(r'$\Delta E_{form} (Sn_{Cu}^0)$ (eV)')
ax0.set_ylabel('Count')

df = pd.read_hdf('sn_cu_+1/sn_cu.h5')
ref =  -49017.36141309-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy+width), width),
            hist=True, kde=False, color=set_color(config), label=config, ax=ax1)
ax1.legend(frameon=False)
ax1.set_xlabel(r'$\Delta E_{form} (Sn_{Cu}^{+1})$ (eV)')

df = pd.read_hdf('sn_cu_+3/sn_cu.h5')
ref =  -49017.36141309-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy+width), width),
            hist=True, kde=False, color=set_color(config), label=config, ax=ax2)
ax2.legend(frameon=False)
ax2.set_xlabel(r'$\Delta E_{form} (Sn_{Cu}^{+3})$ (eV)')

plt.tight_layout()
plt.savefig('sn_cu.pdf')
