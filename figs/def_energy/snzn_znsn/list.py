#!/usr/bin/env python3

import pandas as pd

df_zn = pd.read_hdf('../v_zn/v_zn.h5')
df_sn = pd.read_hdf('../zn_sn/zn_sn.h5') 

with open('snzn_znsn.dat') as f: 
    a = f.read().splitlines()

for i in range(len(a)): 
    zn, sn = a[i].split() 
    zn = int(zn) + 1 
    sn = int(sn) + 1 
    zn_env = df_zn[df_zn['index'] == zn].counts_str.tolist()
    sn_env = df_sn[df_sn['index'] == sn].counts_str.tolist()
    if zn_env == ['4000']:
        print(zn, sn, zn_env, sn_env)
    #print(zn, sn, zn_env, sn_env)
