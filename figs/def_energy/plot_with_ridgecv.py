#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import RidgeCV
from matplotlib import cm, colors, colorbar
from matplotlib.ticker import MaxNLocator, FormatStrFormatter
from mpl_toolkits.axes_grid1 import make_axes_locatable
import seaborn as sns
import pandas as pd
from os.path import join

fig_width = 17.5/2.54
fig_height = fig_width*0.52
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 7,
          'font.weight': 'light',
          'legend.fontsize': 5,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'ytick.right': False,
          'xtick.major.width': 0.4,
          'xtick.minor.width': 0.2,
          'ytick.major.width': 0.4,
          'ytick.minor.width': 0.2}
plt.rcParams.update(params)
cmap = plt.get_cmap('RdYlBu')
norm = colors.Normalize(-0.8,0.8)
norm2 = colors.Normalize(-0.5,0.5)

fig = plt.figure(constrained_layout=False)
gs = fig.add_gridspec(1,2,width_ratios=[3.1,2],left=0.06, right=0.99, top=0.99, bottom=0.08, wspace=0.2)

gs0 = gs[0].subgridspec(2,3, hspace=0.15)
gs1 = gs[1].subgridspec(2,2, hspace=0.15)

acceptors = ['v_cu', 'v_zn', 'v_sn', 'cu_sn', 'zn_sn', 'cu_zn']
donors = ['v_s', 'sn_cu', 'sn_zn', 'zn_cu']
ref_v_s   = -49233.30292575-(-49253.93209689--49253.68138789)
ref_sn_cu = -49017.36141309-(-49253.93209689--49253.68138789)
ref_cu_sn = -49490.02091436-(-49253.93209689--49253.68138789)
ref_v_cu  = -48883.84579622-(-49253.93209689--49253.68138789)
ref_v_zn  = -48841.40383027-(-49253.93209689--49253.68138789)
ref_v_sn  = -49119.84850759-(-49253.93209689--49253.68138789)
ref_sn_zn = -48975.12939791-(-49253.93209689--49253.68138789)
ref_zn_sn = -49532.46449111-(-49253.93209689--49253.68138789)
ref_cu_zn = -49211.52308154-(-49253.93209689--49253.68138789)
ref_zn_cu = -49296.25420102-(-49253.93209689--49253.68138789)
ref_list = {'v_s':ref_v_s, 'sn_cu':ref_sn_cu, 'cu_sn':ref_cu_sn, 'sn_zn':ref_sn_zn, 'zn_sn':ref_zn_sn, 'v_zn':ref_v_zn, 'v_sn':ref_v_sn, 'v_cu':ref_v_cu, 'cu_zn':ref_cu_zn, 'zn_cu':ref_zn_cu}
name_list = {'v_s':r'$V_S$', 'sn_cu':r'$Sn_{Cu}$', 'cu_sn':r'$Cu_{Sn}$', 'sn_zn':r'$Sn_{Zn}$', 'zn_sn':r'$Zn_{Sn}$', 'v_zn':r'$V_{Zn}$', 'v_sn':r'$V_{Sn}$', 'v_cu':r'$V_{Cu}$', 'cu_zn':r'$Cu_{Zn}$', 'zn_cu':r'$Zn_{Cu}$'}

motifs = ['Cu-Cu-Zn-Sn', 'Cu-Cu-Cu-Sn', 'Cu-Zn-Zn-Sn', 'Zn-Zn-Zn-Sn']
motifs_ind = {'Cu-Cu-Zn-Sn':'C1', 'Cu-Cu-Cu-Sn':'C3', 'Cu-Zn-Zn-Sn':'C0', 'Zn-Zn-Zn-Sn':'C0'}

width = 0.05
kwargs={'alpha':0.9}
label_kw = dict({"fontsize":6, "fontweight":"light"})

def set_color(config):
    fac = [4,3,5,6]
    _num = np.sum([int(n)*fac[ind] for ind,n in enumerate(list(config))])
    _num_norm = (_num - 16.0)/4.0
    color_series = cmap(norm(_num_norm))
    return list(color_series)

def set_edgecolor(config):
    fac = [4,3,5,6]
    _num = np.sum([int(n)*fac[ind] for ind,n in enumerate(list(config))])
    _num_norm = (_num - 16.0)/4.0
    if np.abs(_num_norm) < 0.2:
        _num_norm = -0.15
    color_series = cmap(norm2(_num_norm))
    return list(color_series)

def ridge_def(defect,gs):
    df = pd.read_hdf(join(defect,''.join((defect,'.h5'))))
    df['energy'] -= ref_list[defect]
    df['energy'] *= 27.2116/2
    X = (df['counts'].apply(lambda x: np.asarray(x))).values
    X = np.concatenate(X).reshape(len(df),4)
    alphas = [1e-3, 1e-2, 1e-1, 1, 1e1]
    ridge = RidgeCV(alphas=alphas, cv=5)
    y = df['energy'] 
    model = ridge.fit(X,y)
    print(model.coef_)

    #_gs = gs.subgridspec(2,2,height_ratios=[0.8,2], width_ratios=[2,0.8], wspace=0.02, hspace=0.02)
    _gs = gs.subgridspec(2,1,height_ratios=[0.8,2],hspace=0.0)
    ax_hist = fig.add_subplot(_gs[0])
    ax = fig.add_subplot(_gs[1])
    '''ax_coef = fig.add_subplot(_gs[-1])
    for i, motif in enumerate(motifs):
        ax_coef.bar(i,model.coef_[i],color=motifs_ind[motif], **kwargs)
    ax_coef.set_xticklabels([])
    ax_coef.xaxis.set_ticks_position('none')
    ax_coef.set_yticklabels([])'''

    configs = list(set(df.counts_str))
    for config in configs:
        _df = df[df['counts_str'] == config]
        _y = _df['energy']
        _X = _df['counts'].apply(lambda x: np.asarray(x)).values
        _X = np.concatenate(_X).reshape(len(_df),4)
        ax.scatter(_y, model.predict(_X), color=set_color(config), edgecolor=set_edgecolor(config), lw=0.5)
        #sns.distplot(_y, bins=np.arange(min(_y), max(_y+width), width), hist=True,
        #            kde=False, color=set_color(config), hist_kws=kwargs, ax=ax_hist)
        sns.histplot(_y, bins=np.arange(min(_y), max(_y+width), width), 
                    kde=False, color=set_color(config), ax=ax_hist)
    ax_hist.set_xticklabels([])
    ax_hist.set_yticklabels([])
    ax_hist.set_xlabel('')
    ax_hist.yaxis.set_ticks_position('none')
    ax_hist.axis('off')

    xlim = ax.get_xlim()
    ylim = ax.get_ylim() 
    ax.plot([-2,2],[-2,2],'--', color='gray', lw=0.5, zorder=0)
    ax.set_xlim(xlim)
    ax_hist.set_xlim(xlim)
    ax.set_ylim(xlim)
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))    

    if defect in ['v_cu', 'cu_sn', 'v_s', 'sn_zn']:
        ax.set_ylabel(r'Predicted $E_{form}$ (eV)') 
        #ax_hist.set_ylabel('Count')
        #ax_hist.yaxis.set_label_coords(-0.28,0.5)
    if defect in acceptors[3:] or defect in donors[2:]:
        ax.set_xlabel(r'Calculated $E_{form}$ (eV)')
    ax.yaxis.set_major_locator(MaxNLocator(3))
    ax.xaxis.set_major_locator(MaxNLocator(3))
    if defect in ['v_cu']:
        ax.text(-.3,1.25,'(a)',transform=ax.transAxes, **label_kw)
    if defect in ['v_s']:
        ax.text(-.3,1.25,'(b)',transform=ax.transAxes, **label_kw)

    ax.text(0.15,0.8,name_list[defect],transform=ax.transAxes)
    
for i, def_ in enumerate(acceptors):
    ridge_def(def_,gs0[i])

for i,def_ in enumerate(donors):
    ridge_def(def_,gs1[i])

fig.set_constrained_layout_pads(w_pad=0.02, h_pad=0.02, hspace=0.02, wspace=0.02)
plt.savefig('def_energy.pdf')
