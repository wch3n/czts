#!/usr/bin/env python3

import pandas as pd

df_cu = pd.read_hdf('../zn_cu/zn_cu.h5')
df_sn = pd.read_hdf('../zn_sn/zn_sn.h5') 

with open('sncu_cusn.dat') as f: 
    a = f.read().splitlines()

for i in range(len(a)): 
    cu, sn = a[i].split() 
    cu = int(cu) + 1 
    sn = int(sn) + 1 
    cu_env = df_cu[df_cu['index'] == cu].counts_str.tolist()
    sn_env = df_sn[df_sn['index'] == sn].counts_str.tolist()
    if sn_env == ['3010']:
        print(cu, sn, cu_env, sn_env)
