#!/usr/bin/env python3

import numpy as np
import pandas as pd

motif = [[4,0,0,0], [0,4,0,0], [0,0,4,0], [0,0,0,4]]
workdir = ['0/energy','1/energy','2/energy','3/energy']

df = pd.DataFrame(columns=['counts', 'counts_str', 'energy']) 

for i, _dir in enumerate(workdir):
    f = np.genfromtxt(_dir)
    _df = pd.DataFrame(columns=['counts', 'counts_str', 'energy'])
    _df['energy'] = f
    _df['counts'] = [motif[i]]*len(_df)
    df = df.append(_df, ignore_index=True)

for j in df.index:
    df.loc[j,'counts_str'] = ''.join(map(str,df.loc[j,'counts']))

df.to_hdf('v_s.h5', key='v_s')
