#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator
import seaborn as sns
import pandas as pd

def set_color(config):
    fac = [4,3,5,6]
    _num = np.sum([int(n)*fac[ind] for ind,n in enumerate(list(config))])
    _num_norm = (_num - 13.0)/6.0
    color_series = cmap(norm(_num_norm))
    return list(color_series)

fig_width = 18/2.54
fig_height = fig_width*0.5
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 5,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
cmap = plt.get_cmap('magma')
norm = colors.Normalize(-0.1,1.1)

fig = plt.figure()
(ax00,ax01,ax02,ax03,ax04),(ax10,ax11,ax12,ax13,ax14) = fig.subplots(2,5, sharey=False)

vs_0 = np.genfromtxt('v_s/0/energy')
vs_1 = np.genfromtxt('v_s/1/energy')
vs_2 = np.genfromtxt('v_s/2/energy')
eV = 27.2116/2
cut = 0.06
width = 0.05

ref = -49233.30292575-(-49253.93209689--49253.68138789)
sns.distplot((vs_0-ref)*eV, bins=np.arange(min((vs_0-ref)*eV),max((vs_0-ref)*eV)+width, width),hist=True, kde=False, label='Cu-Cu-Zn-Sn', color=set_color('4000'),ax=ax03)
sns.distplot((vs_1-ref)*eV, bins=np.arange(min((vs_1-ref)*eV),max((vs_1-ref)*eV)+width, width),hist=True, kde=False, label='Cu-Cu-Cu-Sn', color=set_color('1300'),ax=ax03)
sns.distplot((vs_2-ref)*eV, bins=np.arange(min((vs_2-ref)*eV),max((vs_2-ref)*eV)+width, width),hist=True, kde=False, label='Cu-Zn-Zn-Sn', color=set_color('2020'),ax=ax03)
ax03.legend(frameon=False, loc='upper left')
ax03.set_xlabel(r'$\Delta E_{form} (V_S^0)$ (eV)')

df = pd.read_hdf('sn_cu/sn_cu.h5')
ref =  -49017.36141309-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy+width), width),
                hist=True, kde=False, color=set_color(config), label=config[:-1], ax=ax13)
ax13.legend(frameon=False)
ax13.set_xlabel(r'$\Delta E_{form} (Sn_{Cu}^0)$ (eV)')

df = pd.read_hdf('cu_sn/cu_sn.h5')
ref = -49490.02091436-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy)+width, width),
                hist=True, kde=False, color=set_color(config), label=config[:-1], ax=ax10)
ax10.legend(frameon=False)
ax10.set_xlabel(r'$\Delta E_{form} (Cu_{Sn}^0)$ (eV)')
ax10.set_ylabel('Count')

df = pd.read_hdf('v_cu/v_cu.h5')
ref = -48883.84579622-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy)+width, width),
                hist=True, kde=False, color=set_color(config), label=config[:-1], ax=ax00)
ax00.legend(frameon=False)
ax00.set_ylabel('Count')
ax00.set_xlabel(r'$\Delta E_{form} (V_{Cu}^0)$ (eV)')

df = pd.read_hdf('v_zn/v_zn.h5')
ref = -48841.40383027-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy)+width, width),
                hist=True, kde=False, color=set_color(config), label=config[:-1], ax=ax01)
ax01.legend(frameon=False)
ax01.set_xlabel(r'$\Delta E_{form} (V_{Zn}^0)$ (eV)')

df = pd.read_hdf('v_sn/v_sn.h5')
ref = -49119.84850759-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy)+width, width),
                hist=True, kde=False, color=set_color(config), label=config[:-1], ax=ax02)
ax02.legend(frameon=False)
ax02.set_xlabel(r'$\Delta E_{form} (V_{Sn}^0)$ (eV)')

df = pd.read_hdf('sn_zn/sn_zn.h5')
ref = -48975.12939791-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy)+width, width),
                hist=True, kde=False, color=set_color(config), label=config[:-1], ax=ax14)
ax14.legend(frameon=False)
ax14.set_xlabel(r'$\Delta E_{form} (Sn_{Zn}^0)$ (eV)')
ax14.set_xlim([-0.6,0.5])

df = pd.read_hdf('zn_sn/zn_sn.h5')
ref = -49532.46449111-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy)+width, width),
                hist=True, kde=False, color=set_color(config), label=config[:-1], ax=ax11)
ax11.legend(frameon=False)
ax11.set_xlabel(r'$\Delta E_{form} (Zn_{Sn}^0)$ (eV)')

df = pd.read_hdf('cu_zn/cu_zn.h5')
ref = -49211.52308154-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy)+width, width),
                hist=True, kde=False, color=set_color(config), label=config[:-1], ax=ax04)
ax04.legend(frameon=False)
ax04.set_xlabel(r'$\Delta E_{form} (Cu_{Zn}^0)$ (eV)')

df = pd.read_hdf('zn_cu/zn_cu.h5')
ref = -49296.25420102-(-49253.93209689--49253.68138789)
configs = list(set(df.counts_str))
configs.sort(reverse=True)
nconf = len(df)
for config in configs:
    if len(df[df['counts_str'] == config]) > int(nconf*cut):
        _energy = (df[df['counts_str'] ==config]['energy'] - ref)*eV
        sns.distplot(_energy, bins=np.arange(min(_energy), max(_energy)+width, width),
                hist=True, kde=False, color=set_color(config), label=config[:-1], ax=ax12)
ax12.legend(frameon=False)
ax12.set_xlabel(r'$\Delta E_{form} (Zn_{Cu}^0)$ (eV)')

for _ax in (ax00,ax01,ax02,ax03,ax04,ax10,ax11,ax12,ax13,ax14):
    _yaxis = _ax.get_yaxis()
    _yaxis.set_major_locator(MaxNLocator(integer=True))

plt.tight_layout(pad=0.2, h_pad=1.0, w_pad=0.1)
plt.savefig('def_energy.pdf')
