#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, AutoMinorLocator
from scipy import interpolate
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score, GridSearchCV

# cluster functions
empty = [0]
point = [1,2]
pair_1 = [3,4]
pair_2 = [5,6,7]
pair_3 = [8,9,10,11]
pair_4 = [12,13,14,15]
triplet_1 = [16,17]
#triplet_2 = [18,19,20,21]

multi = [1, 2, 1, 4, 8, 2, 4, 2, 4, 4]

fig_width = 16/2.54
fig_height = fig_width*0.40
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)

fig = plt.figure()

gs1 = gridspec.GridSpec(1,1)
gs1.update(left=0.06, right=0.58, top=0.98, bottom=0.12)

gs2 = gridspec.GridSpec(2,2)
gs2.update(left=0.60, right=0.98, top=0.98, bottom=0.12, hspace=0.02, wspace=0.05)

ax1 = plt.subplot(gs1[0])
ax2_0 = plt.subplot(gs2[0,0])
ax2_1 = plt.subplot(gs2[0,1])
ax2_2 = plt.subplot(gs2[1,0])
ax2_3 = plt.subplot(gs2[1,1])

def select_cluster(df_corr, clusters):
    clus = []
    for i in clusters:
        clus = clus + i
    clus = np.array(clus)
    return df_corr.iloc[:,clus]

def regularize_dos(df_dos, grid_fine, smooth_width=0, log=False):
    df_dos_fine = pd.DataFrame(index=df_dos.index, columns=['dos_hse'])
    for i in df_dos.index:
        dos = df_dos.loc[i, "dos_hse"]
        grid_coarse = df_dos.loc[i, "energy_hse"]
        if smooth_width > 0:
            box = np.ones(smooth_width)/smooth_width
            dos = np.convolve(dos, box, mode='same')
        f = interpolate.interp1d(grid_coarse,dos)
        df_dos_fine.loc[i,"dos_hse"] = f(grid_fine)
    if log:
        _dos_log = df_dos_fine.copy()
        for i in df_dos_fine.index:
            _dos_log.loc[i,'dos_hse'] = np.log(df_dos_fine.loc[i,'dos_hse']+0.1)
        df_dos_fine = _dos_log
    return df_dos_fine

def scissor(df):
    for i in df.index:
        df.loc[i,'energy'][df.loc[i,'energy'] > df.loc[i,'vbm']+0.2] += 0.9 
    return df

def weight(df_dos, gap_max, w_exp):
    w = np.exp((gap_max-df_dos.gap.values.astype(float))/w_exp)
    return w

def calc_model(corr,x, w):
    xx = np.concatenate(x.values)
    ndos = len(xx)/len(corr)
    xx = xx.reshape(len(corr), int(ndos))
    ridge = linear_model.Ridge(fit_intercept=False)
    alphas = np.logspace(-5, 1, 10)
    params = [{'alpha': alphas}]
    model = GridSearchCV(ridge, params, scoring='neg_mean_squared_error', cv=3, refit=True, return_train_score= True)
    model.fit(corr.values, xx)
    return model

def match(df_dos, df_corr):
    overlap = list(set(df_dos['configname']).intersection(set(df_corr['configname'])))
    if len(overlap) < len(df_dos['configname']):
        raise ValueError('DOS and correlation data inconsistent.')
    df = pd.DataFrame()
    for i in df_dos['configname']:
        df = df.append(df_corr[df_corr['configname'] == i])
    return df

def main(mat, clusters, dos, gap_th=0.2, w_exp=0.1, align=False, verbose=False):
    global grid_fine, df_dos_fine, ref_ind
    smooth_width = 5 # the box width used to smooth the density of states
    is_log = False
    #grid_fine = np.arange(2,8,0.02) # grid for the selected energy range
    # read corr matrix and electronic densities
    df_corr = pd.read_csv('corr_{0}.csv'.format(mat), delim_whitespace=True)
    df_dos = pd.read_hdf('pdos_{0}.h5'.format(mat))
    # semiconductors only
    # drop metallic structures
    #
    df_dos = df_dos[df_dos['configname'].str.contains('SCEL1|SCEL2|SCEL3')]
    #
    semi = df_dos[(df_dos.gap > gap_th)].index
    df_dos = df_dos.loc[semi]
    # apply a scissor shift
    df_dos = scissor(df_dos)
    df_corr = match(df_dos, df_corr)
    #
    assert (df_dos.index == df_corr.index).all()
    #ref_ind = df_dos.gap.idxmax()
    if mat == 'czts':
        ref_ind = 1
    else:
        ref_ind = 0
    vbm = df_dos.loc[ref_ind, 'vbm']
    # interpolate the dos on a specific regular mesh
    #df_dos_fine = regularize_dos(df_dos, grid_fine, smooth_width=smooth_width, log=is_log)
    #

    # alignment wrt Cu-semicore
    if (mat == 'czts') | (mat == 'czts_occ'):
        dos_list = ['dos', 'dos_cu', 'dos_sn', 'dos_s', 'dos_zn']
    else:
        dos_list = ['dos', 'dos_cu', 'dos_sn', 'dos_s', 'dos_cd']
    if align:
        ref_level = df_dos.loc[ref_ind, 'ref']
        delta_ref = df_dos['ref'] - ref_level
        for dos_type in dos_list:
            df_dos[dos_type] -= delta_ref
    # keep original mesh and data
    df_dos_fine = df_dos
    grid_fine = df_dos_fine.iloc[0]['energy'] - vbm
    delta_e = grid_fine[1] - grid_fine[0]
    # return the index of energy grid at the vbm
    ind_vbm = np.argmax(grid_fine > 0)
    # normalize the DOS
    for dos_type in dos_list:
        df_dos[dos_type] = df_dos[dos_type] / df_dos['natom']
    train = df_corr
    df_corr_train = train.loc[:,"corr(0)":"corr(17)"]
    df_dos_train = df_dos_fine.loc[train.index]
    dos_train = df_dos_train.loc[:,dos].sum(axis=1)
    #
    corr_train = select_cluster(df_corr_train, clusters)
    #
    w = weight(df_dos.loc[train.index],df_dos.loc[ref_ind,'gap'],w_exp)
    if verbose:
        display(corr_train)
        display(dos_train)
    model = calc_model(corr_train, dos_train, w)
    dos_train_pred = model.predict(corr_train)

    ec = model.best_estimator_.coef_[ind_vbm:ind_vbm + int(0.2/delta_e)].mean(axis=0)/delta_e/multi
    #ec = model.best_estimator_.coef_[ind_vbm + int(0.4/delta_e):ind_vbm + int(0.65/delta_e)].mean(axis=0)/delta_e/multi)
    #
    # test the cluster expansion on the test set
    # plot the real dos (gray) and the predicted dos (red)
    if verbose:
        fig, ax1 = plt.subplots(1)
        for i in range(len(dos_train)):
            ax1.plot(grid_fine, dos_train.iloc[i]+0.5*i, color='gray')
            ax1.plot(grid_fine, dos_train_pred[i]+0.5*i, color='r')
        ax1.set_xlim([-1,2])
        ax1.set_ylim([0,5])
        ax1.set_title("Cluster expanded DOS: Test set")
    # plot the eci vs energy
    '''for i in range(len(model.best_estimator_.coef_[0,:])):
        ax0.plot(grid_fine, model.best_estimator_.coef_[:,i], label=i)
    for pred_iloc in range(len(dos_train)):
        if (dos_train.iloc[pred_iloc] - dos_train.loc[ref_ind]).max() == 0:
            break
    #ax0.plot(grid_fine, dos_train_pred[pred_iloc], 'k-')
    #ax0.plot(grid_fine, dos_train.loc[ref_ind], 'k--')
    ax0.set_ylabel("Effective cluster DOS")
    ax0.text(0.4, 0.8, mat.upper()+" "+" + ".join(dos).upper(), transform=ax0.transAxes, fontsize=12)
    ax0.set_xlim([-1,2])
    ax0.set_ylim([-0.2,1.])
    ax0.legend()'''
    #
    return model, ec

def ecdos(mat,clusters,dos,model,ec,write=False):
    # plot DOS vs Temperature using the correlation matrix from MC simulations
    df_mc = pd.read_csv('mc_{0}.csv'.format(mat), delim_whitespace=True)
    df_mc = df_mc[df_mc['T'] <= 1000].reset_index()
    corr_str = []
    for i in range(18):
        corr_str.append("<corr({0:})>".format(i))
    df_corr_mc = df_mc.loc[:,corr_str]
    corr_mc = select_cluster(df_corr_mc, clusters)
    corr_mc = corr_mc.set_index(df_mc['T']).sort_index(ascending=False) # use temperature as index
    #downsampling
    corr_mc = corr_mc[::2]
    dos_mc_pred = model.predict(corr_mc)
    # cluster decomposed contribution
    _eci = model.best_estimator_.coef_.T
    _corr = corr_mc.values
    _ecdos = np.zeros(len(_corr.T))
    corr_fraction = np.zeros((len(model.best_estimator_.coef_),)+corr_mc.shape)
    for i in range(len(corr_fraction)): # energy
        for j in range(len(_corr)):     # temperature
            for k in range(len(_corr.T)): # corr
                _ecdos[k] = _corr[j,k]*_eci[k,i]
            corr_fraction[i,j] = _ecdos
    #
    norm = mpl.colors.Normalize(vmin=corr_mc.index.min(), vmax = corr_mc.index.max())
    cmap = mpl.cm.get_cmap('plasma')
    c = [cmap(norm(temp)) for temp in corr_mc.index]
    ax1.set_xlim([-1.0,2.6])
    ax1.set_ylim([-0.,1.0])
    grid_ = grid_fine
    for i in range(len(dos_mc_pred)):
        ax1.plot(grid_, dos_mc_pred[i], lw=0.3, color=c[i])
    cax, _ = mpl.colorbar.make_axes(ax1)
    cbar = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm)
    axins = ax1.inset_axes([0.4, 0.5, 0.35, 0.35])
    for i in range(len(dos_mc_pred)):
        axins.plot(grid_, dos_mc_pred[i], lw=0.3, color=c[i])
    axins.set_xlim([-0.1, 0.2])
    axins.set_ylim([0.,0.2])
    minorLocator = AutoMinorLocator(n=2)
    axins.yaxis.set_minor_locator(minorLocator)
    axins.yaxis.set_major_locator(MultipleLocator(0.1))
    #ax1.indicate_inset_zoom(axins, ls='--', zorder=0)
    # plot the kesterite reference dos
    #ax1.plot(grid_, df_dos_fine.loc[ref_ind, "dos"], '--', lw=0.5, c='gray', zorder=0)
    ax1.set_xlabel(r'$E-E^{Kest}_{VBM}$ (eV)')
    ax1.set_ylabel(r'Temperature-dependent DOS (states/eV)')
    ax1.text(0.9,0.9, '(a)', fontweight='regular', transform=ax1.transAxes)
    #ax1.text(0.4, 0.8, mat.upper()+" "+" + ".join(dos).upper(), transform=ax1.transAxes, fontsize=12)
    #ax1.legend()
    #
    for i in range(len(dos_mc_pred)):
        ax2_0.plot(grid_, corr_fraction[:,i,1:3].sum(axis=1), lw=0.5, color=c[i])
        ax2_1.plot(grid_, corr_fraction[:,i,3:5].sum(axis=1), lw=0.5, color=c[i])
        ax2_2.plot(grid_, corr_fraction[:,i,5:8].sum(axis=1), lw=0.5, color=c[i])
        ax2_3.plot(grid_, corr_fraction[:,i,8:].sum(axis=1),  lw=0.5, color=c[i])
    ax2_0.plot(grid_, corr_fraction[:,0,0], lw=0.5, ls='--', color='gray', zorder=0)
    ax2_1.plot(grid_, corr_fraction[:,0,0], lw=0.5, ls='--', color='gray', zorder=0)
    ax2_2.plot(grid_, corr_fraction[:,0,0], lw=0.5, ls='--', color='gray', zorder=0)
    ax2_3.plot(grid_, corr_fraction[:,0,0], lw=0.5, ls='--', color='gray', zorder=0)
    ax2_0.text(0.6,0.2, 'point', transform=ax2_0.transAxes)
    ax2_0.text(0.38,0.6, r'$V_\alpha=[{0:.1f},{1:.1f}$]'.format(ec[1],ec[2]), transform=ax2_0.transAxes)
    ax2_1.text(0.6,0.2, 'pair 1NN', transform=ax2_1.transAxes)
    ax2_1.text(0.38,0.6, r'$V_\alpha=[{0:.1f},{1:.1f}$]'.format(ec[3],ec[4]), transform=ax2_1.transAxes)
    ax2_2.text(0.6,0.2, 'pair 2NN', transform=ax2_2.transAxes)
    ax2_2.text(0.38,0.6, r'$V_\alpha=[{0:.1f},{1:.1f},{2:.1f}$]'.format(ec[5],ec[6],ec[7]), transform=ax2_2.transAxes)
    ax2_3.text(0.6,0.2, 'triplet 1NN', transform=ax2_3.transAxes)
    ax2_3.text(0.38,0.6, r'$V_\alpha=[{0:.1f},{1:.1f}$]'.format(ec[8],ec[9]), transform=ax2_3.transAxes)
    ax2_0.text(0.9,0.9, '(b)', fontweight='regular',transform=ax2_0.transAxes)
    ax2_1.text(0.9,0.9, '(c)', fontweight='regular',transform=ax2_1.transAxes)
    ax2_2.text(0.9,0.9, '(d)', fontweight='regular',transform=ax2_2.transAxes)
    ax2_3.text(0.9,0.9, '(e)', fontweight='regular',transform=ax2_3.transAxes)
    for _ax in [ax2_0,ax2_1,ax2_2,ax2_3]:
        _ax.set_xlim([-0.5,1.2])
        _ax.set_ylim([-0.15,0.15])
        minorLocator = AutoMinorLocator(n=2)
        _ax.yaxis.set_minor_locator(minorLocator)
        _ax.yaxis.set_major_locator(MultipleLocator(0.1))
        #cax, _ = mpl.colorbar.make_axes(_ax)
        #cbar = mpl.colorbar.ColorbarBase(cax, cmap=cmap, norm=norm)
    ax2_0.set_xticklabels([])
    ax2_1.set_xticklabels([])
    ax2_1.set_yticklabels([])
    ax2_3.set_yticklabels([])
    ax2_2.set_xlabel(r'$E - E^{Kest}_{VBM}$ (eV)')
    ax2_2.xaxis.set_label_coords(1,-0.17)
    if write:
        plt.savefig('dos.png', dpi=300, transparent=True)
    return corr_fraction, corr_mc

mat = 'czts'
w_exp = 10
gap_th = 0.3
align = True
verbose = False
clusters = [empty, point, pair_1, pair_2, triplet_1]
dos_cuzn = ['dos_cu', 'dos_zn']; dos_cu = ['dos_cu']; dos_zn = ['dos_zn']
dos_tot = ['dos']; dos_s = ['dos_s']; dos_sn = ['dos_sn']

for dos in [dos_tot]:
    model_czts, ec = main(mat=mat, clusters=clusters, dos=dos, gap_th=gap_th, w_exp=w_exp, align=align, verbose=verbose)
    corr_fraction, corr_mc = ecdos(mat=mat, clusters=clusters, dos=dos, model=model_czts, ec=ec, write=True)

'''corr_mc.loc[:,"<corr(1)>":"<corr(2)>"].plot()
corr_mc.loc[:,"<corr(3)>":"<corr(4)>"].plot()
corr_mc.loc[:,"<corr(5)>":"<corr(7)>"].plot()
corr_mc.loc[:,"<corr(16)>":"<corr(17)>"].plot()
'''
