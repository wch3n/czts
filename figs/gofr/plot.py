#!/usr/bin/env python3

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, AutoMinorLocator

fig_width = 8.5/2.54
fig_height = fig_width*0.75
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

gs = gridspec.GridSpec(2,1)
gs.update(left=0.12, right=0.95, top=0.98, bottom=0.12, hspace=0.06)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])

gofr = np.genfromtxt('gofr.dat')
a = 5.45
kwargs = {'width': 0.2/a, 'lw': 0.4, 'alpha':0.5, 'rasterized':False}

C1 = "#7fb9dc"
C2 = "#ffb97f"
C3 = "#bf966e"

# plot bars with higher values
ma1 = gofr[:,3] > gofr[:,4]
ma2 = gofr[:,3] < gofr[:,4]
ax1.bar(gofr[:,0][ma2]/a, gofr[:,4][ma2], edgecolor='k', facecolor=C2, label='200 K', zorder=10, **kwargs)
ax1.bar(gofr[:,0][ma1]/a, gofr[:,3][ma1], edgecolor='k', facecolor=C1, label='800 K', zorder=10, **kwargs)
ax1.bar(gofr[:,0][ma1]/a, gofr[:,4][ma1], edgecolor='k', facecolor=C3, zorder=11, **kwargs)
ax1.bar(gofr[:,0][ma2]/a, gofr[:,3][ma2], edgecolor='k', facecolor=C3, zorder=11, **kwargs)
ax1.set_xlim([0,10/a])
ax1.set_ylim([0,29])
#ax1.bar(gofr[:,0]/a, gofr[:,3], edgecolor='C0', facecolor='C0', label='800 K', zorder=10, **kwargs)
#ax1.bar(gofr[:,0]/a, gofr[:,4], edgecolor='C1', facecolor='C1', label='200 K', zorder=8, **kwargs)
minorLocator = AutoMinorLocator(n=2)
ax1.xaxis.set_minor_locator(minorLocator)
ax1.xaxis.set_major_locator(MultipleLocator(0.5))
ax1.yaxis.set_minor_locator(minorLocator)
ax1.yaxis.set_major_locator(MultipleLocator(10))
leg = ax1.legend()
leg.get_frame().set_linewidth(0)
ax1.text(0.1,0.8,r'Cu-Cu', transform=ax1.transAxes)
ax1.xaxis.set_ticklabels([])
ax1.xaxis.label.set_visible(False)

ma1 = gofr[:,1] > gofr[:,2]
ma2 = gofr[:,1] < gofr[:,2]
ax2.bar(gofr[:,0][ma2]/a, gofr[:,2][ma2], edgecolor='k', facecolor=C2, label='200 K', zorder=10, **kwargs)
ax2.bar(gofr[:,0][ma1]/a, gofr[:,1][ma1], edgecolor='k', facecolor=C1, label='800 K', zorder=10, **kwargs)
ax2.bar(gofr[:,0][ma1]/a, gofr[:,2][ma1], edgecolor='k', facecolor=C3, zorder=11, **kwargs)
ax2.bar(gofr[:,0][ma2]/a, gofr[:,1][ma2], edgecolor='k', facecolor=C3, zorder=11, **kwargs)
ax2.set_xlim([0,10/a])
ax2.set_ylim([0,29])
ax2.set_xlabel(r'Distance ($a_{CZTS}$)')
ax2.set_ylabel('Pair distribution function')
ax2.yaxis.set_label_coords(-0.1,1)
ax2.text(0.1,0.8,r'Zn-Zn', transform=ax2.transAxes)
ax2.xaxis.set_minor_locator(minorLocator)
ax2.xaxis.set_major_locator(MultipleLocator(0.5))
ax2.yaxis.set_minor_locator(minorLocator)
ax2.yaxis.set_major_locator(MultipleLocator(10))
ax2.text(np.sqrt(2)/2, 20, 'NN', ha='center', fontsize=6)
ax2.text(1, 20, '2NN', ha='center', fontsize=6)

plt.savefig('gofr.png', dpi=300)
