#!/usr/bin/env python3

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, AutoMinorLocator

fig_width = 8.5/2.54
fig_height = fig_width*1.0
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

gs = gridspec.GridSpec(3,1)
gs.update(left=0.12, right=0.95, top=0.98, bottom=0.12, hspace=0.06)
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
ax3 = plt.subplot(gs[2])

lt_cucu = np.genfromtxt('lt_cucu.dat')
ht_cucu = np.genfromtxt('ht_cucu.dat')
mc_cucu = np.genfromtxt('mc_cucu.dat')
lt_znzn = np.genfromtxt('lt_znzn.dat')
ht_znzn = np.genfromtxt('ht_znzn.dat')
mc_znzn = np.genfromtxt('mc_znzn.dat')
lt_cuzn = np.genfromtxt('lt_cuzn.dat')
ht_cuzn = np.genfromtxt('ht_cuzn.dat')
mc_cuzn = np.genfromtxt('mc_cuzn.dat')
a = 5.45
kwargs = {'width': 0.10/a, 'lw': 0.5, 'alpha':0.7, 'rasterized':False}

s = 0.20/2/a

C2 = 'C3'
C1 = 'C0'
C3 = 'w'

ax1.bar(lt_cucu[:,0]/a-s, lt_cucu[:,1], ls='--',edgecolor='k', facecolor=C3, label='Kesterite', zorder=10, **kwargs)
ax1.bar(ht_cucu[:,0]/a, ht_cucu[:,1], edgecolor='k', facecolor=C2, label='288-atom SC', zorder=10, **kwargs)
ax1.bar(mc_cucu[:,0]/a+s, mc_cucu[:,1], edgecolor='k', facecolor=C1, label='MC (800 K)', zorder=10, **kwargs)
ax1.set_xlim([0,1.8])
#ax1.set_ylim([0,29])
minorLocator = AutoMinorLocator(n=2)
ax1.xaxis.set_minor_locator(minorLocator)
ax1.xaxis.set_major_locator(MultipleLocator(0.5))
ax1.yaxis.set_minor_locator(minorLocator)
ax1.yaxis.set_major_locator(MultipleLocator(10))
ax1.set_ylabel(r'$g(r)$')
leg = ax1.legend(loc='upper left')
leg.get_frame().set_linewidth(0)
ax1.text(0.1,0.2,r'Cu-Cu', transform=ax1.transAxes)
ax1.xaxis.set_ticklabels([])
ax1.xaxis.label.set_visible(False)

ax2.bar(lt_cuzn[:,0]/a-s, lt_cuzn[:,1], ls='--', edgecolor='k', facecolor=C3, label='KS', zorder=10, **kwargs)
ax2.bar(ht_cuzn[:,0]/a, ht_cuzn[:,1], edgecolor='k', facecolor=C2, label='SC', zorder=10, **kwargs)
ax2.bar(mc_cuzn[:,0]/a+s, mc_cuzn[:,1], edgecolor='k', facecolor=C1, label='MC', zorder=10, **kwargs)
ax2.set_xlim([0,1.8])
#ax2.set_ylim([0,29])
ax2.set_ylabel(r'$g(r)$')
#ax2.yaxis.set_label_coords(-0.1,0.5)
ax2.text(0.1,0.2,r'Cu-Zn', transform=ax2.transAxes)
ax2.xaxis.set_minor_locator(minorLocator)
ax2.xaxis.set_major_locator(MultipleLocator(0.5))
ax2.yaxis.set_minor_locator(minorLocator)
ax2.yaxis.set_major_locator(MultipleLocator(10))
ax2.xaxis.set_ticklabels([])
#ax2.text(np.sqrt(2)/2, 20, '1NN', ha='center', fontsize=6)
#ax2.text(1, 20, '2NN', ha='center', fontsize=6)

ax3.bar(lt_znzn[:,0]/a-s, lt_znzn[:,1], ls='--', edgecolor='k', facecolor=C3, label='SC', zorder=10, **kwargs)
ax3.bar(ht_znzn[:,0]/a, ht_znzn[:,1], edgecolor='k', facecolor=C2, label='SC', zorder=10, **kwargs)
ax3.bar(mc_znzn[:,0]/a+s, mc_znzn[:,1], edgecolor='k', facecolor=C1, label='MC', zorder=10, **kwargs)
ax3.set_xlim([0,1.8])
#ax3.set_ylim([0,29])
ax3.set_ylabel(r'$g(r)$')
ax3.set_xlabel(r'$r$ ($a_{CZTS}$)')
ax3.text(0.1,0.2,r'Zn-Zn', transform=ax3.transAxes)
ax3.xaxis.set_minor_locator(minorLocator)
ax3.xaxis.set_major_locator(MultipleLocator(0.5))
ax3.yaxis.set_minor_locator(minorLocator)
ax3.yaxis.set_major_locator(MultipleLocator(10))

plt.savefig('gofr_mc.png', dpi=300)
