#!/usr/bin/env python3

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator, MultipleLocator
import numpy as np
from pymatgen.io.vasp.inputs import Poscar
from pymatgen.analysis.local_env import BrunnerNN_real
from scipy.fftpack import fft, ifft, fftn, ifftn, fftshift, ifftshift
from scipy.interpolate import interp1d
from scipy.signal import fftconvolve, gaussian
import seaborn as sns

fig_width = 8.5/2.54
fig_height = fig_width*0.75
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)
props = dict(boxstyle='square,pad=0.2', lw=0.5, edgecolor='none', facecolor='white', alpha=0.7)

fig = plt.figure(constrained_layout=False)
gs = fig.add_gridspec(2,2,height_ratios=[0.20,0.8],left=0.14, right=0.98, top=0.95, bottom=0.13, hspace=0.1)
gs0_hist = gs[0,0]
gs0_bstr = gs[1,0].subgridspec(2,1, height_ratios=[0.2,0.55], hspace=0.06)

gs1_hist = gs[0,1]
gs1_bstr = gs[1,1].subgridspec(2,1, height_ratios=[0.2,0.55], hspace=0.06)

cmap='Blues'
#cmap='YlGnBu'
vmax = 0.06

A = 16.29
C = 21.72

gs10 = gs0_bstr
ax00 = fig.add_subplot(gs10[0])
ax01 = fig.add_subplot(gs10[1])

f = np.genfromtxt('ht_7.60_8.98')
r = f[:,0]*0.529177
ldos_ht= f[:,1::3].T
egrid = np.arange(7.60,8.99,0.02)-8.3051
xx, yy = np.meshgrid(r, egrid)
ldos_ht[(yy[:,0] < 0.1) & (yy[:,0] > -0.05)] *= 3
ldos_ht[(yy[:,0] < 0.7) & (yy[:,0] > 0.4)] *= 10
ax01.contourf(xx,yy, ldos_ht, cmap=cmap, vmin=0.0, vmax=vmax)
#ax01.set_xticklabels([])
ax01.set_ylim([-0.45,0.1])

cb_shift = 0.72
yy[yy > 0.3] += cb_shift
ax00.contourf(xx,yy, ldos_ht, cmap=cmap, vmin=0.0, vmax=vmax)
ax00.set_ylim([1.1,1.3])
ax00.set_xticklabels([])
ax00.xaxis.tick_top()
ax00.spines['bottom'].set_visible(False)
ax01.spines['top'].set_visible(False)
ax01.xaxis.tick_bottom()

##
gs11 = gs1_bstr
ax10 = fig.add_subplot(gs11[0])
ax11 = fig.add_subplot(gs11[1])

f = np.genfromtxt('lt_7.60_9.00')
r = f[:,0]*0.529177
ldos_lt = f[:,1::3].T
egrid = np.arange(7.60,9.01,0.02)-8.0822
xx, yy = np.meshgrid(r, egrid)
ldos_lt[(yy[:,0] < 0.1) & (yy[:,0] > -0.05)] *= 5
ldos_lt[(yy[:,0] < 0.9) & (yy[:,0] > 0.75)] *= 10
ax11.contourf(xx,yy, ldos_lt, cmap=cmap, vmin=0.0, vmax=vmax)
ax11.set_ylim([-0.45,0.1])
ax11.xaxis.tick_bottom()

yy[yy > 0.5] += cb_shift
ax10.contourf(xx,yy, ldos_lt, cmap=cmap, vmin=0.0, vmax=vmax)
ax10.set_ylim([1.4,1.6])
ax10.set_xticklabels([])
ax10.xaxis.tick_top()

ax10.spines['bottom'].set_visible(False)
ax11.spines['top'].set_visible(False)

for ax in [ax00, ax01, ax10, ax11]:
    ax.set_xlim([r[0], r[-1]])
    ax.yaxis.set_major_locator(MultipleLocator(.1))

ax01.yaxis.get_major_ticks()[-2].label1.set_visible(False)
ax11.yaxis.get_major_ticks()[-2].label1.set_visible(False)

ax00.set_ylabel(r'$E - E_{VBM}$ (eV)')
ax00.yaxis.set_label_coords(-0.2,-.75)
#ax10.set_ylabel(r'$E - E_{VBM}$ (eV)')
ax01.set_xlabel(r'z ($\AA$)')
ax11.set_xlabel(r'z ($\AA$)')
ax01.xaxis.set_label_coords(0.5,-.125)
ax11.xaxis.set_label_coords(0.5,-.125)
ax00.text(-0.16,2.2,'(a)', fontweight='normal', transform=ax00.transAxes)
ax10.text(-0.15,2.2,'(b)', fontweight='normal', transform=ax10.transAxes)

###
ax_hist0 = fig.add_subplot(gs0_hist)
poscar = Poscar.from_file('ht_288_opt.vasp')
s = poscar.structure
hist_1 = []
hist_2 = []
nn = BrunnerNN_real(cutoff=4)
for ind in range(72,72+144):
    counts = [0,0,0,0]
    m = [k.species_string for k in nn.get_nn(s,ind)]
    m.sort()
    if '-'.join(m) == 'Cu-Cu-Cu-Sn':
        hist_1.append(s[ind].coords[-1])
    elif '-'.join(m) == 'Cu-Cu-Sn-Zn':
        hist_2.append(s[ind].coords[-1])

for hist in [hist_1, hist_2]:
    hist = np.array(hist)
    hist[hist >= C] -= C
    hist[hist < 0] += C

sns.histplot(hist_1, kde=False, bins=30, stat='count', color='C0', ax=ax_hist0)
#sns.histplot(hist_2, kde=False, bins=30, ax=ax_hist0)

ax_hist0.set_xlim([r[0], r[-1]])
#ax_hist0.set_ylim([0, 1])
ax_hist0.set_xticklabels([])
#ax_hist0.set_yticklabels([])
ax_hist0.set_xlabel('')
ax_hist0.set_ylabel('Count')
ax_hist0.yaxis.set_label_coords(-0.2,0.5)
#ax_hist0.axis('off')
ax_hist0.text(0.40,0.7,'Cu-Cu-Cu-Sn', fontweight='normal', transform=ax_hist0.transAxes)
ax_hist0.tick_params(axis='both', top=False, right=False)

###
ax_hist1 = fig.add_subplot(gs1_hist)
poscar = Poscar.from_file('lt_288.vasp')
s = poscar.structure
hist_1 = []
hist_2 = []
nn = BrunnerNN_real(cutoff=4)
for ind in range(72,72+144):
    counts = [0,0,0,0]
    m = [k.species_string for k in nn.get_nn(s,ind)]
    m.sort()
    if '-'.join(m) == 'Cu-Cu-Cu-Sn':
        hist_1.append(s[ind].coords[-1])
    elif '-'.join(m) == 'Cu-Cu-Sn-Zn':
        hist_2.append(s[ind].coords[-1])

for hist in [hist_1, hist_2]:
    hist = np.array(hist)
    hist[hist >= C] -= C
    hist[hist < 0] += C

#sns.histplot(hist_1, kde=False, bins=30, ax=ax_hist1)
sns.histplot(hist_2, kde=False, stat='count', color='C2', bins=30, ax=ax_hist1)

ax_hist1.set_xlim([r[0], r[-1]])
#ax_hist1.set_ylim([0, 1])
ax_hist1.set_xticklabels([])
#ax_hist1.set_yticklabels([])
ax_hist1.set_xlabel('')
ax_hist1.set_ylabel('')
#ax_hist1.axis('off')
ax_hist1.text(0.40,0.7,'Cu-Cu-Zn-Sn', fontweight='normal', transform=ax_hist1.transAxes, bbox=props)
ax_hist1.tick_params(axis='both', top=False, right=False)

##
w=31
c_vbm = 'C2'
vbm_ht = [yy[np.where(yy[np.where(ldos_ht[:,i] > 0.02)[0]]<0.1)[0].max(),0] for i in range(len(r))]
print(r.max()/len(r)*w)
vbm_smooth = fftconvolve(vbm_ht, np.ones(w)/w, 'same')
ax01.plot(r, vbm_smooth, '--', c=c_vbm, lw=1.)
#ax01.plot(r, fftconvolve(vbm_ht, gaussian(w,2)/sum(gaussian(w,2)), 'same'), '--', lw=0.8)
print(vbm_smooth.std())


vbm_lt = [yy[np.where(yy[np.where(ldos_lt[:,i] > 0.001)[0]]<0.1)[0].max(),0] for i in range(len(r))]
ax11.plot(r, fftconvolve(vbm_lt, np.ones(w)/w, 'same'), '--', c=c_vbm, lw=1.)

plt.savefig('gap_var.png', dpi=300, transparent=True)
