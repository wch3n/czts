#!/usr/bin/env python3

import numpy as np
from math import exp, ceil, floor, pi
import pandas as pd

def report(defect_list, n_site_list, efermi, x):
    print("Efermi = {0:4.2f}".format(efermi))
    #print("n: {0:8.1e}".format(calc_electrons(efermi,eg_lt)))
    print("{0:10}{1:6.0f} & \\num{{{2:.0e}}}".format("p",0,calc_holes(efermi,0)))
    idx = np.abs(x-efermi).argmin()
    for _id, d in enumerate(defect_list):
        for _iq, q in enumerate(d['q']):
            c = boltzmann(d['e'][_iq][idx], n_site_list[_id])
            print('{0:10}{1:6.0f} & \\num{{{2:.0e}}}'.format(d['type'], q, c))
            #print('{0:10}{1:6.0f} {2:.0e}'.format(d['type'], q, c))

def report_ht(defect_list, efermi, x):
    print("Efermi = {0:4.2f}".format(efermi))
    #print("n: {0:8.1e}".format(calc_electrons(efermi,eg_ht)))
    print("{0:10}{1:6.0f} & \\num{{{2:.0e}}}".format("p",0,calc_holes(efermi,0)))
    idx = np.abs(x-efermi).argmin()
    for _id, d in enumerate(defect_list):
        f = np.genfromtxt(d['type']+'_ht.sites')
        for _iq, q in enumerate(d['q']):
            c = 0
            for s in range(len(f)):
                ef = d['e'][_iq][idx] + f[s,1] - f[0,1]
                n_site = f[s, -1]
                c += boltzmann(ef, n_site)
            print('{0:10}{1:6.0f} & \\num{{{2:.0e}}}'.format(d['type'], q, c))
            #print('{0:10}{1:6.0f} {2:.0e}'.format(d['type'], q, c))

def calc_defect_concent(defect_list, n_site_list, fermi, x):
    c = 0
    idx = np.abs(x-fermi).argmin()
    for _id, d in enumerate(defect_list):
        for _iq, q in enumerate(d['q']):
            c += boltzmann(d['e'][_iq][idx], n_site_list[_id])*q
    return fermi, c

def calc_defect_concent_ht(defect_list, fermi, x):
    c = 0
    idx = np.abs(x-fermi).argmin()
    for _id, d in enumerate(defect_list):
        f = np.genfromtxt(d['type']+'_ht.sites')
        for _iq, q in enumerate(d['q']):
            for s in range(len(f)):
                ef = d['e'][_iq][idx] + f[s,1] - f[0,1]
                n_site = f[s, -1]
                c += boltzmann(ef, n_site)*q
    return fermi, c

def parse(f_params,mu,xx):
    params = np.genfromtxt(f_params)
    eform = np.zeros((len(params),N))
    eform_dict = {}
    for i in range(len(params)):
        eform[i] = list(map(lambda x: (x-xx[0])*params[i,0] + params[i,1], xx))
    eform_dict['e'] = eform + mu
    eform_dict['q'] = params[:,0]
    eform_dict['type'] = site[f_params]
    return eform_dict

def boltzmann(ef,n_sites):
    return (n_sites/VOL)*exp(-ef/(8.617333e-5*T))

def calc_holes(fermi, e_v):
    return N_v/exp((fermi - e_v)/(8.617333e-5*T2))

def calc_electrons(fermi, e_c):
    return N_c/exp((e_c - fermi)/(8.617333e-5*T2))

def calc_density_effective(m_eff,T):
    return 2*(2*pi*m_eff*m0*kb*T/h**2)**(3./2)*1e-6

N = 1001
VOL = 38895.4317*(5.2918e-9)**3
T = 800
T2 = 300
h = 6.62607004e-34
kb = 1.38064852e-23
m0 = 9.1093837e-31
N_c = calc_density_effective(0.18,T2)
N_v = calc_density_effective(0.71,T2)

print(N_c, N_v)

#chemical potentials
# Cu-poor 1
mu_1 ={'Cu':-0.50, 'Sn':-0.57, 'Zn': -1.54}
# Cu-poor 2
mu_2 ={'Cu':-0.50, 'Sn':-1.49, 'Zn': -2.02}
# Cu-rich 1
mu_3 ={'Cu':0, 'Sn':0.00, 'Zn': -1.02}
# Cu-rich 2
mu_4 ={'Cu':0, 'Sn':-0.96, 'Zn': -1.51}
# S-poor 
mu_5 = {'Cu':-0.15, 'Sn':-0.25, 'Zn': -1.30}
# S-rich
mu_6 = {'Cu':-0.60, 'Sn':-1.29, 'Zn': -1.90}
mu_list = [mu_1, mu_2, mu_3, mu_4, mu_6, mu_5]

eg_ht = 1.02
eg_lt = 1.40
xx_lt = np.linspace(0, eg_lt, N)
xx_ht = np.linspace(0, eg_ht, N)

site = {"sn_cu_ht.params":"sn_cu", "sn_zn_ht.params":"sn_zn", "zn_cu_ht.params": "zn_cu", "cu_zn_ht.params": "cu_zn", "v_cu_ht.params": "v_cu", "v_zn_ht.params": "v_zn", "zn_sn_ht.params": "zn_sn", "cu_sn_ht.params":"cu_sn", "v_sn_ht.params": "v_sn", "cl_sn_zn_ht.params":"cl_sn_zn", "cl_sn_zn_1_ht.params":"cl_sn_zn_1", "sncu_cusn_ht.params":"sncu_cusn", "snzn_znsn_ht.params":"snzn_znsn", "snzn_vzn_ht.params":"snzn_vzn", "snzn_2vcu_ht.params":"snzn_2vcu",
        "sn_cu_lt.params":"sn_cu", "sn_zn_lt.params":"sn_zn", "zn_cu_lt.params": "zn_cu", "cu_zn_lt.params": "cu_zn", "v_cu_lt.params": "v_cu", "v_zn_lt.params": "v_zn", "zn_sn_lt.params": "zn_sn", "cu_sn_lt.params":"cu_sn", "v_sn_lt.params": "v_sn", "cl_sn_zn_lt.params":"cl_sn_zn", "cl_sn_zn_1_lt.params":"cl_sn_zn_1", "sncu_cusn_lt.params":"sncu_cusn","snzn_znsn_lt.params":"snzn_znsn","snzn_vzn_lt.params":"snzn_vzn", "snzn_2vcu_lt.params":"snzn_2vcu"}

for mu in mu_list:
    print(mu)
    eform_szvz_lt = parse('snzn_vzn_lt.params',2*mu['Zn']-mu['Sn'],xx_lt)
    eform_sz2vc_lt = parse('snzn_2vcu_lt.params',2*mu['Cu']+mu['Zn']-mu['Sn'],xx_lt)
    eform_cl_lt = parse('cl_sn_zn_lt.params',3*mu['Zn']-2*mu['Cu']-mu['Sn'],xx_lt)
    eform_cl1_lt = parse('cl_sn_zn_1_lt.params',2*mu['Zn']-1*mu['Cu']-mu['Sn'],xx_lt)
    eform_sccs_lt = parse('sncu_cusn_lt.params',0,xx_lt)
    eform_szzs_lt = parse('snzn_znsn_lt.params',0,xx_lt)
    eform_cu_lt = parse('sn_cu_lt.params',mu['Cu']-mu['Sn'],xx_lt)
    eform_zn_lt = parse('sn_zn_lt.params',mu['Zn']-mu['Sn'],xx_lt)
    eform_zc_lt = parse('zn_cu_lt.params',mu['Cu']-mu['Zn'],xx_lt)
    eform_cz_lt = parse('cu_zn_lt.params',mu['Zn']-mu['Cu'],xx_lt)
    eform_vc_lt = parse('v_cu_lt.params',mu['Cu'],xx_lt)
    eform_vz_lt = parse('v_zn_lt.params',mu['Zn'],xx_lt)
    eform_zs_lt = parse('zn_sn_lt.params',mu['Sn']-mu['Zn'],xx_lt)
    eform_vs_lt = parse('v_sn_lt.params',mu['Sn'],xx_lt)
    eform_cs_lt = parse('cu_sn_lt.params',mu['Sn']-mu['Cu'],xx_lt)

    results = np.zeros((N,3))
    defect_list = [eform_vc_lt, eform_vz_lt, eform_vs_lt, eform_cz_lt, eform_cs_lt, eform_zs_lt, eform_zc_lt, eform_cu_lt, eform_zn_lt, eform_sccs_lt, eform_szzs_lt, eform_szvz_lt, eform_sz2vc_lt, eform_cl_lt, eform_cl1_lt]
    n_site_list = [72, 36, 36, 36, 36, 36, 72, 72, 36, 72, 36, 36, 36, 36, 36]
    for i, fermi in enumerate(xx_lt):
        results[i,:2] = calc_defect_concent(defect_list, n_site_list, fermi, xx_lt)
        results[i,2] = calc_holes(fermi, 0) - calc_electrons(fermi, eg_lt)
    efermi = results[np.abs(results[:,1] + results[:,2]).argmin()][0]
    report(defect_list, n_site_list, efermi, xx_lt)

for mu in mu_list:
    print(mu)
    eform_szvz = parse('snzn_vzn_ht.params',2*mu['Zn']-mu['Sn'],xx_ht)
    eform_sz2vc = parse('snzn_2vcu_ht.params',2*mu['Cu']+mu['Zn']-mu['Sn'],xx_ht)
    eform_cl = parse('cl_sn_zn_ht.params',3*mu['Zn']-2*mu['Cu']-mu['Sn'],xx_ht)
    eform_cl1 = parse('cl_sn_zn_1_ht.params',2*mu['Zn']-1*mu['Cu']-mu['Sn'],xx_ht)
    eform_sccs = parse('sncu_cusn_ht.params',0,xx_ht)
    eform_szzs = parse('snzn_znsn_ht.params',0,xx_ht)
    eform_cu = parse('sn_cu_ht.params',mu['Cu']-mu['Sn'],xx_ht)
    eform_zn = parse('sn_zn_ht.params',mu['Zn']-mu['Sn'],xx_ht)
    eform_zc = parse('zn_cu_ht.params',mu['Cu']-mu['Zn'],xx_ht)
    eform_cz = parse('cu_zn_ht.params',mu['Zn']-mu['Cu'],xx_ht)
    eform_vc = parse('v_cu_ht.params',mu['Cu'],xx_ht)
    eform_vz = parse('v_zn_ht.params',mu['Zn'],xx_ht)
    eform_zs = parse('zn_sn_ht.params',mu['Sn']-mu['Zn'],xx_ht)
    eform_vs = parse('v_sn_ht.params',mu['Sn'],xx_ht)
    eform_cs = parse('cu_sn_ht.params',mu['Sn']-mu['Cu'],xx_ht)

    results = np.zeros((N,3))
    defect_list = [eform_vc, eform_vz, eform_vs, eform_cz, eform_cs, eform_zs, eform_zc, eform_cu, eform_zn, eform_sccs, eform_szzs, eform_szvz, eform_sz2vc, eform_cl, eform_cl1]
    for i, fermi in enumerate(xx_ht):
        results[i,:2] = calc_defect_concent_ht(defect_list, fermi, xx_ht)
        results[i,2] = calc_holes(fermi, 0) - calc_electrons(fermi, eg_ht)
    efermi = results[np.abs(results[:,1] + results[:,2]).argmin()][0]
    report_ht(defect_list, efermi, xx_ht)
