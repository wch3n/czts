#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import seaborn as sns
import pandas as pd
from matplotlib.collections import PatchCollection
import matplotlib.patches as mpatches

def parse(params,mu,X):
    params = np.genfromtxt(params)
    n_plt = params.shape[1] - 1
    eform_dict = {}
    ctl_dict = {}
    for il in range(n_plt):
        eform = np.zeros((len(params), 101))
        for i in range(len(params)):
            eform[i] = list(map(lambda x: (x-X[0])*params[i,0] + params[i,il+1], X))
        eform_min = np.amin(eform, axis=0) + mu
        #
        _c = [params[i,il+1] - params[i+1,il+1] + X[0] for i in range(len(params)-1)]
        _ctl = []
        i = 0
        while i < len(_c):
            if (i == len(_c)-1) or (_c[i] > _c[i+1]):
                _ctl.append(_c[i])
                i += 1
            else:
                _ctl.append(0.5*(_c[i]+_c[i+1]))
                i += 2
        ctl = np.zeros((len(_ctl),2))
        for i,v in enumerate(_ctl):
            ctl[i,0] = v
            ctl[i,1] = eform_min[np.argmin(np.abs(X-v))]
        eform_dict[il] = eform_min
        ctl_dict[il] = ctl
    return eform_dict, ctl_dict

def plot_ctl(x, eform_dict, ctl_dict, color, typ, ax, zorder=9):
    if typ == 'ht':
        for i in range(len(eform_dict)):
            ax.plot(x, eform_dict[i], c=color, lw=0.8, solid_capstyle='round', zorder=zorder)
            ax.scatter(ctl_dict[i][:,0], ctl_dict[i][:,1], c=color)
    elif typ =='lt':
        for i in range(len(eform_dict)):
            ax.plot(x, eform_dict[i], '--', c=color, lw=0.4, zorder=zorder-1)
            ax.scatter(ctl_dict[i][:,0], ctl_dict[i][:,1], facecolor='None', edgecolor=color, zorder=zorder-1)

fig_width = 12/2.54
fig_height = fig_width*0.50
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 1.0,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'ytick.right': False,
          'axes.spines.top': False,
          'axes.spines.right': False,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

mu_1 ={'Cu':-0.5, 'Sn':-0.40, 'Zn': -1.45}
fermi = {"ht": 0.34, "lt":0.61}
eg_ht = 1.02
dvbm = -0.28+0.28
dcbm = 0.06+0.28
yo = -0.3; ye = 3.0
fig = plt.figure()
(ax1,ax0) = fig.subplots(1,2, sharey=False)
x_ht = np.linspace(0,eg_ht,101)
x_lt = np.linspace(dvbm, eg_ht+dcbm, 101)
patches = []

kwargs = {"fontsize":6}

eform_cl, ctl_cl = parse('cl_sn_zn_ht.params',3*mu_1['Zn']-2*mu_1['Cu']-mu_1['Sn'],x_ht)
eform_cl_lt, ctl_cl_lt = parse('cl_sn_zn_lt.params',3*mu_1['Zn']-2*mu_1['Cu']-mu_1['Sn'],x_lt)
eform_cu, ctl_cu = parse('sn_cu_ht.params',mu_1['Cu']-mu_1['Sn'],x_ht)
eform_zn, ctl_zn = parse('sn_zn_ht.params',mu_1['Zn']-mu_1['Sn'],x_ht)
eform_zc, ctl_zc = parse('zn_cu_ht.params',mu_1['Cu']-mu_1['Zn'],x_ht)
eform_cu_lt, ctl_cu_lt = parse('sn_cu_lt.params',mu_1['Cu']-mu_1['Sn'],x_lt)
eform_zn_lt, ctl_zn_lt = parse('sn_zn_lt.params',mu_1['Zn']-mu_1['Sn'],x_lt)
eform_zc_lt, ctl_zc_lt = parse('zn_cu_lt.params',mu_1['Cu']-mu_1['Zn'],x_lt)
plot_ctl(x_ht, eform_cl, ctl_cl, 'C5', 'ht', ax0, zorder=1)
plot_ctl(x_ht, eform_cu, ctl_cu, 'C0', 'ht', ax0, zorder=9)
plot_ctl(x_ht, eform_zn, ctl_zn, 'C3', 'ht', ax0, zorder=9)
plot_ctl(x_ht, eform_zc, ctl_zc, 'C2', 'ht', ax0, zorder=9)
plot_ctl(x_lt, eform_cl_lt, ctl_cl_lt, 'C5', 'lt', ax0, zorder=0)
plot_ctl(x_lt, eform_cu_lt, ctl_cu_lt, 'C0', 'lt', ax0, zorder=9)
plot_ctl(x_lt, eform_zn_lt, ctl_zn_lt, 'C3', 'lt', ax0, zorder=9)
plot_ctl(x_lt, eform_zc_lt, ctl_zc_lt, 'C2', 'lt', ax0, zorder=9)
ax0.imshow([[0.,0.],[1.,1]], cmap=cm.Blues, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
#ax0.axvline(0,lw=0.8, color='k')
ax0.axvline(eg_ht, lw=0.8, color='gray')
ax0.set_xlim([dvbm,eg_ht+dcbm])
ax0.set_ylim([yo,ye])
ax0.yaxis.set_major_locator(MultipleLocator(1.0))
ax0.yaxis.set_minor_locator(MultipleLocator(0.5))
ax0.xaxis.set_major_locator(MultipleLocator(0.4))
ax0.xaxis.set_minor_locator(MultipleLocator(0.2))
ax0.text(0.55,0.82,r'Sn$_{Cu}$',color='C0',weight='normal',transform=ax0.transAxes, **kwargs)
ax0.text(0.50,0.49,r'Sn$_{Zn}$',color='C3',weight='normal',transform=ax0.transAxes, **kwargs)
ax0.text(0.55,0.15,r'Zn$_{Cu}$',color='C2',weight='normal',transform=ax0.transAxes, **kwargs)
ax0.text(0.30,0.30,r'2Cu$_{Zn}$+Sn$_{Zn}$',color='C5',weight='normal',transform=ax0.transAxes, **kwargs)
ax0.set_xlabel('Fermi energy (eV)')
ax0.tick_params(labelleft=False)

eform_cz, ctl_cz = parse('cu_zn_ht.params',mu_1['Zn']-mu_1['Cu'],x_ht)
eform_vc, ctl_vc = parse('v_cu_ht.params',mu_1['Cu'],x_ht)
eform_vs, ctl_vs = parse('v_sn_ht.params',mu_1['Sn'],x_ht)
eform_vz, ctl_vz = parse('v_zn_ht.params',mu_1['Zn'],x_ht)
eform_cs, ctl_cs = parse('cu_sn_ht.params',mu_1['Sn']-mu_1['Cu'],x_ht)
eform_zs, ctl_zs = parse('zn_sn_ht.params',mu_1['Sn']-mu_1['Zn'],x_ht)
eform_cz_lt, ctl_cz_lt = parse('cu_zn_lt.params',mu_1['Zn']-mu_1['Cu'],x_lt)
eform_vc_lt, ctl_vc_lt = parse('v_cu_lt.params',mu_1['Cu'],x_lt)
eform_vs_lt, ctl_vs_lt = parse('v_sn_lt.params',mu_1['Sn'],x_lt)
eform_vz_lt, ctl_vz_lt = parse('v_zn_lt.params',mu_1['Zn'],x_lt)
eform_cs_lt, ctl_cs_lt = parse('cu_sn_lt.params',mu_1['Sn']-mu_1['Cu'],x_lt)
eform_zs_lt, ctl_zs_lt = parse('zn_sn_lt.params',mu_1['Sn']-mu_1['Zn'],x_lt)

plot_ctl(x_ht, eform_vs, ctl_vs, 'C8', 'ht', ax1)
plot_ctl(x_ht, eform_vz, ctl_vz, 'C7', 'ht', ax1)
plot_ctl(x_ht, eform_cs, ctl_cs, 'C0', 'ht', ax1)
plot_ctl(x_ht, eform_zs, ctl_zs, 'C3', 'ht', ax1)
plot_ctl(x_ht, eform_cz, ctl_cz, 'C2', 'ht', ax1)
plot_ctl(x_ht, eform_vc, ctl_vc, 'C4', 'ht', ax1)
plot_ctl(x_lt, eform_vs_lt, ctl_vs_lt, 'C8', 'lt', ax1)
plot_ctl(x_lt, eform_vz_lt, ctl_vz_lt, 'C7', 'lt', ax1)
plot_ctl(x_lt, eform_cs_lt, ctl_cs_lt, 'C0', 'lt', ax1)
plot_ctl(x_lt, eform_zs_lt, ctl_zs_lt, 'C3', 'lt', ax1)
plot_ctl(x_lt, eform_cz_lt, ctl_cz_lt, 'C2', 'lt', ax1)
plot_ctl(x_lt, eform_vc_lt, ctl_vc_lt, 'C4', 'lt', ax1)
ax1.imshow([[0.,0.],[1.,1]], cmap=cm.Blues, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
ax1.axvline(eg_ht, lw=0.8, color='gray')
ax1.set_xlim([dvbm,eg_ht+dcbm])
ax1.set_ylim([yo,ye])
ax1.xaxis.set_major_locator(MultipleLocator(0.4))
ax1.xaxis.set_minor_locator(MultipleLocator(0.2))
ax1.text(0.20,0.42,r'V$_{Zn}$',color='C7',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.10,0.05,r'Cu$_{Zn}$',color='C2',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.15,0.15,r'V$_{Cu}$',color='C4',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.10,0.55,r'Cu$_{Sn}$',color='C0',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.22,0.20,r'Zn$_{Sn}$',color='C3',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.62,0.80,r'V$_{Sn}$',color='C8',weight='normal',transform=ax1.transAxes,**kwargs)

ax1.set_xlabel('Fermi energy (eV)')
ax1.set_ylabel('Formation energy (eV)')

ax1.text(0.05,0.95,'(a)', transform=ax1.transAxes)
ax0.text(0.05,0.95,'(b)', transform=ax0.transAxes)

'''for ax in [ax0,ax1]:
    ax.axvline(fermi['ht'],ls='-',lw=0.5)
    ax.axvline(fermi['lt'],ls='--',lw=0.5)
'''

plt.tight_layout(pad=0.2, h_pad=1.0, w_pad=0.5)
plt.savefig('def_level_w.pdf')
