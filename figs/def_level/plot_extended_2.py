#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import seaborn as sns
import pandas as pd
from matplotlib.collections import PatchCollection

def parse(params,mu,X):
    params = np.genfromtxt(params)
    n_plt = params.shape[1] - 1
    eform_dict = {}
    ctl_dict = {}
    for il in range(n_plt):
        eform = np.zeros((len(params), 101))
        for i in range(len(params)):
            eform[i] = list(map(lambda x: (x-X[0])*params[i,0] + params[i,il+1], X))
        eform_min = np.amin(eform, axis=0) + mu
        #
        _c = [params[i,il+1] - params[i+1,il+1] + X[0] for i in range(len(params)-1)]
        _ctl = []
        i = 0
        while i < len(_c):
            if (i == len(_c)-1) or (_c[i] > _c[i+1]):
                _ctl.append(_c[i])
                i += 1
            else:
                _ctl.append(0.5*(_c[i]+_c[i+1]))
                i += 2
        ctl = np.zeros((len(_ctl),2))
        for i,v in enumerate(_ctl):
            ctl[i,0] = v
            ctl[i,1] = eform_min[np.argmin(np.abs(X-v))]
        eform_dict[il] = eform_min
        ctl_dict[il] = ctl
    return eform_dict, ctl_dict

def plot_ctl(x, eform_dict, ctl_dict, color, typ, ax, zorder=9, style='solid'):
    if typ == 'ht':
        for i in range(len(eform_dict)):
            ax.plot(x, eform_dict[i], c=color, lw=0.8, linestyle=style, solid_capstyle='round', zorder=zorder)
            ax.scatter(ctl_dict[i][:,0], ctl_dict[i][:,1], c=color, clip_on=True)
    elif typ =='lt':
        for i in range(len(eform_dict)):
            ax.plot(x, eform_dict[i], '-', c=color, lw=0.8, linestyle=style, zorder=zorder-1)
            ax.scatter(ctl_dict[i][:,0], ctl_dict[i][:,1], facecolor='None', edgecolor=color, zorder=zorder-1, clip_on=True)

fig_width = 8.5/2.54
fig_height = fig_width*1.05
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'ytick.right': False,
          'axes.spines.top': False,
          'axes.spines.right': False,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)
props = dict(boxstyle='square,pad=0.2', lw=0.5, edgecolor='none', facecolor='white', alpha=0.7)

# ordered
#mu_1 ={'Cu':-0.5, 'Sn':-0.40, 'Zn': -1.45} 
# disordered 
mu_1 ={'Cu':-0.50, 'Sn':-0.57, 'Zn': -1.54}
fermi = {"ht": 0.25, "lt":0.52}
eg_ht = 1.02
dvbm = -0.28+0.28
dcbm = 0.06+0.28
yo = -0.3; ye = 2.1+1

fig = plt.figure(constrained_layout=False)
(ax5,ax4),(ax7,ax6) = fig.subplots(2,2, sharey=True, sharex=False, gridspec_kw={'width_ratios':[1.0,1.1]})

x_ht = np.linspace(0,eg_ht,101)
x_lt = np.linspace(dvbm, eg_ht+dcbm, 101)

kwargs = {"fontsize":6}
'''
eform_cu, ctl_cu = parse('sn_cu_ht.params',mu_1['Cu']-mu_1['Sn'],x_ht)
eform_zn, ctl_zn = parse('sn_zn_ht.params',mu_1['Zn']-mu_1['Sn'],x_ht)
eform_zc, ctl_zc = parse('zn_cu_ht.params',mu_1['Cu']-mu_1['Zn'],x_ht)
eform_cu_lt, ctl_cu_lt = parse('sn_cu_lt.params',mu_1['Cu']-mu_1['Sn'],x_lt)
eform_zn_lt, ctl_zn_lt = parse('sn_zn_lt.params',mu_1['Zn']-mu_1['Sn'],x_lt)
eform_zc_lt, ctl_zc_lt = parse('zn_cu_lt.params',mu_1['Cu']-mu_1['Zn'],x_lt)

plot_ctl(x_ht, eform_cu, ctl_cu, 'C0', 'ht', ax3, zorder=9)
plot_ctl(x_ht, eform_zn, ctl_zn, 'C3', 'ht', ax3, zorder=9)
plot_ctl(x_ht, eform_zc, ctl_zc, 'C2', 'ht', ax3, zorder=9)

plot_ctl(x_lt, eform_cu_lt, ctl_cu_lt, 'C0', 'lt', ax2, zorder=9)
plot_ctl(x_lt, eform_zn_lt, ctl_zn_lt, 'C3', 'lt', ax2, zorder=9)
plot_ctl(x_lt, eform_zc_lt, ctl_zc_lt, 'C2', 'lt', ax2, zorder=9)
ax3.imshow([[0.,0.],[1.,1]], cmap=cm.Blues, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
ax2.imshow([[0.,0.],[1.,1]], cmap=cm.Purples, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
ax3.set_xlim([dvbm,eg_ht])
ax3.set_ylim([yo,ye])
ax2.set_xlim([dvbm,eg_ht+dcbm])
ax2.set_ylim([yo,ye])

ax3.text(0.60,0.75,r'Sn$_{Cu}$',color='C0',weight='normal',transform=ax3.transAxes, **kwargs)
ax3.text(0.60,0.45,r'Sn$_{Zn}$',color='C3',weight='normal',transform=ax3.transAxes, **kwargs)
ax3.text(0.60,0.12,r'Zn$_{Cu}$',color='C2',weight='normal',transform=ax3.transAxes, **kwargs)

##
eform_cz, ctl_cz = parse('cu_zn_ht.params',mu_1['Zn']-mu_1['Cu'],x_ht)
eform_vc, ctl_vc = parse('v_cu_ht.params',mu_1['Cu'],x_ht)
eform_vs, ctl_vs = parse('v_sn_ht.params',mu_1['Sn'],x_ht)
eform_vz, ctl_vz = parse('v_zn_ht.params',mu_1['Zn'],x_ht)
eform_cs, ctl_cs = parse('cu_sn_ht.params',mu_1['Sn']-mu_1['Cu'],x_ht)
eform_zs, ctl_zs = parse('zn_sn_ht.params',mu_1['Sn']-mu_1['Zn'],x_ht)
eform_cz_lt, ctl_cz_lt = parse('cu_zn_lt.params',mu_1['Zn']-mu_1['Cu'],x_lt)
eform_vc_lt, ctl_vc_lt = parse('v_cu_lt.params',mu_1['Cu'],x_lt)
eform_vs_lt, ctl_vs_lt = parse('v_sn_lt.params',mu_1['Sn'],x_lt)
eform_vz_lt, ctl_vz_lt = parse('v_zn_lt.params',mu_1['Zn'],x_lt)
eform_cs_lt, ctl_cs_lt = parse('cu_sn_lt.params',mu_1['Sn']-mu_1['Cu'],x_lt)
eform_zs_lt, ctl_zs_lt = parse('zn_sn_lt.params',mu_1['Sn']-mu_1['Zn'],x_lt)

plot_ctl(x_ht, eform_vs, ctl_vs, 'C8', 'ht', ax1)
plot_ctl(x_ht, eform_vz, ctl_vz, 'C7', 'ht', ax1)
plot_ctl(x_ht, eform_cs, ctl_cs, 'C0', 'ht', ax1)
plot_ctl(x_ht, eform_zs, ctl_zs, 'C3', 'ht', ax1)
plot_ctl(x_ht, eform_cz, ctl_cz, 'C2', 'ht', ax1)
plot_ctl(x_ht, eform_vc, ctl_vc, 'C4', 'ht', ax1)

plot_ctl(x_lt, eform_vs_lt, ctl_vs_lt, 'C8', 'lt', ax0)
plot_ctl(x_lt, eform_vz_lt, ctl_vz_lt, 'C7', 'lt', ax0)
plot_ctl(x_lt, eform_cs_lt, ctl_cs_lt, 'C0', 'lt', ax0)
plot_ctl(x_lt, eform_zs_lt, ctl_zs_lt, 'C3', 'lt', ax0)
plot_ctl(x_lt, eform_cz_lt, ctl_cz_lt, 'C2', 'lt', ax0)
plot_ctl(x_lt, eform_vc_lt, ctl_vc_lt, 'C4', 'lt', ax0)

ax1.imshow([[0.,0.],[1.,1]], cmap=cm.Blues, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
ax0.imshow([[0.,0.],[1.,1]], cmap=cm.Purples, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
ax1.set_xlim([dvbm,eg_ht])
ax0.set_xlim([dvbm,eg_ht+dcbm])
ax1.set_ylim([yo,ye])
ax0.set_ylim([yo,ye])

ax1.text(0.80,0.82,r'V$_{Sn}$',color='C8',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.50,0.43,r'Cu$_{Sn}$',color='C0',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.30,0.35,r'V$_{Zn}$',color='C7',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.05,0.30,r'Zn$_{Sn}$',color='C3',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.05,0.18,r'V$_{Cu}$',color='C4',weight='normal',transform=ax1.transAxes,**kwargs)
ax1.text(0.05,0.04,r'Cu$_{Zn}$',color='C2',weight='normal',transform=ax1.transAxes,**kwargs)

'''
#
eform_cl, ctl_cl = parse('cl_sn_zn_ht.params',3*mu_1['Zn']-2*mu_1['Cu']-mu_1['Sn'],x_ht)
eform_cl1, ctl_cl1 = parse('cl_sn_zn_1_ht.params',2*mu_1['Zn']-mu_1['Cu']-mu_1['Sn'],x_ht)
eform_sc_cs, ctl_sc_cs = parse('sncu_cusn_ht.params', 0, x_ht)
eform_sz_zs, ctl_sz_zs = parse('snzn_znsn_ht.params', 0, x_ht)
eform_sz_vz, ctl_sz_vz = parse('snzn_vzn_ht.params', 2*mu_1['Zn']-mu_1['Sn'], x_ht)
eform_sz_2vc, ctl_sz_2vc = parse('snzn_2vcu_ht.params', 2*mu_1['Cu']+mu_1['Zn']-mu_1['Sn'], x_ht)

eform_cl_lt, ctl_cl_lt = parse('cl_sn_zn_lt.params',3*mu_1['Zn']-2*mu_1['Cu']-mu_1['Sn'],x_lt)
eform_cl1_lt, ctl_cl1_lt = parse('cl_sn_zn_1_lt.params',2*mu_1['Zn']-mu_1['Cu']-mu_1['Sn'],x_lt)
eform_sc_cs_lt, ctl_sc_cs_lt = parse('sncu_cusn_lt.params', 0, x_lt)
eform_sz_zs_lt, ctl_sz_zs_lt = parse('snzn_znsn_lt.params', 0, x_lt)
eform_sz_vz_lt, ctl_sz_vz_lt = parse('snzn_vzn_lt.params', 2*mu_1['Zn']-mu_1['Sn'], x_lt)
eform_sz_2vc_lt, ctl_sz_2vc_lt = parse('snzn_2vcu_lt.params', 2*mu_1['Cu']+mu_1['Zn']-mu_1['Sn'], x_lt)

plot_ctl(x_ht, eform_sc_cs, ctl_sc_cs, 'C0', 'ht', ax5, zorder=1)
plot_ctl(x_ht, eform_sz_zs, ctl_sz_zs, 'C4', 'ht', ax5, zorder=1)
plot_ctl(x_lt, eform_sc_cs_lt, ctl_sc_cs_lt, 'C0', 'lt', ax4, zorder=1)
plot_ctl(x_lt, eform_sz_zs_lt, ctl_sz_zs_lt, 'C4', 'lt', ax4, zorder=1)

ax5.text(0.33,0.56,r'Sn$_{Cu}$+Cu$_{Sn}$',color='C0',weight='normal',transform=ax5.transAxes, **kwargs, zorder=10)
ax5.text(0.33,0.22,r'Sn$_{Zn}$+Zn$_{Sn}$',color='C4',weight='normal',transform=ax5.transAxes, **kwargs, zorder=10)

ax5.imshow([[0.,0.],[1.,1]], cmap=cm.Blues, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
ax4.imshow([[0.,0.],[1.,1]], cmap=cm.Purples, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')

ax5.set_xlim([dvbm,eg_ht])
ax5.set_ylim([yo,ye])
ax4.set_xlim([dvbm,eg_ht+dcbm])
ax4.set_ylim([yo,ye])
#

plot_ctl(x_ht, eform_cl1, ctl_cl1, 'C2', 'ht', ax7, zorder=1, style='dashed')
plot_ctl(x_ht, eform_cl, ctl_cl, 'C3', 'ht', ax7, zorder=1)
plot_ctl(x_ht, eform_sz_vz, ctl_sz_vz, 'C5', 'ht', ax7, zorder=1)
plot_ctl(x_ht, eform_sz_2vc, ctl_sz_2vc, 'C7', 'ht', ax7, zorder=1)
plot_ctl(x_lt, eform_cl1_lt, ctl_cl1_lt, 'C2', 'lt', ax6, zorder=0, style='dashed')
plot_ctl(x_lt, eform_cl_lt, ctl_cl_lt, 'C3', 'lt', ax6, zorder=2)
plot_ctl(x_lt, eform_sz_vz_lt, ctl_sz_vz_lt, 'C5', 'lt', ax6, zorder=0)
plot_ctl(x_lt, eform_sz_2vc_lt, ctl_sz_2vc_lt, 'C7', 'lt', ax6, zorder=0)

ax7.imshow([[0.,0.],[1.,1]], cmap=cm.Blues, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
ax6.imshow([[0.,0.],[1.,1]], cmap=cm.Purples, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')

ax6.text(0.10,0.30,r'Sn$_{Zn}$+Cu$_{Zn}$',color='C2',weight='normal',transform=ax6.transAxes, **kwargs)
ax7.text(0.45,0.12,r'Sn$_{Zn}$+2Cu$_{Zn}$',color='C3',weight='normal',transform=ax7.transAxes, **kwargs, zorder=10)
ax7.text(0.45,0.30,r'Sn$_{Zn}$+V$_{Zn}$',color='C5',weight='normal',transform=ax7.transAxes, **kwargs, zorder=10)
ax7.text(0.33,0.52,r'Sn$_{Zn}$+2V$_{Cu}$',color='C7',weight='normal',transform=ax7.transAxes, **kwargs, zorder=10)
ax7.set_xlim([dvbm,eg_ht])
ax7.set_ylim([yo,ye])
ax6.set_xlim([dvbm,eg_ht+dcbm])
ax6.set_ylim([yo,ye])

for _ax in [ax6,ax7]:
    _ax.set_xlabel('Fermi energy (eV)')

#ax1.set_ylabel('Formation energy (eV)')
#ax3.set_ylabel('Formation energy (eV)')
ax5.set_ylabel('Formation energy (eV)')
ax7.set_ylabel('Formation energy (eV)')

#ax1.text(0.05,0.95,'(a)', weight='normal',  transform=ax1.transAxes, zorder=99)
#ax0.text(0.05,0.95,'(b)', weight='normal',  transform=ax0.transAxes, zorder=99)
#ax3.text(0.05,0.95,'(c)', weight='normal',  transform=ax3.transAxes, zorder=99)
#ax2.text(0.05,0.95,'(d)', weight='normal',  transform=ax2.transAxes, zorder=99)
ax5.text(0.05,0.95,'(a)', weight='normal',  transform=ax5.transAxes, zorder=99)
ax4.text(0.05,0.95,'(b)', weight='normal',  transform=ax4.transAxes, zorder=99)
ax7.text(0.05,0.95,'(c)', weight='normal',  transform=ax7.transAxes, zorder=99)
ax6.text(0.05,0.95,'(d)', weight='normal',  transform=ax6.transAxes, zorder=99)

for _ax in [ax5,ax7]:
    _ax.axvline(fermi['ht'],ls='--',c='gray',lw=0.5)
for _ax in [ax4,ax6]:
    _ax.axvline(fermi['lt'],ls='--',c='gray',lw=0.5)

for _ax in [ax4,ax5,ax6,ax7]:
    _ax.yaxis.set_major_locator(MultipleLocator(1.0))
    _ax.yaxis.set_minor_locator(MultipleLocator(0.5))
    _ax.xaxis.set_major_locator(MultipleLocator(0.4))
    _ax.xaxis.set_minor_locator(MultipleLocator(0.2))

plt.tight_layout(pad=0.2, h_pad=1.0, w_pad=0.5)
plt.savefig('def_level_2.pdf')
