#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors
from matplotlib.ticker import MaxNLocator, MultipleLocator
import seaborn as sns
import pandas as pd
from matplotlib.collections import PatchCollection

def parse(params,mu,X):
    params = np.genfromtxt(params)
    n_plt = params.shape[1] - 1
    eform_dict = {}
    ctl_dict = {}
    for il in range(n_plt):
        eform = np.zeros((len(params), 101))
        for i in range(len(params)):
            eform[i] = list(map(lambda x: (x-X[0])*params[i,0] + params[i,il+1], X))
        eform_min = np.amin(eform, axis=0) + mu
        #
        _c = [params[i,il+1] - params[i+1,il+1] + X[0] for i in range(len(params)-1)]
        _ctl = []
        i = 0
        while i < len(_c):
            if (i == len(_c)-1) or (_c[i] > _c[i+1]):
                _ctl.append(_c[i])
                i += 1
            else:
                _ctl.append(0.5*(_c[i]+_c[i+1]))
                i += 2
        ctl = np.zeros((len(_ctl),2))
        for i,v in enumerate(_ctl):
            ctl[i,0] = v
            ctl[i,1] = eform_min[np.argmin(np.abs(X-v))]
        eform_dict[il] = eform_min
        ctl_dict[il] = ctl
    return eform_dict, ctl_dict

def plot_ctl(x, eform_dict, ctl_dict, color, typ, ax):
    if typ == 'ht':
        for i in range(len(eform_dict)):
            ax.plot(x, eform_dict[i], c=color, lw=0.8, solid_capstyle='round', zorder=9)
            ax.scatter(ctl_dict[i][:,0], ctl_dict[i][:,1], c=color)
    elif typ =='lt':
        for i in range(len(eform_dict)):
            ax.plot(x, eform_dict[i], '--', c=color, lw=0.5, zorder=8)
            ax.scatter(ctl_dict[i][:,0], ctl_dict[i][:,1], facecolor='None', edgecolor=color, zorder=8)

fig_width = 16/2.54
fig_height = fig_width*1.05
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.8,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 2.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': False,
          'ytick.right': False,
          'axes.spines.top': False,
          'axes.spines.right': False,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.4,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.4}
plt.rcParams.update(params)

# Cu-poor 1
mu_1 ={'Cu':-0.50, 'Sn':-0.57, 'Zn': -1.54}
# Cu-poor 2
mu_2 ={'Cu':-0.50, 'Sn':-1.49, 'Zn': -2.02}
# Cu-rich 1
mu_3 ={'Cu':0, 'Sn':0.00, 'Zn': -1.02}
# Cu-rich 2
mu_4 ={'Cu':0, 'Sn':-0.96, 'Zn': -1.51}
# S-poor 
mu_5 = {'Cu':-0.15, 'Sn':-0.25, 'Zn': -1.30}
# S-rich
mu_6 = {'Cu':-0.60, 'Sn':-1.29, 'Zn': -1.90}

mu = [mu_1, mu_3, mu_2, mu_4, mu_6, mu_5]

eg_ht = 1.02
dvbm = -0.28+0.28
dcbm = 0.06+0.28
yo = -0.5; ye = 3.
x_ht = np.linspace(0,eg_ht,101)
x_lt = np.linspace(dvbm, eg_ht+dcbm, 101)

fig = plt.figure(constrained_layout=False)
#plt.subplots_adjust(left=0.06, bottom=0.05, right=0.99, top=0.99, wspace=0.05, hspace=0.2)
gs0 = fig.add_gridspec(3,2)
gs0.update(left=0.06, bottom=0.05, right=0.99, top=0.99, wspace=0.2, hspace=0.15)

pd_label = ['P1','R1','P2','R2','RS','PS']
props = dict(boxstyle='square,pad=0.5', lw=0.5, edgecolor='gray', facecolor='white', alpha=1)

for _i, _mu in enumerate(mu):
    _gs = gs0[_i].subgridspec(1,2,wspace=0.05)
    ax1 = fig.add_subplot(_gs[0])
    ax0 = fig.add_subplot(_gs[1])


    eform_cu, ctl_cu = parse('sn_cu_ht.params',_mu['Cu']-_mu['Sn'],x_ht)
    eform_zn, ctl_zn = parse('sn_zn_ht.params',_mu['Zn']-_mu['Sn'],x_ht)
    eform_zc, ctl_zc = parse('zn_cu_ht.params',_mu['Cu']-_mu['Zn'],x_ht)
    eform_cu_lt, ctl_cu_lt = parse('sn_cu_lt.params',_mu['Cu']-_mu['Sn'],x_lt)
    eform_zn_lt, ctl_zn_lt = parse('sn_zn_lt.params',_mu['Zn']-_mu['Sn'],x_lt)
    eform_zc_lt, ctl_zc_lt = parse('zn_cu_lt.params',_mu['Cu']-_mu['Zn'],x_lt)
    eform_cl, ctl_cl = parse('cl_sn_zn_ht.params',3*_mu['Zn']-2*_mu['Cu']-_mu['Sn'],x_ht)
    eform_cl_lt, ctl_cl_lt = parse('cl_sn_zn_lt.params',3*_mu['Zn']-2*_mu['Cu']-_mu['Sn'],x_lt)
    eform_cl1, ctl_cl1 = parse('cl_sn_zn_1_ht.params',2*_mu['Zn']-_mu['Cu']-_mu['Sn'],x_ht)
    eform_cl1_lt, ctl_cl1_lt = parse('cl_sn_zn_1_lt.params',2*_mu['Zn']-_mu['Cu']-_mu['Sn'],x_lt)

    plot_ctl(x_ht, eform_cu, ctl_cu, 'C0', 'ht', ax0)
    plot_ctl(x_ht, eform_zn, ctl_zn, 'C3', 'ht', ax0)
    plot_ctl(x_ht, eform_zc, ctl_zc, 'C2', 'ht', ax0)
    plot_ctl(x_ht, eform_cl, ctl_cl, 'C5', 'ht', ax0)
    plot_ctl(x_ht, eform_cl1, ctl_cl1, 'C1', 'ht', ax0)
    plot_ctl(x_lt, eform_cu_lt, ctl_cu_lt, 'C0', 'lt', ax0)
    plot_ctl(x_lt, eform_zn_lt, ctl_zn_lt, 'C3', 'lt', ax0)
    plot_ctl(x_lt, eform_zc_lt, ctl_zc_lt, 'C2', 'lt', ax0)
    plot_ctl(x_lt, eform_cl_lt, ctl_cl_lt, 'C5', 'lt', ax0)
    plot_ctl(x_lt, eform_cl1_lt, ctl_cl1_lt, 'C1', 'lt', ax0)
    _cmap = cm.Blues if _i % 2 == 0 else cm.Greys
    ax0.imshow([[0.,0.],[1.,1]], cmap=_cmap, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
    ax0.axvline(eg_ht, lw=0.5, color='k')
    ax0.set_xlim([dvbm,eg_ht+dcbm])
    ax0.set_ylim([yo,ye])
    ax0.yaxis.set_major_locator(MultipleLocator(1.0))
    ax0.yaxis.set_minor_locator(MultipleLocator(0.5))
    ax0.xaxis.set_major_locator(MultipleLocator(0.4))
    ax0.xaxis.set_minor_locator(MultipleLocator(0.2))
    if _i == 0:
        ax0.text(0.55,0.89,r'Sn$_{Cu}$',color='C0',weight='light',transform=ax0.transAxes)
        ax0.text(0.58,0.67,r'Sn$_{Zn}$',color='C3',weight='light',transform=ax0.transAxes)
        ax0.text(0.40,0.15,r'Zn$_{Cu}$',color='C2',weight='light',transform=ax0.transAxes)
        ax0.text(0.19,0.32,r'Sn$_{Zn}$+2Cu$_{Zn}$',color='C5',weight='light',transform=ax0.transAxes)
        ax0.text(0.35,0.40,r'Sn$_{Zn}$+Cu$_{Zn}$',color='C1',weight='light',transform=ax0.transAxes)
    if _i > 3:
        ax0.set_xlabel('Fermi energy (eV)')
    ax0.tick_params(labelleft=False)

    eform_cz, ctl_cz = parse('cu_zn_ht.params',_mu['Zn']-_mu['Cu'],x_ht)
    eform_vc, ctl_vc = parse('v_cu_ht.params',_mu['Cu'],x_ht)
    eform_vs, ctl_vs = parse('v_sn_ht.params',_mu['Sn'],x_ht)
    eform_vz, ctl_vz = parse('v_zn_ht.params',_mu['Zn'],x_ht)
    eform_cs, ctl_cs = parse('cu_sn_ht.params',_mu['Sn']-_mu['Cu'],x_ht)
    eform_zs, ctl_zs = parse('zn_sn_ht.params',_mu['Sn']-_mu['Zn'],x_ht)
    eform_cz_lt, ctl_cz_lt = parse('cu_zn_lt.params',_mu['Zn']-_mu['Cu'],x_lt)
    eform_vc_lt, ctl_vc_lt = parse('v_cu_lt.params',_mu['Cu'],x_lt)
    eform_vs_lt, ctl_vs_lt = parse('v_sn_lt.params',_mu['Sn'],x_lt)
    eform_vz_lt, ctl_vz_lt = parse('v_zn_lt.params',_mu['Zn'],x_lt)
    eform_cs_lt, ctl_cs_lt = parse('cu_sn_lt.params',_mu['Sn']-_mu['Cu'],x_lt)
    eform_zs_lt, ctl_zs_lt = parse('zn_sn_lt.params',_mu['Sn']-_mu['Zn'],x_lt)

    plot_ctl(x_ht, eform_vs, ctl_vs, 'C8', 'ht', ax1)
    plot_ctl(x_ht, eform_vz, ctl_vz, 'C7', 'ht', ax1)
    plot_ctl(x_ht, eform_cs, ctl_cs, 'C0', 'ht', ax1)
    plot_ctl(x_ht, eform_zs, ctl_zs, 'C3', 'ht', ax1)
    plot_ctl(x_ht, eform_cz, ctl_cz, 'C2', 'ht', ax1)
    plot_ctl(x_ht, eform_vc, ctl_vc, 'C4', 'ht', ax1)
    plot_ctl(x_lt, eform_vs_lt, ctl_vs_lt, 'C8', 'lt', ax1)
    plot_ctl(x_lt, eform_vz_lt, ctl_vz_lt, 'C7', 'lt', ax1)
    plot_ctl(x_lt, eform_cs_lt, ctl_cs_lt, 'C0', 'lt', ax1)
    plot_ctl(x_lt, eform_zs_lt, ctl_zs_lt, 'C3', 'lt', ax1)
    plot_ctl(x_lt, eform_cz_lt, ctl_cz_lt, 'C2', 'lt', ax1)
    plot_ctl(x_lt, eform_vc_lt, ctl_vc_lt, 'C4', 'lt', ax1)

    ax1.imshow([[0.,0.],[1.,1]], cmap=_cmap, interpolation='bicubic',vmin=0, vmax=2,
        extent=(dvbm,eg_ht+dcbm,yo,ye), alpha=0.3, aspect='auto')
    ax1.axvline(eg_ht, lw=0.5, color='k')
    ax1.set_xlim([dvbm,eg_ht+dcbm])
    ax1.set_ylim([yo,ye])
    ax1.xaxis.set_major_locator(MultipleLocator(0.4))
    ax1.xaxis.set_minor_locator(MultipleLocator(0.2))
    if _i == 0:
        ax1.text(0.17,0.42,r'V$_{Zn}$',color='C7',weight='light',transform=ax1.transAxes)
        ax1.text(0.08,0.08,r'Cu$_{Zn}$',color='C2',weight='light',transform=ax1.transAxes)
        ax1.text(0.08,0.22,r'V$_{Cu}$',color='C4',weight='light',transform=ax1.transAxes)
        ax1.text(0.10,0.50,r'Cu$_{Sn}$',color='C0',weight='light',transform=ax1.transAxes)
        ax1.text(0.02,0.35,r'Zn$_{Sn}$',color='C3',weight='light',transform=ax1.transAxes)
        ax1.text(0.60,0.80,r'V$_{Sn}$',color='C8',weight='light',transform=ax1.transAxes)
    if _i > 3:
        ax1.set_xlabel('Fermi energy (eV)')
    ax1.set_ylabel('Formation energy (eV)')
    ax1.text(0.06,0.95,pd_label[_i], bbox=props, transform=ax1.transAxes, zorder=10)
    ax1.text(0.20,0.95,'Cu:{0:.2f}, Zn:{1:.2f}, Sn:{2:.2f}'.format(_mu['Cu'],_mu['Zn'],_mu['Sn']), bbox=props, transform=ax1.transAxes, zorder=10)
plt.savefig('def_level_all.pdf')
