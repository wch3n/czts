#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, AutoMinorLocator
from scipy import interpolate
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score, GridSearchCV
import seaborn as sns

df_czts = pd.read_hdf('occ_czts.h5')
df_czts = df_czts.sort_values(by='Temp')

fig_width = 8.5/2.54
fig_height = fig_width*1.0
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)

fig, (ax1,ax0) = plt.subplots(2,1, sharex=True)

C0="#A2C3A4"
C1="#525B76"
C2="#869D96"
C3="#4F878B"

'''
ax2.plot(df_czts.Temp, df_czts.Cu_2a, '-o', color=C0, lw=0.5, label=r'$2a$')
ax2.plot(df_czts.Temp, df_czts.Cu_2c, '-^', color=C1, lw=0.5, label=r'$2c$')
ax2.plot(df_czts.Temp, 1 - df_czts.Zn_2d, '-v', color=C2, lw=0.5, label=r'$2d$')
#ax2.text(700, 0.1, r'Cu$_2$ZnSnS$_4$', fontsize=7)
#ax2.set_xlabel('Temperature (K)')
ax2.set_ylabel('Cu occupancy')
ax2.set_xlim([200,800])
ax2.axhline(2/3, color=C3, lw=0.5, ls='dashed', zorder=0)
leg = ax2.legend()
leg.get_frame().set_linewidth(0)

minorLocator = AutoMinorLocator(n=2)
ax2.yaxis.set_minor_locator(minorLocator)
'''

# cluster functions
empty = [0]
point = [1,2]
pair_1 = [3,4]
pair_2 = [5,6,7]
pair_3 = [8,9,10,11]
pair_4 = [12,13,14,15]
triplet_1 = [16,17]
#triplet_2 = [18,19,20,21]

def select_cluster(df_corr, clusters):
    clus = []
    for i in clusters:
        clus = clus + i
    clus = np.array(clus)
    return df_corr.iloc[:,clus]

def regularize_dos(df_dos, grid_fine, smooth_width=0, log=False):
    df_dos_fine = pd.DataFrame(index=df_dos.index, columns=['dos_hse'])
    for i in df_dos.index:
        dos = df_dos.loc[i, "dos_hse"]
        grid_coarse = df_dos.loc[i, "energy_hse"]
        if smooth_width > 0:
            box = np.ones(smooth_width)/smooth_width
            dos = np.convolve(dos, box, mode='same')
        f = interpolate.interp1d(grid_coarse,dos)
        df_dos_fine.loc[i,"dos_hse"] = f(grid_fine)
    if log:
        _dos_log = df_dos_fine.copy()
        for i in df_dos_fine.index:
            _dos_log.loc[i,'dos_hse'] = np.log(df_dos_fine.loc[i,'dos_hse']+0.1)
        df_dos_fine = _dos_log
    return df_dos_fine

def weight(df_dos, gap_max, w_exp):
    w = np.exp((gap_max-df_dos.gap.values.astype(float))/w_exp)
    return w

def calc_model(corr,x, w):
    ridge = linear_model.Ridge(fit_intercept=False)
    alphas = np.logspace(-5, 0, 5)
    params = [{'alpha': alphas}]
    #reg.fit(corr.values, x, w)
    #model = linear_model.RidgeCV(alphas=[0.001, 0.01, 0.1, 1], fit_intercept=False, cv=3)
    model = GridSearchCV(ridge, params, scoring='neg_mean_squared_error', cv=3, refit=True, return_train_score= True)
    model.fit(corr.values, x)
    return model

def match(df_dos, df_corr):
    overlap = list(set(df_dos['configname']).intersection(set(df_corr['configname'])))
    if len(overlap) < len(df_dos['configname']):
        raise ValueError('DOS and correlation data inconsistent.')
    df = pd.DataFrame()
    for i in df_dos['configname']:
        df = df.append(df_corr[df_corr['configname'] == i])
    return df

def main(mat, clusters, gap_th=0.2, w_exp=0.1, align=False, verbose=False):
    global grid_fine, df_dos_fine, ref_ind
    smooth_width = 5 # the box width used to smooth the density of states
    is_log = False
    #grid_fine = np.arange(2,8,0.02) # grid for the selected energy range
    # read corr matrix and electronic densities
    df_corr = pd.read_csv('corr_{0}.csv'.format(mat), delim_whitespace=True)
    df_dos = pd.read_hdf('pdos_{0}.h5'.format(mat))
    # semiconductors only
    # drop metallic structures
    #
    df_dos = df_dos[df_dos['configname'].str.contains('SCEL1|SCEL2|SCEL3')]
    #
    semi = df_dos[(df_dos.gap > gap_th)].index
    df_dos = df_dos.loc[semi]
    df_corr = match(df_dos, df_corr)
    #
    assert (df_dos.index == df_corr.index).all()
    #ref_ind = df_dos.gap.idxmax()
    if mat == 'czts':
        ref_ind = 1
    else:
        ref_ind = 0
    vbm = df_dos.loc[ref_ind, 'vbm']
    # interpolate the dos on a specific regular mesh
    #df_dos_fine = regularize_dos(df_dos, grid_fine, smooth_width=smooth_width, log=is_log)
    #
    # define training set
    #train = df_corr.sample(frac=1, random_state=None)
    train = df_corr
    if ref_ind not in train.index:
        train = train.append(df_corr.loc[ref_ind])
    df_corr_train = train.loc[:,"corr(0)":"corr(17)"]
    gap_train = df_dos.loc[train.index, "gap"]
    # scissor shift
    gap_train += 0.9
    #
    corr_train = select_cluster(df_corr_train, clusters)
    #
    w = weight(df_dos.loc[train.index],df_dos.loc[ref_ind,'gap'],w_exp)
    model = calc_model(corr_train, gap_train, w)
    gap_train_pred = model.predict(corr_train)
    # plot the real dos (gray) and the predicted dos (red)
    '''plt.rcParams['figure.figsize'] = [10, 8]
    fig, ax1 = plt.subplots(1)
    for i in range(len(gap_train)):
        ax1.plot(gap_train.iloc[i], gap_train_pred[i], 'x', color='blue')
    #mse = mean_squared_error(gap_test, gap_test_pred)
    ax1.plot(np.linspace(0,1,2), np.linspace(0,1,2), '-', color='gray')
    ax1.set_xlabel('DFT band gap (eV)')
    ax1.set_ylabel('CE band gap (eV)')
    ax1.set_xlim([0.2,0.7])
    ax1.set_ylim([0.2,0.7])
    #ax1.text(0.2, 0.6, 'RMSE={0:.2f} eV'.format(mse**0.5), transform=ax1.transAxes)
    #'''
    return model

def ecdos(mat,clusters,model):
    global df_mc
    # plot DOS vs Temperature using the correlation matrix from MC simulations
    df_mc = pd.read_csv('mc_{0}.csv'.format(mat), delim_whitespace=True)
    df_mc = df_mc[df_mc['T'] <= 1000].reset_index()
    corr_str = []
    for i in range(18):
        corr_str.append("<corr({0:})>".format(i))
    df_corr_mc = df_mc.loc[:,corr_str]
    corr_mc = select_cluster(df_corr_mc, clusters)
    gap_mc_pred = model.predict(corr_mc)
    #
    return corr_mc, gap_mc_pred

mat = 'czts'
w_exp = 1
gap_th = 0.3
align = True
verbose = False
clusters = [empty, point, pair_1, pair_2, triplet_1]

model_czts = main(mat=mat, clusters=clusters, gap_th=gap_th, w_exp=w_exp, align=align, verbose=verbose)
print(np.sqrt(-model_czts.best_score_))
corr_mc, gap_mc_pred = ecdos(mat=mat, clusters=clusters, model=model_czts)

df_var = pd.read_hdf('var.h5')
try:
    for temp in df_mc['T']:
        _df_corr = df_var[df_var['Temp'] == temp].iloc[:,1:]
        _df_corr = select_cluster(_df_corr, clusters)
        _gap_pred = model_czts.predict(_df_corr)
        print(temp, _gap_pred.mean(), _gap_pred.std())
except:
    pass

ax1.set_xlim([200,800])
ax1.set_ylim([0.4,0.6])
#scissor
ax1.set_ylim([0.4+0.9, 0.6+0.9])
ax1.plot(df_mc['T'], gap_mc_pred, '-o', color=C3, lw=0.8, label=r'$E_g$')
ax1.set_ylabel('Band gap (eV)')
minorLocator = AutoMinorLocator(n=4)
ax1.yaxis.set_minor_locator(minorLocator)
ax1.yaxis.set_major_locator(MultipleLocator(0.1))
ax1s = ax1.twinx()
ax1s.plot(df_czts.Temp, (df_czts.Zn_2d-1./3)/(1-1./3), '-o', color=C1, fillstyle='full', lw=0.8, markersize=2.5, markerfacecolor='white', label='S')
ax1s.set_ylim([0,1])
ax1s.set_ylabel('Order parameter')
lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax1s.get_legend_handles_labels()
ax1.legend(lines + lines2, labels + labels2, loc="lower left")
ax1.set_zorder(1)
ax1.patch.set_visible(False)

#ax1.text(0.9,0.9, '(c)', fontweight='normal', transform=ax1.transAxes)
#ax2.text(0.9,0.9, '(a)', fontweight='normal', transform=ax2.transAxes)
#ax0.text(0.9,0.9, '(b)', fontweight='normal', transform=ax0.transAxes)

# motif
df = pd.read_json('motif.json', orient='table')
count = []
label = []
motif_print = ['Cu-Cu-Zn-Sn','Cu-Cu-Cu-Sn','Cu-Zn-Zn-Sn']
for i, motif in enumerate(['Cu-Cu-Sn-Zn', 'Cu-Cu-Cu-Sn', 'Cu-Sn-Zn-Zn']):
    f = interpolate.interp1d(df.index, df[motif]/32000, 'linear')
    xx = np.linspace(200,800,16,endpoint=True)
    count.append(f(xx))
    label.append(motif_print[i])

ax0.stackplot(xx, count[0], count[1], count[2], labels=label, colors=sns.color_palette("GnBu", 4))

ax0.set_xlim(200,800)
ax0.set_ylim(0,1)

ax0.set_xlabel('Temperature (K)')
ax0.set_ylabel('Concentration')
ax0.legend(loc='lower left')

fig.tight_layout()
fig.subplots_adjust(hspace=0.06)
plt.savefig('occ_gap_motif.png', dpi=300, transparent=True)
