#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, AutoMinorLocator

df_czts = pd.read_hdf('occ_czts.h5')

df_czts = df_czts.sort_values(by='Temp')

fig_width = 8.5/2.54
fig_height = fig_width*0.8
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)

#fig, (ax1,ax2) = plt.subplots(1,2, sharey=True)
fig, ax2 = plt.subplots(1)

ax2.plot(df_czts.Temp, 1 - df_czts.Zn_2d, '-x', label=r'Cu$_{2d}$')
ax2.plot(df_czts.Temp, df_czts.Cu_2a, '-x', label=r'Cu$_{2a}$')
ax2.plot(df_czts.Temp, df_czts.Cu_2c, '-x', label=r'Cu$_{2c}$')
ax2.text(700, 0.1, r'Cu$_2$ZnSnS$_4$', fontsize=7)
ax2.set_xlabel('Temperature (K)')
ax2.set_xlim([200,900])
ax2.axhline(2/3, color='gray', lw=0.5, ls='dashed', zorder=0)
ax2.legend()

minorLocator = AutoMinorLocator(n=2)
ax2.yaxis.set_minor_locator(minorLocator)

plt.tight_layout()
plt.savefig('occ.pdf')
