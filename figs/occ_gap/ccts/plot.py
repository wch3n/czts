#!/usr/bin/env python3

import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, AutoMinorLocator
from scipy import interpolate
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score, GridSearchCV

fig_width = 8.5/2.54
fig_height = fig_width*0.6
fig_size = [fig_width, fig_height]
params = {'backend': 'ps',
          'axes.linewidth': 0.6,
          'axes.labelsize': 7,
          'axes.labelweight': 'light',
          'font.size': 6,
          'font.weight': 'light',
          'legend.fontsize': 6,
          'xtick.labelsize': 6,
          'ytick.labelsize': 6,
          'text.usetex': False,
          'mathtext.default': 'regular',
          'figure.figsize': fig_size,
          'lines.markersize': 3.0,
          'xtick.direction': 'in',
          'ytick.direction': 'in',
          'xtick.top': True,
          'ytick.right': True,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.3,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.3}
plt.rcParams.update(params)

fig, ax1 = plt.subplots(1)

C0="#A2C3A4"
C1="#525B76"
C2="#869D96"
C3="#4F878B"

minorLocator = AutoMinorLocator(n=2)

# cluster functions
empty = [0]
point = [1,2]
pair_1 = [3,4]
pair_2 = [5,6,7]
pair_3 = [8,9,10,11]
pair_4 = [12,13,14,15]
triplet_1 = [16,17]
#triplet_2 = [18,19,20,21]

def select_cluster(df_corr, clusters):
    clus = []
    for i in clusters:
        clus = clus + i
    clus = np.array(clus)
    return df_corr.iloc[:,clus]

def regularize_dos(df_dos, grid_fine, smooth_width=0, log=False):
    df_dos_fine = pd.DataFrame(index=df_dos.index, columns=['dos_hse'])
    for i in df_dos.index:
        dos = df_dos.loc[i, "dos_hse"]
        grid_coarse = df_dos.loc[i, "energy_hse"]
        if smooth_width > 0:
            box = np.ones(smooth_width)/smooth_width
            dos = np.convolve(dos, box, mode='same')
        f = interpolate.interp1d(grid_coarse,dos)
        df_dos_fine.loc[i,"dos_hse"] = f(grid_fine)
    if log:
        _dos_log = df_dos_fine.copy()
        for i in df_dos_fine.index:
            _dos_log.loc[i,'dos_hse'] = np.log(df_dos_fine.loc[i,'dos_hse']+0.1)
        df_dos_fine = _dos_log
    return df_dos_fine

def weight(df_dos, gap_max, w_exp):
    w = np.exp((gap_max-df_dos.gap.values.astype(float))/w_exp)
    return w

def calc_model(corr,x, w):
    ridge = linear_model.Ridge(fit_intercept=False)
    alphas = np.logspace(-5, 0, 5)
    params = [{'alpha': alphas}]
    #reg.fit(corr.values, x, w)
    #model = linear_model.RidgeCV(alphas=[0.001, 0.01, 0.1, 1], fit_intercept=False, cv=3)
    model = GridSearchCV(ridge, params, scoring='neg_mean_squared_error', cv=3, refit=True, return_train_score= True)
    model.fit(corr.values, x)
    return model

def match(df_dos, df_corr):
    overlap = list(set(df_dos['configname']).intersection(set(df_corr['configname'])))
    if len(overlap) < len(df_dos['configname']):
        raise ValueError('DOS and correlation data inconsistent.')
    df = pd.DataFrame()
    for i in df_dos['configname']:
        df = df.append(df_corr[df_corr['configname'] == i])
    return df

def main(mat, clusters, gap_th=0.2, w_exp=0.1, align=False, verbose=False):
    global grid_fine, df_dos_fine, ref_ind
    smooth_width = 5 # the box width used to smooth the density of states
    is_log = False
    #grid_fine = np.arange(2,8,0.02) # grid for the selected energy range
    # read corr matrix and electronic densities
    df_corr = pd.read_csv('corr_{0}.csv'.format(mat), delim_whitespace=True)
    df_dos = pd.read_hdf('pdos_{0}.h5'.format(mat))
    # semiconductors only
    # drop metallic structures
    #
    df_dos = df_dos[df_dos['configname'].str.contains('SCEL1|SCEL2|SCEL3')]
    #
    semi = df_dos[(df_dos.gap > gap_th)].index
    df_dos = df_dos.loc[semi]
    df_corr = match(df_dos, df_corr)
    #
    assert (df_dos.index == df_corr.index).all()
    #ref_ind = df_dos.gap.idxmax()
    if mat == 'czts':
        ref_ind = 1
    else:
        ref_ind = 0
    vbm = df_dos.loc[ref_ind, 'vbm']
    # interpolate the dos on a specific regular mesh
    #df_dos_fine = regularize_dos(df_dos, grid_fine, smooth_width=smooth_width, log=is_log)
    #
    # define training set
    #train = df_corr.sample(frac=1, random_state=None)
    train = df_corr
    if ref_ind not in train.index:
        train = train.append(df_corr.loc[ref_ind])
    df_corr_train = train.loc[:,"corr(0)":"corr(17)"]
    gap_train = df_dos.loc[train.index, "gap"]
    #
    corr_train = select_cluster(df_corr_train, clusters)
    #
    w = weight(df_dos.loc[train.index],df_dos.loc[ref_ind,'gap'],w_exp)
    model = calc_model(corr_train, gap_train, w)
    gap_train_pred = model.predict(corr_train)
    # plot the real dos (gray) and the predicted dos (red)
    '''plt.rcParams['figure.figsize'] = [10, 8]
    fig, ax1 = plt.subplots(1)
    for i in range(len(gap_train)):
        ax1.plot(gap_train.iloc[i], gap_train_pred[i], 'x', color='blue')
    #mse = mean_squared_error(gap_test, gap_test_pred)
    ax1.plot(np.linspace(0,1,2), np.linspace(0,1,2), '-', color='gray')
    ax1.set_xlabel('DFT band gap (eV)')
    ax1.set_ylabel('CE band gap (eV)')
    ax1.set_xlim([0.2,0.7])
    ax1.set_ylim([0.2,0.7])
    #ax1.text(0.2, 0.6, 'RMSE={0:.2f} eV'.format(mse**0.5), transform=ax1.transAxes)
    #'''
    return model

def ecdos(mat,clusters,model):
    global df_mc
    # plot DOS vs Temperature using the correlation matrix from MC simulations
    df_mc = pd.read_csv('mc_{0}.csv'.format(mat), delim_whitespace=True)
    df_mc = df_mc[df_mc['T'] <= 1000].reset_index()
    corr_str = []
    for i in range(18):
        corr_str.append("<corr({0:})>".format(i))
    df_corr_mc = df_mc.loc[:,corr_str]
    corr_mc = select_cluster(df_corr_mc, clusters)
    gap_mc_pred = model.predict(corr_mc)
    #
    return corr_mc, gap_mc_pred

mat = 'ccts'
w_exp = 1
gap_th = 0.2
align = True
verbose = False
clusters = [empty, point, pair_1, pair_2, triplet_1] 

model_czts = main(mat=mat, clusters=clusters, gap_th=gap_th, w_exp=w_exp, align=align, verbose=verbose)
print(np.sqrt(-model_czts.best_score_))
corr_mc, gap_mc_pred = ecdos(mat=mat, clusters=clusters, model=model_czts)

ax1.set_xlim([200,800])
ax1.set_ylim([0.2,0.4])
ax1.plot(df_mc['T'], gap_mc_pred, '-o', color=C3, lw=0.8)
ax1.set_xlabel('Temperature (K)')
ax1.set_ylabel('Band gap (eV)')
minorLocator = AutoMinorLocator(n=4)
ax1.yaxis.set_minor_locator(minorLocator)
ax1.yaxis.set_major_locator(MultipleLocator(0.1))

ax1.text(0.9,0.9, '(b)', fontweight='normal', transform=ax1.transAxes)

plt.tight_layout()
plt.savefig('gap.eps')

np.savetxt('gap_T.dat', np.array(list(zip(df_mc['T'],gap_mc_pred))))
